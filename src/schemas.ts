import z from 'zod';
import { createTemplateString } from '@/utils/createTemplateString';
import {
    COMMON_REQUIRED_ERROR,
    MIN_NAME_LENGTH,
    MAX_NAME_LENGTH,
    NAME_MIN_LENGTH_ERROR,
    NAME_MAX_LENGTH_ERROR,
    EMAIL_ERROR,
    MIN_PHONE_LENGTH,
    MAX_PHONE_LENGTH,
    PHONE_MIN_LENGTH_ERROR,
    PHONE_MAX_LENGTH_ERROR,
    PHONE_ERROR,
    PASSWORD_MAX_LENGTH_ERROR,
    PASSWORD_MIN_LENGTH_ERROR,
    MAX_PASSWORD_LENGTH,
    MIN_PASSWORD_LENGTH,
    PASSWORD_TIP_NOT_WHITESPACE,
    WHITESPACE_REGEX,
    LOWERCASE_AND_UPPERCASE_REGEX,
    FIELD_PASSWORD_LOWERCASE_AND_UPPERCASE_TIP,
    SPECIAL_SYMBOLS_REGEX,
    FIELD_PASSWORD_SPECIAL_SYMBOLS_TIP,
} from '@/constants';

export const emailSchema = z.string({ required_error: COMMON_REQUIRED_ERROR }).trim().email(EMAIL_ERROR);

export const passwordSchema = z
    .string({ required_error: COMMON_REQUIRED_ERROR })
    .trim()
    .min(MIN_PASSWORD_LENGTH, {
        message: createTemplateString(PASSWORD_MIN_LENGTH_ERROR, [MIN_PASSWORD_LENGTH.toString()]),
    })
    .max(MAX_PASSWORD_LENGTH, {
        message: createTemplateString(PASSWORD_MAX_LENGTH_ERROR, [MAX_PASSWORD_LENGTH.toString()]),
    })
    .regex(new RegExp(WHITESPACE_REGEX), PASSWORD_TIP_NOT_WHITESPACE);

export const passwordCreateSchema = passwordSchema
    .regex(new RegExp(LOWERCASE_AND_UPPERCASE_REGEX), FIELD_PASSWORD_LOWERCASE_AND_UPPERCASE_TIP)
    .regex(new RegExp(SPECIAL_SYMBOLS_REGEX), FIELD_PASSWORD_SPECIAL_SYMBOLS_TIP);

export const nameSchema = z
    .string({ required_error: COMMON_REQUIRED_ERROR })
    .min(MIN_NAME_LENGTH, { message: createTemplateString(NAME_MIN_LENGTH_ERROR, [MIN_NAME_LENGTH.toString()]) })
    .max(MAX_NAME_LENGTH, { message: createTemplateString(NAME_MAX_LENGTH_ERROR, [MAX_NAME_LENGTH.toString()]) });

export const phoneSchema = z
    .string({ required_error: COMMON_REQUIRED_ERROR })
    .regex(/^\+?(?:[0-9] ?){6,14}[0-9]$/, PHONE_ERROR)
    .min(MIN_PHONE_LENGTH, { message: createTemplateString(PHONE_MIN_LENGTH_ERROR, [MIN_PHONE_LENGTH.toString()]) })
    .max(MAX_PHONE_LENGTH, { message: createTemplateString(PHONE_MAX_LENGTH_ERROR, [MAX_PHONE_LENGTH.toString()]) });

export const stringSchema = z.string({ required_error: COMMON_REQUIRED_ERROR }).min(1, COMMON_REQUIRED_ERROR);

export const dropdownItemSchema = z.object({
    label: stringSchema,
    value: stringSchema,
});

export const requiredSelectItemSchema = z
    .object(
        {
            label: z.string().min(1, COMMON_REQUIRED_ERROR),
            value: z.string().min(1, COMMON_REQUIRED_ERROR),
        },
        { required_error: COMMON_REQUIRED_ERROR, invalid_type_error: COMMON_REQUIRED_ERROR }
    )
    .nullable()
    .refine(
        (value) => {
            return value !== null;
        },
        {
            message: COMMON_REQUIRED_ERROR,
        }
    );

export const zipCodeSchema = z.string({ required_error: COMMON_REQUIRED_ERROR }).min(5, 'Zip code must be 5 digits');
