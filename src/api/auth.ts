import { type JWTPayload, SignJWT, jwtVerify } from 'jose';
import bcrypt from 'bcryptjs';
import { isPast } from 'date-fns';
import type { Options } from 'ky';
import type { RequestCommonResponse } from '@/types';
import type { Affiliate } from './firebase';
import { addAffiliate, addUser, getUserByEmail } from './firebase';
import { loginSchema } from '@/modules/Login/schemas';
import { emailSchema } from '@/schemas';
import { HASH_SALT, JWT_ALG, JWT_EXPIRATION_TIME, LS_AUTH_TOKEN, SECRET_KEY } from '@/constants';

export type UserType = 'Affiliate' | 'Partner';

export type User = {
    affiliate_id: number;
    sponsor_id?: number;
    tree_level?: number;
    type: UserType;
    email: string;
    phone: string;
    company: string;
    lastname: string;
    state: string;
    city: string;
    zip: string;
    firstname: string;
    password: string;
    account_name?: string;
    bank_routing_number?: string;
    bank_account_number?: string;
    bank_name?: string;
    bank_address?: string;
    'Hubspot ID'?: string;
    verified?: boolean;
    homeStateLifeInsuranceNumber: string;
    homeStateHealthInsuranceNumber: string;
    // Splitted by comma
    licensedStates: string;
};

export type UserJWTPayload = {
    id: string;
    email: string;
    hubspotContactId: string;
    sponsorId?: string;
    exp: number;
};

export type AuthLoginRequest = {
    email: string;
    password: string;
};
export type AuthLoginResponse = RequestCommonResponse<{
    token?: string;
    user: User;
}>;

export type AuthRegisterRequest = User;
export type AuthRegisterResponse = RequestCommonResponse<unknown>;

// export type AuthRegisterResponse = {
//     archived: boolean;
//     createdAt: string | Date;
//     id: string;
//     properties: Omit<AuthRegisterRequest, 'password'>;
// };

export const verifyAuthToken = async (): Promise<false | JWTPayload | { isExpired: true }> => {
    const authToken = localStorage.getItem(LS_AUTH_TOKEN);

    if (!authToken) {
        return false;
    }

    try {
        const result = await jwtVerify(authToken, SECRET_KEY);

        if (!result || !Object.keys(result).length || !result?.payload?.exp) {
            return false;
        }

        const tokenExpirationTimestamp = result?.payload?.exp * 1000;

        if (isPast(new Date(tokenExpirationTimestamp))) {
            return {
                isExpired: true,
            };
        }

        return result.payload;
    } catch (error) {
        return {
            isExpired: true,
        };
    }
};

export const logIn = async (data: AuthLoginRequest, _config?: Options): Promise<AuthLoginResponse> => {
    const isDataValid = loginSchema.safeParse(data).success;

    if (!isDataValid) {
        return {
            success: false,
            message: 'Invalid data. Please try again.',
        };
    }

    const user = await getUserByEmail(data.email.toLowerCase());

    if (!user) {
        return {
            success: false,
            message: 'User not found',
        };
    }

    const isPasswordCorrect = await bcrypt.compare(data.password, user.password);

    const token = await new SignJWT({
        email: data.email,
        id: user.affiliate_id,
        tree_level: user.tree_level ?? 0,
        hubspotContactId: user['Hubspot ID'] ?? '',
        sponsorId: user.sponsor_id ?? '',
    })
        .setProtectedHeader({ alg: JWT_ALG })
        .setExpirationTime(JWT_EXPIRATION_TIME)
        .sign(SECRET_KEY);

    return new Promise<AuthLoginResponse>((resolve) => {
        if (isPasswordCorrect) {
            if (!user.verified) {
                resolve({
                    success: false,
                    message: 'User not verified. Please check your email for the verification link.',
                });
            }

            localStorage.setItem(LS_AUTH_TOKEN, token);

            resolve({
                success: true,
                token,
                user,
            });
        } else {
            resolve({
                success: false,
                message: 'Invalid email or password',
            });
        }
    });
};

export const register = async (
    { password, email, ...rest }: AuthRegisterRequest,
    _config?: Options
): Promise<AuthRegisterResponse> => {
    try {
        const isEmailValid = emailSchema.safeParse(email).success;

        if (!isEmailValid) {
            return {
                success: false,
                message: 'Invalid data. Please try again.',
            };
        }

        const hashedPassword = await bcrypt.hash(password, HASH_SALT);
        const dataWithoutUndefinedFields = Object.fromEntries(
            Object.entries({ ...rest, password: hashedPassword, email: email.toLowerCase() }).filter(([, value]) => {
                return value !== undefined;
            })
        ) as AuthRegisterRequest;

        await addUser({
            ...dataWithoutUndefinedFields,
            verified: false,
        });

        const { affiliate_id, firstname, lastname } = rest;
        const affiliateDataWithoutUndefinedFields = Object.fromEntries(
            Object.entries({
                id: affiliate_id.toString(),
                sponsor_id: dataWithoutUndefinedFields.sponsor_id,
                tree_level: dataWithoutUndefinedFields.tree_level,
                email: email.toLowerCase(),
                phone: dataWithoutUndefinedFields.phone,
                firstname,
                lastname,
            }).filter(([, value]) => {
                return value !== undefined;
            })
        ) as Omit<Affiliate, 'affiliates'>;

        await addAffiliate(affiliateDataWithoutUndefinedFields);

        return {
            success: true,
        };
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);

        return {
            success: false,
            message: 'Something went wrong. Please try again.',
        };
    }
};

export const logOut = async () => {
    await localStorage.removeItem(LS_AUTH_TOKEN);
};
