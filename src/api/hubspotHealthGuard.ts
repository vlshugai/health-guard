import type { Options } from 'ky';
import type { HubspotContactTierLevel } from '@/types';
import type { UserType } from '@/api/auth';
import type { DealStatusCode, DealsCommissionStatusOptionIds } from '@/modules/Dashboard/types';
import { hubspotHealthGuardHttp } from './ky';

type HubspotDealPosition = {
    [key in `deal_position_${HubspotContactTierLevel}_affiliate_id`]: string | null;
} & {
    [key in `deal_position_${HubspotContactTierLevel}_commission_amount`]: string | null;
} & {
    [key in `deal_position_${HubspotContactTierLevel}_commission_payment_status`]: DealsCommissionStatusOptionIds | null;
};

export type HubspotContact = {
    affiliate_id: number;
    sponsor_id?: number;
    tree_level: number;
    type: UserType;
    email: string;
    phone: string;
    company?: string;
    lastname: string;
    firstname: string;
    account: string;
    city: string;
    state: string;
    zip: string;
    routing: string;
    bank_account_holder_name: string;
    bank_name: string;
    bank_address: string;
    paypal_address: string;
    bank_city: string;
    bank_state_code: string;
    bank_zip_code: string;
};

export type CreateHubspotContactRequest = HubspotContact;

export type CreateHubspotContactSuccess = {
    hubspot_contact_id: string;
};
export type CreateHubspotContactError = {
    email_used_by: string;
};
export type CreateHubspotContactResponse = CreateHubspotContactSuccess | CreateHubspotContactError;

export type UpdateHubspotContactRequest = Partial<
    Pick<
        HubspotContact,
        | 'tree_level'
        | 'email'
        | 'phone'
        | 'company'
        | 'lastname'
        | 'firstname'
        | 'account'
        | 'routing'
        | 'bank_name'
        | 'bank_address'
        | 'paypal_address'
        | 'city'
        | 'state'
        | 'zip'
        | 'bank_city'
        | 'bank_state_code'
        | 'bank_zip_code'
        | 'bank_account_holder_name'
    >
> &
    CreateHubspotContactResponse;

export type GetHubspotDealsRequest = Partial<{
    affiliate: string;
}>;

export type HubspotSubmissionFields = {
    firstname: string;
    lastname: string;
    email: string;
    phone: string;
    state?: string;
    city?: string;
    address?: string;
    zip?: string;
    call_back_date?: string;
    call_back_time?: string;
};

export type HubspotDeal = {
    affiliate_id: `${number}`;
    closedate: string | Date;
    createdate: string | Date;
    dealstage: DealStatusCode;
    dealname?: string | null;
    hs_object_id: string;
    hs_lastmodifieddate: string | Date;
} & HubspotDealPosition;

export type GetDealsByAffiliatesRequest = {
    affiliate: string;
};

export type HubspotDealItem = {
    id: string;
    properties: HubspotDeal;
    createdAt: string | Date;
    updatedAt: string | Date;
    archived: boolean;
};

export type GetDealsByAffiliatesResponse = {
    total: number;
    results: HubspotDealItem[];
};

export const getDealsByAffiliates = async (data: GetDealsByAffiliatesRequest, config?: Options) => {
    const response = await hubspotHealthGuardHttp.post('deals-by-affiliates', {
        json: data,
        ...config,
    });

    return await response.json<GetDealsByAffiliatesResponse>();
};

export const createHubspotContact = async (data: CreateHubspotContactRequest, config?: Options) => {
    const response = await hubspotHealthGuardHttp.post('create-contact', {
        json: data,
        ...config,
    });

    return await response.json<CreateHubspotContactResponse>();
};

export const updateHubspotContact = async (data: UpdateHubspotContactRequest, config?: Options) => {
    const response = await hubspotHealthGuardHttp.post('update-contact', {
        json: data,
        ...config,
    });

    return await response.json();
};
