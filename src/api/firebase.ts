import { initializeApp } from 'firebase/app';
import { getDatabase, ref, set, onValue, update } from 'firebase/database';
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth';
import type { RequestCommonResponse } from '@/types';
import type { User } from './auth';
import {
    FIREBASE_API_KEY,
    FIREBASE_APP_ID,
    FIREBASE_AUTH_DOMAIN,
    FIREBASE_AUTH_EMAIL,
    FIREBASE_AUTH_PASSWORD,
    FIREBASE_DATABASE_URL,
    FIREBASE_MESSAGING_SENDER_ID,
    FIREBASE_PROJECT_ID,
    FIREBASE_STORAGE_BUCKET,
} from '@/constants';
import { emailSchema, stringSchema } from '@/schemas';

export type Affiliate = {
    id: string;
    affiliates: string;
} & Pick<User, 'sponsor_id' | 'tree_level' | 'email' | 'firstname' | 'lastname' | 'phone'>;

export type VerifyUserResponse = RequestCommonResponse<unknown>;
export type GetUserTeamResponse = {
    totalFirstLevelAffiliates: number;
    totalSecondLevelAffiliates: number;
    firstLevelAffiliates: Affiliate[];
    secondLevelAffiliates: Affiliate[];
} | null;
export type GetAffiliateByIdResponse = Affiliate | null;
export type GetUserByEmailResponse = User | null;

// Initialize Firebase
const app = initializeApp({
    apiKey: FIREBASE_API_KEY,
    authDomain: FIREBASE_AUTH_DOMAIN,
    projectId: FIREBASE_PROJECT_ID,
    storageBucket: FIREBASE_STORAGE_BUCKET,
    messagingSenderId: FIREBASE_MESSAGING_SENDER_ID,
    appId: FIREBASE_APP_ID,
    databaseURL: FIREBASE_DATABASE_URL,
});
export const db = getDatabase(app);
export const auth = getAuth(app);

signInWithEmailAndPassword(auth, FIREBASE_AUTH_EMAIL, FIREBASE_AUTH_PASSWORD);

export const convertEmailToFirebaseContactId = (email: string) => {
    return email.toLowerCase().replace(/[&/\\#,+()$~%.'":*?<>{}]/g, '_');
};

export const addAffiliateIdToSponsor = (ids: string, affiliateId: string) => {
    return Array.from(new Set([...ids.split(','), affiliateId]))
        .filter(Boolean)
        .join(',');
};

export const addUser = async (data: User) => {
    const isEmailValid = emailSchema.safeParse(data.email).success;

    if (!isEmailValid) {
        throw new Error('Invalid email. Please try again with another one.');
    }

    try {
        const emailWithUnderscores = convertEmailToFirebaseContactId(data.email);
        await set(ref(db, `users/${emailWithUnderscores}`), data);
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
        throw new Error('Failed to add user to Firebase database');
    }
};

export const getUserByEmail = async (email: string): Promise<GetUserByEmailResponse> => {
    return await new Promise<User | null>((resolve) => {
        const isEmailValid = emailSchema.safeParse(email).success;

        if (!isEmailValid) {
            resolve(null);
            return;
        }

        try {
            const emailWithUnderscores = convertEmailToFirebaseContactId(email);
            const userSnapshotRef = ref(db, `users/${emailWithUnderscores}`);

            onValue(userSnapshotRef, (snapshot) => {
                resolve(snapshot.val());
            });
        } catch (error) {
            // eslint-disable-next-line no-console
            console.error(error);
            resolve(null);
        }
    });
};

export const getSponsorById = async (sponsorId?: string) => {
    return await new Promise<User | null>((resolve, reject) => {
        const isSponsorIdValid = stringSchema.safeParse(sponsorId).success;

        if (!isSponsorIdValid) {
            resolve(null);
            return;
        }

        try {
            const userSnapshotRef = ref(db, `users/${sponsorId}`);

            onValue(userSnapshotRef, (snapshot) => {
                resolve(snapshot.val());
            });
        } catch (error) {
            // eslint-disable-next-line no-console
            console.error(error);
            reject(new Error('Failed to get sponsor from Firebase database'));
        }
    });
};

export const getAffiliateById = async (id: string): Promise<GetAffiliateByIdResponse> => {
    return await new Promise<Affiliate | null>((resolve) => {
        const isAffiliateIdValid = stringSchema.safeParse(id).success;

        if (!isAffiliateIdValid) {
            resolve(null);
            return;
        }

        try {
            const affiliateSnapshotRef = ref(db, `affiliates/${id}`);

            onValue(affiliateSnapshotRef, (snapshot) => {
                resolve(snapshot.val());
            });
        } catch (error) {
            // eslint-disable-next-line no-console
            console.error(error);
            resolve(null);
        }
    });
};

export const addAffiliate = async (data: Omit<Affiliate, 'affiliates'>) => {
    const isIsValid = stringSchema.safeParse(data.id).success;
    const isEmailValid = emailSchema.safeParse(data.email).success;

    if (!isIsValid || !isEmailValid) {
        throw new Error('Invalid data. Please try again.');
    }

    try {
        const newAffiliate: Affiliate = {
            ...data,
            affiliates: '',
        };

        const updates = {} as Record<string, Affiliate>;

        await set(ref(db, `affiliates/${data.id}`), newAffiliate);

        if (data?.sponsor_id) {
            const sponsor = await getAffiliateById(data.sponsor_id.toString());
            if (sponsor) {
                const updatedSponsorAffiliates = addAffiliateIdToSponsor(sponsor.affiliates, data.id);

                updates[`affiliates/${data.sponsor_id}`] = {
                    ...sponsor,
                    affiliates: updatedSponsorAffiliates,
                };
            }
        }

        if (Object.keys(updates).length > 0) {
            await update(ref(db), updates);
        }

        return newAffiliate;
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
        throw new Error('Failed to add affiliate to Firebase database');
    }
};

export const getUserAffiliatesTeam = async (userAffiliateId?: string): Promise<GetUserTeamResponse> => {
    if (!userAffiliateId) {
        return null;
    }

    try {
        const userAffiliate = await getAffiliateById(userAffiliateId.toString());

        if (!userAffiliate) {
            return null;
        }

        const firstLevelAffiliatesIds = (userAffiliate?.affiliates ?? []).split(',').filter(Boolean);

        const firstLevelAffiliatesPromiseResponses = await Promise.all(
            firstLevelAffiliatesIds.map(async (id) => {
                return await getAffiliateById(id);
            })
        );

        const firstLevelAffiliates = firstLevelAffiliatesPromiseResponses.filter(Boolean) as Affiliate[];
        const totalFirstLevelAffiliates = firstLevelAffiliates.length;

        const secondLevelAffiliatesIds = firstLevelAffiliates
            .flatMap((affiliate) => {
                return affiliate.affiliates.split(',');
            })
            .filter(Boolean);

        const secondLevelAffiliatesPromiseResponses = await Promise.all(
            secondLevelAffiliatesIds.map(async (id) => {
                return await getAffiliateById(id);
            })
        );

        const secondLevelAffiliates = secondLevelAffiliatesPromiseResponses.filter(Boolean) as Affiliate[];
        const totalSecondLevelAffiliates = secondLevelAffiliates.length;

        return {
            totalFirstLevelAffiliates,
            totalSecondLevelAffiliates,
            firstLevelAffiliates,
            secondLevelAffiliates,
        };
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);
        throw new Error('Failed to get user team from Firebase database');
    }
};

export const verifyUser = async (email: string): Promise<VerifyUserResponse> => {
    const isEmailValid = emailSchema.safeParse(email).success;

    if (!isEmailValid) {
        return {
            success: false,
            message: 'Invalid email. Please try again with another one.',
        };
    }

    try {
        const user = await getUserByEmail(email.toLowerCase());

        if (!user) {
            return {
                success: false,
                message: 'User not found',
            };
        }

        if (user?.verified) {
            return {
                success: false,
                message: 'User was already verified',
            };
        }

        await update(ref(db, `users/${convertEmailToFirebaseContactId(email)}`), {
            verified: true,
        });

        return {
            success: true,
        };
    } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error);

        return {
            success: false,
            message: 'Oops. Something went wrong. Please try again later.',
        };
    }
};
