import type { Options } from 'ky';
import { hubspotFormsHttp } from './ky';
import { HUBSPOT_FORMS_GUID, HUBSPOT_FORMS_PORTAL_ID } from '@/constants';

export type HubspotSubmissionFields = {
    firstname: string;
    lastname: string;
    email: string;
    phone: string;
    state?: string;
    city?: string;
    address?: string;
    zip?: string;
    call_back_date?: string;
    call_back_time?: string;
};

export type CreateHubspotSubmissionRequest = { name: keyof HubspotSubmissionFields; value: string }[];

export type CreateHubspotSubmissionResponse = {
    inlineMessage: string;
};

export const createHubspotSubmission = async (data: CreateHubspotSubmissionRequest, config?: Options) => {
    const response = await hubspotFormsHttp.post(
        `submissions/v3/integration/submit/${HUBSPOT_FORMS_PORTAL_ID}/${HUBSPOT_FORMS_GUID}`,
        {
            json: {
                formGuid: HUBSPOT_FORMS_GUID,
                portalId: HUBSPOT_FORMS_PORTAL_ID,
                fields: data,
            },
            ...config,
        }
    );

    return await response.json<CreateHubspotSubmissionResponse>();
};
