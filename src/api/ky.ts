import ky from 'ky';
import { API_URL } from '@/constants';

export const http = ky.create({
    prefixUrl: API_URL,
});

export const hubspotHealthGuardHttp = ky.create({
    prefixUrl: 'https://forms.triton.finance/',
    retry: 1,
});

export const hubspotFormsHttp = ky.create({
    prefixUrl: 'https://api.hsforms.com/',
});

export const inboundProspectHttp = ky.create({
    prefixUrl: 'https://api.inboundprospect.com/',
    retry: 1,
});
