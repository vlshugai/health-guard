import { Outlet, createFileRoute, redirect } from '@tanstack/react-router';
import type { UserJWTPayload } from '@/api/auth';
import { verifyAuthToken } from '@/api/auth';
import DashboardLayout from '@/components/layouts/DashboardLayout';

export const Route = createFileRoute('/_dashboard-layout')({
    component() {
        return (
            <DashboardLayout>
                <Outlet />
            </DashboardLayout>
        );
    },
    async beforeLoad() {
        const tokenVerification = await verifyAuthToken();

        if (!tokenVerification) {
            throw redirect({
                to: '/login',
            });
        }

        if (tokenVerification.isExpired) {
            throw redirect({
                to: '/login/session-expired',
            });
        }

        return {
            user: tokenVerification as UserJWTPayload,
        };
    },
    loader({ context }) {
        return context;
    },
});
