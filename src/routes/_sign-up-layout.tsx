import z from 'zod';
import { Outlet, createFileRoute } from '@tanstack/react-router';
import { getAffiliateById } from '@/api/firebase';
import { EXTRACT_NUMBER_REGEX } from '@/constants';
import {
    signUpSponsorIdSearchParamsSchema,
    signUpContactFieldsSearchParamsSchema,
    signUpPaymentFieldsSearchParamsSchema,
    signUpCreatePasswordFieldsSearchParamsSchema,
} from '@/modules/SignUp/schemas';
import {
    SIGN_UP_CONTACT_STEP_INITIAL_VALUES,
    SIGN_UP_PAYMENT_STEP_INITIAL_VALUES,
    SIGN_UP_CREATE_PASSWORD_STEP_INITIAL_VALUES,
} from '@/modules/SignUp/constants';
import PageLoader from '@/modules/PageLoader';
import SignUp from '@/modules/SignUp';
import SignUpLayout from '@/components/layouts/SignUpLayout';

type SignUpLayoutSearchParams = z.infer<typeof signUpSponsorIdSearchParamsSchema> &
    z.infer<typeof signUpContactFieldsSearchParamsSchema> &
    z.infer<typeof signUpPaymentFieldsSearchParamsSchema> &
    z.infer<typeof signUpCreatePasswordFieldsSearchParamsSchema>;

export const Route = createFileRoute('/_sign-up-layout')({
    component() {
        return (
            <SignUpLayout>
                <SignUp />
                <Outlet />
            </SignUpLayout>
        );
    },
    validateSearch(search: Record<string, string>): SignUpLayoutSearchParams {
        const signUpSponsorIdValidation = signUpSponsorIdSearchParamsSchema.safeParse(search);
        const contactStepValidation = signUpContactFieldsSearchParamsSchema.safeParse(search);
        const paymentStepValidation = signUpPaymentFieldsSearchParamsSchema.safeParse(search);
        const passwordStepValidation = signUpCreatePasswordFieldsSearchParamsSchema.safeParse(search);

        return {
            subId: signUpSponsorIdValidation.success ? signUpSponsorIdValidation.data.subId : '',
            ...(contactStepValidation.success ? contactStepValidation.data : SIGN_UP_CONTACT_STEP_INITIAL_VALUES),
            ...(paymentStepValidation.success ? paymentStepValidation.data : SIGN_UP_PAYMENT_STEP_INITIAL_VALUES),
            ...(passwordStepValidation.success
                ? passwordStepValidation.data
                : SIGN_UP_CREATE_PASSWORD_STEP_INITIAL_VALUES),
        };
    },
    beforeLoad({ search }) {
        let sponsorId = search?.sponsorId ?? '';

        if (search?.subId) {
            sponsorId = search.subId.toString().replace(EXTRACT_NUMBER_REGEX, '');
        }

        return {
            ...search,
            sponsorId,
        };
    },
    async loader({ context: { sponsorId, ...rest } }) {
        if (!sponsorId) {
            return {
                isWithSponsor: false,
                sponsorId: '',
                sponsorName: '',
                ...rest,
            };
        }

        try {
            const sponsor = await getAffiliateById(sponsorId);

            return {
                isWithSponsor: !!sponsor,
                sponsorId,
                sponsorName: sponsor?.firstname ?? '',
                ...rest,
            };
        } catch {
            return {
                isWithSponsor: false,
                sponsorId: '',
                sponsorName: '',
                ...rest,
            };
        }
    },
    pendingComponent: PageLoader,
});
