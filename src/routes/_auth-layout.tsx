import { logOut } from '@/api/auth';
import AuthLayout from '@/components/layouts/AuthLayout';
import { Outlet, createFileRoute } from '@tanstack/react-router';

export const Route = createFileRoute('/_auth-layout')({
    component() {
        return (
            <AuthLayout>
                <Outlet />
            </AuthLayout>
        );
    },
    async beforeLoad() {
        await logOut();
    },
});
