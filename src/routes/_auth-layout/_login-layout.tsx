import Login from '@/modules/Login';
import { Outlet, createFileRoute } from '@tanstack/react-router';

export const Route = createFileRoute('/_auth-layout/_login-layout')({
    component() {
        return (
            <>
                <Login />
                <Outlet />
            </>
        );
    },
});
