import { createLazyFileRoute } from '@tanstack/react-router';
import SignUpSuccess from '@/modules/SignUp/components/Success';

export const Route = createLazyFileRoute('/_auth-layout/sign-up/success')({
    component: SignUpSuccess,
});
