import { createLazyFileRoute } from '@tanstack/react-router';

export const Route = createLazyFileRoute('/_auth-layout/_login-layout/login/')({
    component() {
        return null;
    },
});
