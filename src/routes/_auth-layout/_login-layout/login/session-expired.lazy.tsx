import { createLazyFileRoute } from '@tanstack/react-router';
import SessionExpiredAlertDialog from '@/modules/Login/components/SessionExpiredAlertDialog';

export const Route = createLazyFileRoute('/_auth-layout/_login-layout/login/session-expired')({
    component: SessionExpiredAlertDialog,
});
