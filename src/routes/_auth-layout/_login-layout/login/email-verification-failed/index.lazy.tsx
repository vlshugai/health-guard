import { createLazyFileRoute } from '@tanstack/react-router';
import EmailVerificationFailedAlertDialog from '@/modules/Login/components/EmailVerificationFailedAlertDialog';

export const Route = createLazyFileRoute('/_auth-layout/_login-layout/login/email-verification-failed/')({
    component: EmailVerificationFailedAlertDialog,
});
