import { createFileRoute } from '@tanstack/react-router';
import type { EmailVerificationFailedSearchParams } from '@/modules/Login/components/EmailVerificationFailedAlertDialog/types';
import { emailVerificationFailedSearchParamsSchema } from '@/modules/Login/components/EmailVerificationFailedAlertDialog/schemas';

export const Route = createFileRoute('/_auth-layout/_login-layout/login/email-verification-failed/')({
    component() {
        return null;
    },
    validateSearch(search: Record<string, string>): EmailVerificationFailedSearchParams {
        return emailVerificationFailedSearchParamsSchema.parse(search);
    },
});
