import { createFileRoute, redirect } from '@tanstack/react-router';
import type { VerifyEmailSearchParams } from '@/modules/VerifyEmail/types';
import { verifyUser } from '@/api/firebase';
import { verifyEmailSearchParamsSchema } from '@/modules/VerifyEmail/schemas';

export const Route = createFileRoute('/_auth-layout/_login-layout/login/verify-email')({
    component() {
        return null;
    },
    validateSearch(search: Record<string, string>): VerifyEmailSearchParams {
        const verifyEmailValidation = verifyEmailSearchParamsSchema.safeParse(search);

        if (!verifyEmailValidation.success) {
            throw redirect({
                to: '/login/email-verification-failed',
                search: {
                    message: 'Invalid email. Please try again.',
                },
            });
        }

        return verifyEmailValidation.data;
    },
    async beforeLoad({ search }) {
        const verifyResponse = await verifyUser(search.email);

        if (!verifyResponse.success) {
            throw redirect({
                to: '/login/email-verification-failed',
                search: {
                    message: verifyResponse.message,
                },
            });
        }

        throw redirect({
            to: '/login/email-verification-success',
        });
    },
});
