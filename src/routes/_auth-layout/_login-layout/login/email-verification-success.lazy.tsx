import { createLazyFileRoute } from '@tanstack/react-router';
import EmailVerificationSuccessAlertDialog from '@/modules/Login/components/EmailVerificationSuccessAlertDialog';

export const Route = createLazyFileRoute('/_auth-layout/_login-layout/login/email-verification-success')({
    component: EmailVerificationSuccessAlertDialog,
});
