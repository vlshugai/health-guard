import { createFileRoute, redirect } from '@tanstack/react-router';
import type { ResetPasswordCreateNewPasswordSearchParams } from '@/modules/ResetPassword/components/CreateNewPassword/types';
import { getUserByEmail } from '@/api/firebase';
import { resetPasswordCreateNewPasswordSearchParamsSchema } from '@/modules/ResetPassword/components/CreateNewPassword/schemas';

export const Route = createFileRoute('/_auth-layout/reset-password/create-new-password/')({
    component() {
        return null;
    },
    validateSearch(search: Record<string, string>): ResetPasswordCreateNewPasswordSearchParams {
        const resetPasswordCreateNewPasswordValidation =
            resetPasswordCreateNewPasswordSearchParamsSchema.safeParse(search);

        if (!resetPasswordCreateNewPasswordValidation.success) {
            throw redirect({
                to: '/reset-password',
            });
        }

        return resetPasswordCreateNewPasswordValidation.data;
    },
    async beforeLoad({ search }) {
        const userToResetPassword = await getUserByEmail(search.email);

        if (!userToResetPassword) {
            throw redirect({
                to: '/reset-password',
            });
        }
    },
});
