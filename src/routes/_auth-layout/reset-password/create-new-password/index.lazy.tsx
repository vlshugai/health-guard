import { createLazyFileRoute } from '@tanstack/react-router';

export const Route = createLazyFileRoute('/_auth-layout/reset-password/create-new-password/')({
    component() {
        return <div>Hello /_auth-layout/reset-password/create-new-password/!</div>;
    },
});
