import { createLazyFileRoute } from '@tanstack/react-router';
import ResetPassword from '@/modules/ResetPassword';

export const Route = createLazyFileRoute('/_auth-layout/reset-password/')({
    component: ResetPassword,
});
