import { createLazyFileRoute } from '@tanstack/react-router';
import ComingSoon from '@/modules/ComingSoon';

export const Route = createLazyFileRoute('/_dashboard-layout/book-appointment')({
    component: ComingSoon,
});
