import { createFileRoute } from '@tanstack/react-router';
import { myTeamSearchParamsSchema } from '@/modules/MyTeam/schemas';
import type { MyTeamSearchParams } from '@/modules/MyTeam/types';

export const Route = createFileRoute('/_dashboard-layout/my-team/')({
    component() {
        return null;
    },
    validateSearch(search: Record<string, string>): MyTeamSearchParams {
        const myTeamSearchValidation = myTeamSearchParamsSchema.parse(search);

        return myTeamSearchValidation;
    },
});
