import { createLazyFileRoute } from '@tanstack/react-router';
import MyTeam from '@/modules/MyTeam';

export const Route = createLazyFileRoute('/_dashboard-layout/my-team/')({
    component: MyTeam,
});
