import { createFileRoute } from '@tanstack/react-router';
import type { DashboardSearchParams } from '@/modules/Dashboard/types';
import {
    dashboardClientReferralsSearchParamsSchema,
    dashboardDealsStatsSearchParamsSchema,
} from '@/modules/Dashboard/schemas';

export const Route = createFileRoute('/_dashboard-layout/dashboard/')({
    component: () => {
        return null;
    },
    validateSearch(search: Record<string, string>): DashboardSearchParams {
        const dealsStatsFilterValidation = dashboardDealsStatsSearchParamsSchema.parse(search);
        const clientReferralsFilterValidation = dashboardClientReferralsSearchParamsSchema.parse(search);

        return {
            ...dealsStatsFilterValidation,
            ...clientReferralsFilterValidation,
        };
    },
});
