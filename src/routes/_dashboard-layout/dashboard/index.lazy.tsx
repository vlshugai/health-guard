import { createLazyFileRoute } from '@tanstack/react-router';
import Dashboard from '@/modules/Dashboard';

export const Route = createLazyFileRoute('/_dashboard-layout/dashboard/')({
    component: Dashboard,
});
