import { lazy, Suspense } from 'react';
import { createRootRoute, Outlet } from '@tanstack/react-router';
import { IS_DEV } from '@/constants';

const TanStackRouterDevtools = IS_DEV
    ? lazy(() => {
          // Lazy load in development
          return import('@tanstack/router-devtools').then((res) => {
              return {
                  default: res.TanStackRouterDevtools,
                  // For Embedded Mode
                  // default: res.TanStackRouterDevtoolsPanel
              };
          });
      })
    : () => {
          return null;
      }; // Render nothing in production

export const Route = createRootRoute({
    component() {
        return (
            <>
                <Outlet />
                <Suspense>
                    <TanStackRouterDevtools />
                </Suspense>
            </>
        );
    },
});
