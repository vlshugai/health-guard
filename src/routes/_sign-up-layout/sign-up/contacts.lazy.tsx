import { createLazyFileRoute } from '@tanstack/react-router';
import ContactsForm from '@/modules/SignUp/components/ContactsForm';

export const Route = createLazyFileRoute('/_sign-up-layout/sign-up/contacts')({
    component: ContactsForm,
});
