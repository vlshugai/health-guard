import { Outlet, createFileRoute, redirect } from '@tanstack/react-router';
import { signUpContactFormSchema } from '@/modules/SignUp/schemas';
import PaymentsForm from '@/modules/SignUp/components/PaymentsForm';

export const Route = createFileRoute('/_sign-up-layout/sign-up/_payments-layout')({
    component: () => {
        return (
            <>
                <PaymentsForm />
                <Outlet />
            </>
        );
    },
    beforeLoad({ search }) {
        const contactStepValidation = signUpContactFormSchema.safeParse(search);

        if (!contactStepValidation.success) {
            throw redirect({
                to: '/sign-up/contacts',
                search,
            });
        }
    },
});
