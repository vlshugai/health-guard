import { createLazyFileRoute } from '@tanstack/react-router';
import ReferralAgreementAlertDialog from '@/modules/SignUp/components/ReferralAgreementAlertDialog';

export const Route = createLazyFileRoute('/_sign-up-layout/sign-up/_payments-layout/payments/affiliate-agreement')({
    component: ReferralAgreementAlertDialog,
});
