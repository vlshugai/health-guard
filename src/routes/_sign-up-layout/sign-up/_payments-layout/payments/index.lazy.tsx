import { createLazyFileRoute } from '@tanstack/react-router';

export const Route = createLazyFileRoute('/_sign-up-layout/sign-up/_payments-layout/payments/')({
    component() {
        return null;
    },
});
