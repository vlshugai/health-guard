import { createFileRoute, redirect } from '@tanstack/react-router';

export const Route = createFileRoute('/_sign-up-layout/sign-up/')({
    component() {
        return null;
    },
    beforeLoad({ search }) {
        throw redirect({
            to: '/sign-up/contacts',
            search,
        });
    },
});
