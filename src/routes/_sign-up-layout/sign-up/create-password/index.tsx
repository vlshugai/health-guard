import { createFileRoute, redirect } from '@tanstack/react-router';
import { signUpContactFormSchema } from '@/modules/SignUp/schemas';

export const Route = createFileRoute('/_sign-up-layout/sign-up/create-password/')({
    component() {
        return null;
    },
    beforeLoad({ search }) {
        const contactStepValidation = signUpContactFormSchema.safeParse(search);

        if (!contactStepValidation.success) {
            throw redirect({
                to: '/sign-up/contacts',
                search,
            });
        }
    },
});
