import { createLazyFileRoute } from '@tanstack/react-router';
import CreatePasswordForm from '@/modules/SignUp/components/CreatePasswordForm';

export const Route = createLazyFileRoute('/_sign-up-layout/sign-up/create-password/')({
    component: CreatePasswordForm,
});
