export const getObjectValueForZodEnum = <T extends Record<string, unknown>>(value: T) => {
    return Object.values(value) as [(typeof value)[keyof T]];
};
