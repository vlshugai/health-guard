export const capitalize = (value: string) => {
    return value
        .trim()
        .split(' ')
        .map((word) => {
            return `${word.charAt(0).toUpperCase()}${word.slice(1).toLowerCase()}`;
        })
        .join(' ');
};
