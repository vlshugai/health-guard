export const createTemplateString = (template: string, params: string[]) => {
    const regex = /\{(\d+)\}/g;

    return template.replace(regex, (match, index) => {
        if (index >= 0 && index < params.length) {
            return params[index];
        }

        return match;
    });
};
