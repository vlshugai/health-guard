import z from 'zod';

export const convertFormSchemaToSearchParamsSchema = <TShape extends z.ZodRawShape>(schema: z.ZodObject<TShape>) => {
    return z.object(
        Object.fromEntries(
            Object.keys(schema.shape).map((key) => {
                return [key, z.string().optional().default('')];
            })
        )
    ) as unknown as z.ZodObject<{ [K in keyof TShape]: z.ZodString }>;
};
