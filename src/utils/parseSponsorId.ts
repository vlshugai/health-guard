import { SPONSOR_MARKETING_ID_PREFIX, EXTRACT_NUMBER_REGEX } from '@/constants';

export const parseSponsorId = (id?: string) => {
    if (!id) {
        return '';
    }

    const isAffiliateSponsorId = id.startsWith(SPONSOR_MARKETING_ID_PREFIX);

    if (!isAffiliateSponsorId) {
        return '';
    }

    return id.replace(EXTRACT_NUMBER_REGEX, '');
};
