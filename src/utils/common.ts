import type { DropdownMenuOptionType } from '@/components/DropdownMenu';

export const noop = () => {};

export const intlNumberFormat = (value: number, precision = 2): string => {
    return new Intl.NumberFormat('en-US', {
        style: 'decimal',
        minimumFractionDigits: precision,
        maximumFractionDigits: precision,
    }).format(value);
};

export const splitObjectToObjectAndArrayValues = <TObject extends Record<string, DropdownMenuOptionType>>(
    value: TObject
) => {
    return {
        optionsObject: value,
        optionsArray: Object.values(value).sort((current, other) => {
            return (current?.order ?? 0) - (other?.order ?? 0);
        }),
    };
};
