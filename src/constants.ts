/**
 * Environment variables
 */
export const IS_DEV = import.meta.env.DEV;
export const API_URL = import.meta.env.VITE_API_URL;
export const BASE_PUBLIC_PATH = import.meta.env.VITE_BASE_PUBLIC_PATH;

export const HUBSPOT_FORMS_PORTAL_ID = import.meta.env.VITE_HUBSPOT_FORMS_PORTAL_ID;
export const HUBSPOT_FORMS_GUID = import.meta.env.VITE_HUBSPOT_FORMS_GUID;

export const FIREBASE_API_KEY = import.meta.env.VITE_FIREBASE_API_KEY;
export const FIREBASE_AUTH_DOMAIN = import.meta.env.VITE_FIREBASE_AUTH_DOMAIN;
export const FIREBASE_PROJECT_ID = import.meta.env.VITE_FIREBASE_PROJECT_ID;
export const FIREBASE_STORAGE_BUCKET = import.meta.env.VITE_FIREBASE_STORAGE_BUCKET;
export const FIREBASE_MESSAGING_SENDER_ID = import.meta.env.VITE_FIREBASE_MESSAGING_SENDER_ID;
export const FIREBASE_APP_ID = import.meta.env.VITE_FIREBASE_APP_ID;
export const FIREBASE_DATABASE_URL = import.meta.env.VITE_FIREBASE_DATABASE_URL;

export const FIREBASE_AUTH_EMAIL = import.meta.env.VITE_FIREBASE_AUTH_EMAIL;
export const FIREBASE_AUTH_PASSWORD = import.meta.env.VITE_FIREBASE_AUTH_PASSWORD;

/**
 * Common constants
 */
export const STATUS_CODE_UNAUTHORIZED = 401;
export const STATUS_CODE_FORBIDDEN = 403;
export const ERR_CANCELLED = 'AbortError';

export const LOCK_SCROLL_Y_CLASS_NAME = 'lock-scroll-y';

export const AFFILIATE_MARKETING_ID_PREFIX = 'aff';
export const SPONSOR_MARKETING_ID_PREFIX = 'spn';
export const DEFAULT_TRANSITION = { type: 'linear', duration: 0.15 };

export const LOWERCASE_AND_UPPERCASE_REGEX = /(?=.*[a-z])(?=.*[A-Z])/;
export const SPECIAL_SYMBOLS_REGEX = /(?=.*[@$!%*#?&])/;
export const WHITESPACE_REGEX = /^\S+$/;
export const EXTRACT_NUMBER_REGEX = /\D+/g;

export const COMMON_REQUIRED_ERROR = 'This field is required.';
export const MIN_NAME_LENGTH = 3;
export const MAX_NAME_LENGTH = 50;
export const NAME_MIN_LENGTH_ERROR = 'The name must be at least {0} characters.';
export const NAME_MAX_LENGTH_ERROR = 'The name must be no more than {0} characters.';
export const EMAIL_ERROR = 'This mailbox isn’t correct';
export const MIN_PASSWORD_LENGTH = 8;
export const MAX_PASSWORD_LENGTH = 100;
export const PASSWORD_MIN_LENGTH_ERROR = 'The password must be at least {0} characters long.';
export const PASSWORD_MAX_LENGTH_ERROR = 'The password must be at max {0} characters long.';
export const MIN_PHONE_LENGTH = 10;
export const MAX_PHONE_LENGTH = 14;
export const PHONE_MIN_LENGTH_ERROR = 'The phone must be at least {0} numbers.';
export const PHONE_MAX_LENGTH_ERROR = 'The phone must be no more than {0} numbers.';
export const PHONE_ERROR = 'Phone number isn’t correct';
export const PASSWORD_TIP = 'Passwords must have at least one digit.';
export const PASSWORD_TIP_NOT_WHITESPACE = 'Passwords must not contain spaces.';
export const FIELD_PASSWORD_LOWERCASE_AND_UPPERCASE_TIP = 'Tip: include uppercase and lowercase characters.';
export const FIELD_PASSWORD_SPECIAL_SYMBOLS_TIP = 'Tip: include special character.';
export const FIELD_PASSWORD_MATCH = 'Password doesn’t match';
export const FILE_MAX_SIZE_ERROR = 'File size is too large';
export const FILE_FORMAT_ERROR = 'File has not correct format';

export const ONE_MINUTE = 1 * 60 * 1000;

export const LS_AUTH_TOKEN = 'healthcare:auth-token';

export const JWT_ALG = 'HS256';
export const HASH_SALT = 10;
export const JWT_EXPIRATION_TIME = '2h';

export const SECRET_KEY = new TextEncoder().encode('cc7e0d44fd473002f1c42167459001140ec6389b7353f8088f4d9a95f2f596f2');
