import z from 'zod';
import { resetPasswordFormSchema } from './schemas';

export type ResetPasswordForm = z.infer<typeof resetPasswordFormSchema>;
