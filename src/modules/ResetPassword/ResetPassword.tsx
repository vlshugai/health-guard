import { useNavigate } from '@tanstack/react-router';
import { Root as AspectRatioRoot } from '@radix-ui/react-aspect-ratio';
import clsx from 'clsx';
import { useGetUserByEmail } from '@/hooks/api/useFirebaseApi';
import { useResetPasswordForm } from './hooks/useResetPasswordForm';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import Input from '@/ui/Input';
import Icon from '@/ui/Icon';
import s from './ResetPassword.module.css';

const ResetPassword: React.FC = () => {
    const navigate = useNavigate({ from: '/reset-password' });

    const {
        emailController: {
            field: emailField,
            fieldState: { error: emailError },
        },
        setError,
        handleSubmit,
    } = useResetPasswordForm();

    const { refetch: getUserByEmail } = useGetUserByEmail(emailField.value, false);

    const submitHandler = handleSubmit(async (data) => {
        const { data: userFromDatabase } = await getUserByEmail();

        if (!userFromDatabase) {
            setError('email', {
                message: 'User with this email does not exist',
            });
            return;
        }

        // TODO: send email with reset password link
        navigate({
            to: '/reset-password/create-new-password',
            search: {
                email: data.email,
            },
        });
    });

    return (
        <div className={s.wrap}>
            <div className={s.left} aria-hidden>
                <AspectRatioRoot className={s['image-wrap']} ratio={1.33 / 1}>
                    <Icon className={clsx(s.icon, s.lock)} id="icon-lock" />
                    <img
                        className={clsx(s.image, s['logo-transparent'])}
                        src="/images/logo-transparent.webp"
                        alt="Healthguard Transparent logo in bottom left corner"
                    />
                </AspectRatioRoot>
            </div>
            <div className={s.right}>
                <form className={s.form} onSubmit={submitHandler} noValidate>
                    <Typography className={s.title} variant="title-h1" asChild>
                        <h1>Reset your password</h1>
                    </Typography>
                    <Typography className={s.subtitle} variant="paragraph-sm" asChild>
                        <h3>
                            Enter the email associated with your account and we’ll send an email to reset your password.
                        </h3>
                    </Typography>
                    <div className={s.fields}>
                        <Input
                            className={s.field}
                            type="email"
                            label="Email"
                            placeholder="Enter your email"
                            value={emailField.value}
                            onChange={emailField.onChange}
                            errorMessage={emailError?.message}
                        />
                    </div>
                    <footer className={s.footer}>
                        <Button
                            className={clsx(s.cta, s.submit)}
                            variant="primary"
                            // disabled={isLogInPending}
                        >
                            Send an email
                        </Button>
                    </footer>
                </form>
            </div>
        </div>
    );
};

export default ResetPassword;
