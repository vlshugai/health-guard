import z from 'zod';
import { resetPasswordCreateNewPasswordSearchParamsSchema } from './schemas';

export type ResetPasswordCreateNewPasswordSearchParams = z.infer<
    typeof resetPasswordCreateNewPasswordSearchParamsSchema
>;
