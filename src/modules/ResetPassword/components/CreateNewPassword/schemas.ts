import z from 'zod';
import { emailSchema } from '@/schemas';

export const resetPasswordCreateNewPasswordSearchParamsSchema = z.object({
    email: emailSchema,
});
