import z from 'zod';
import { emailSchema } from '@/schemas';

export const resetPasswordFormSchema = z.object({
    email: emailSchema,
});
