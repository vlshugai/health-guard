import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import type { ResetPasswordForm } from '../types';
import { useFieldController } from '@/hooks/useFieldController';
import { resetPasswordFormSchema } from '../schemas';

export const useResetPasswordForm = () => {
    const { control, ...rest } = useForm<ResetPasswordForm>({
        resolver: zodResolver(resetPasswordFormSchema),
        defaultValues: {
            email: '',
        },
        reValidateMode: 'onChange',
    });

    const emailController = useFieldController({
        control,
        name: 'email',
    });

    return {
        emailController,
        ...rest,
    };
};
