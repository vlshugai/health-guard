import clsx from 'clsx';
import s from './PageLoader.module.css';

const PageLoader: React.FC = () => {
    return (
        <div className={clsx(s.wrap, 'full-height')}>
            <div className={s['loader-wrap']}>
                <img className={s.loader} src="/images/icon-loader.svg" alt="Loader" aria-hidden />
            </div>
            <span className="visually-hidden" aria-label="Loading">
                Loading
            </span>
        </div>
    );
};

export default PageLoader;
