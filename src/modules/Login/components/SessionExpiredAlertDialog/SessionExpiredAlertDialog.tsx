import AlertDialog from '@/components/AlertDialog/AlertDialog';
import { Link } from '@tanstack/react-router';
import {
    Title as AlertDialogTitle,
    Description as AlertDialogDescription,
    Action as AlertDialogAction,
} from '@radix-ui/react-alert-dialog';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import s from './SessionExpiredAlertDialog.module.css';

const SessionExpiredAlertDialog: React.FC = () => {
    return (
        <AlertDialog>
            <header className={s.header}>
                <div className={s['image-wrap']}>
                    <img src="/images/laptop-dead.webp" alt="Your session has expired" />
                </div>
                <AlertDialogTitle asChild>
                    <Typography className={s.title} variant="title-h4" asChild>
                        <h1>Your session has expired</h1>
                    </Typography>
                </AlertDialogTitle>
                <AlertDialogDescription className={s.subtitle} asChild>
                    <Typography variant="paragraph-base" asChild>
                        <span>
                            Please refresh the page. Don’t worry, we kept all of your filters and breakdowns in place.
                        </span>
                    </Typography>
                </AlertDialogDescription>
            </header>
            <footer>
                <AlertDialogAction asChild>
                    <Button variant="primary" asChild>
                        <Link to="/login">
                            <Typography variant="paragraph-base" asChild>
                                <span>Refresh</span>
                            </Typography>
                        </Link>
                    </Button>
                </AlertDialogAction>
            </footer>
        </AlertDialog>
    );
};

export default SessionExpiredAlertDialog;
