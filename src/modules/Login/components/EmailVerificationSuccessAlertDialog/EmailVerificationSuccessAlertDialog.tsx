import AlertDialog from '@/components/AlertDialog/AlertDialog';
import {
    Title as AlertDialogTitle,
    Description as AlertDialogDescription,
    Action as AlertDialogAction,
} from '@radix-ui/react-alert-dialog';
import { Link } from '@tanstack/react-router';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import s from './EmailVerificationSuccessAlertDialog.module.css';

const EmailVerificationSuccessAlertDialog: React.FC = () => {
    return (
        <AlertDialog>
            <header className={s.header}>
                <div className={s['image-wrap']}>
                    <img src="/images/octagon-blue-check.webp" alt="Email verified!" />
                </div>
                <AlertDialogTitle asChild>
                    <Typography className={s.title} variant="title-h4" asChild>
                        <h1>Email verified!</h1>
                    </Typography>
                </AlertDialogTitle>
                <AlertDialogDescription className={s.subtitle} asChild>
                    <Typography variant="paragraph-base" asChild>
                        <span>
                            Your email has been verified by HealthGuard. Now you can log by the email and password.
                        </span>
                    </Typography>
                </AlertDialogDescription>
            </header>
            <footer>
                <AlertDialogAction asChild>
                    <Button variant="primary" asChild>
                        <Link to="/login">
                            <Typography variant="paragraph-base" asChild>
                                <span>Finish</span>
                            </Typography>
                        </Link>
                    </Button>
                </AlertDialogAction>
            </footer>
        </AlertDialog>
    );
};

export default EmailVerificationSuccessAlertDialog;
