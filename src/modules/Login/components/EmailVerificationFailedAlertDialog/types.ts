import z from 'zod';
import { emailVerificationFailedSearchParamsSchema } from './schemas';

export type EmailVerificationFailedSearchParams = z.infer<typeof emailVerificationFailedSearchParamsSchema>;
