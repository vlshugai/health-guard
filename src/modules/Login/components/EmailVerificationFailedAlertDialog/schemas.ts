import z from 'zod';
import { EMAIL_VERIFICATION_FAILED_DEFAULT_ERROR_MESSAGE } from './constants';

export const emailVerificationFailedSearchParamsSchema = z.object({
    message: z
        .string()
        .default(EMAIL_VERIFICATION_FAILED_DEFAULT_ERROR_MESSAGE)
        .catch(EMAIL_VERIFICATION_FAILED_DEFAULT_ERROR_MESSAGE),
});
