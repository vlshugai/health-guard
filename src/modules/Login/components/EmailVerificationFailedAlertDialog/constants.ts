export const EMAIL_VERIFICATION_FAILED_DEFAULT_ERROR_MESSAGE = 'We failed to verify your email. Please try again.';
