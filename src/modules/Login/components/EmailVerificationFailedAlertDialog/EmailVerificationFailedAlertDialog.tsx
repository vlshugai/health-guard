import AlertDialog from '@/components/AlertDialog/AlertDialog';
import {
    Title as AlertDialogTitle,
    Description as AlertDialogDescription,
    Action as AlertDialogAction,
} from '@radix-ui/react-alert-dialog';
import { Link, getRouteApi } from '@tanstack/react-router';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import s from './EmailVerificationFailedAlertDialog.module.css';

const routeApi = getRouteApi('/_auth-layout/_login-layout/login/email-verification-failed/');

const EmailVerificationFailedAlertDialog: React.FC = () => {
    const search = routeApi.useSearch();

    return (
        <AlertDialog>
            <header className={s.header}>
                <div className={s['image-wrap']}>
                    <img src="/images/octagon-red-cross.webp" alt="Verification failed!" />
                </div>
                <AlertDialogTitle asChild>
                    <Typography className={s.title} variant="title-h4" asChild>
                        <h1>Verification failed!</h1>
                    </Typography>
                </AlertDialogTitle>
                <AlertDialogDescription className={s.subtitle} asChild>
                    <Typography variant="paragraph-base" asChild>
                        <span>{search.message}</span>
                    </Typography>
                </AlertDialogDescription>
            </header>
            <footer>
                <AlertDialogAction asChild>
                    <Button variant="primary" asChild>
                        <Link to="/login">
                            <Typography variant="paragraph-base" asChild>
                                <span>Close</span>
                            </Typography>
                        </Link>
                    </Button>
                </AlertDialogAction>
            </footer>
        </AlertDialog>
    );
};

export default EmailVerificationFailedAlertDialog;
