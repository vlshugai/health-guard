import z from 'zod';
import { loginSchema } from './schemas';

export type LoginFormType = z.infer<typeof loginSchema>;
