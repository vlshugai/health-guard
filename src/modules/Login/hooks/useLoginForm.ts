import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import type { LoginFormType } from '../types';
import { useFieldController } from '@/hooks/useFieldController';
import { loginSchema } from '../schemas';

export const useLoginForm = () => {
    const [globalError, setGlobalError] = useState('');

    const { control, ...rest } = useForm<LoginFormType>({
        resolver: zodResolver(loginSchema),
        defaultValues: {
            email: '',
            password: '',
        },
        reValidateMode: 'onChange',
    });

    const emailController = useFieldController(
        {
            control,
            name: 'email',
        },
        () => {
            rest.clearErrors('email');
            setGlobalError('');
        }
    );

    const passwordController = useFieldController(
        {
            control,
            name: 'password',
        },
        () => {
            rest.clearErrors('password');
            setGlobalError('');
        }
    );

    return {
        emailController,
        passwordController,
        globalError,
        setGlobalError,
        ...rest,
    };
};
