import { Link, useNavigate } from '@tanstack/react-router';
import clsx from 'clsx';
import { useLogin } from '@/hooks/api/useAuthApi';
import { useLoginForm } from './hooks/useLoginForm';
import { ALL_VALUE } from '@/modules/Dashboard/constants';
import ErrorMessage from '@/ui/ErrorMessage';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import Input from '@/ui/Input';
import s from './Login.module.css';

const Login: React.FC = () => {
    const navigate = useNavigate({ from: '/login' });

    const {
        emailController: {
            field: emailField,
            fieldState: { error: emailError },
        },
        passwordController: {
            field: passwordField,
            fieldState: { error: passwordError },
        },
        globalError,
        handleSubmit,
        setGlobalError,
    } = useLoginForm();

    const { mutateAsync: logIn, isPending: isLogInPending } = useLogin();

    const submitHandler = handleSubmit(async (data) => {
        try {
            const response = await logIn(data);

            if (!response.success) {
                setGlobalError(response.message);
                return;
            }

            navigate({
                to: '/dashboard',
                search: {
                    commissionStatus: ALL_VALUE,
                    createdDate: ALL_VALUE,
                    dealStatus: ALL_VALUE,
                    level: ALL_VALUE,
                    soldReferrals: ALL_VALUE,
                    totalReferrals: ALL_VALUE,
                    totalCommissions: ALL_VALUE,
                },
            });
        } catch (e) {
            const error = e as Error;
            // eslint-disable-next-line no-console
            console.error(error);
        }
    });

    return (
        <div className={s.wrap}>
            <div className={s.left}>
                <div className={s['image-wrap']}>
                    <img
                        className={s.image}
                        src="/images/login-graphics.webp"
                        alt="Two men shake hands after successful deal"
                    />
                </div>
            </div>
            <div className={s.right}>
                <form className={s.form} onSubmit={submitHandler} noValidate>
                    <Typography className={s.title} variant="title-h1" asChild>
                        <h1>Login</h1>
                    </Typography>
                    <div className={s.fields}>
                        <Input
                            className={s.field}
                            type="email"
                            label="Email"
                            placeholder="Enter your email"
                            value={emailField.value}
                            disabled={isLogInPending}
                            onChange={emailField.onChange}
                            errorMessage={emailError?.message}
                        />
                        <Input
                            className={s.field}
                            type="password"
                            label="Password"
                            placeholder="Password"
                            value={passwordField.value}
                            disabled={isLogInPending}
                            onChange={passwordField.onChange}
                            errorMessage={passwordError?.message}
                        />
                    </div>
                    {globalError ? <ErrorMessage className={s['error-wrap']}>{globalError}</ErrorMessage> : null}
                    <Typography className={clsx(s['forgot-password-link'], 'focus-primary')} variant="link" asChild>
                        <Link to="/" search={{}}>
                            Forgot password?
                        </Link>
                    </Typography>
                    <footer className={s.footer}>
                        <Button className={clsx(s.cta, s.login)} variant="primary" disabled={isLogInPending}>
                            Log in
                        </Button>
                        <Button
                            className={clsx(s.cta, s['sign-up'])}
                            variant="outline"
                            disabled={isLogInPending}
                            asChild
                        >
                            <Link to="/sign-up" search>
                                Become an Affiliate
                            </Link>
                        </Button>
                    </footer>
                </form>
            </div>
        </div>
    );
};

export default Login;
