import z from 'zod';
import { emailSchema, stringSchema } from '@/schemas';

export const loginSchema = z.object({
    email: emailSchema,
    password: stringSchema,
});
