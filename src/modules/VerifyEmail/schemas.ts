import z from 'zod';
import { emailSchema } from '@/schemas';

export const verifyEmailSearchParamsSchema = z.object({
    email: emailSchema,
});
