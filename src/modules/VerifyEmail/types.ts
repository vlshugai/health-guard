import z from 'zod';
import { verifyEmailSearchParamsSchema } from './schemas';

export type VerifyEmailSearchParams = z.infer<typeof verifyEmailSearchParamsSchema>;
