import z from 'zod';
import { getObjectValueForZodEnum } from '@/utils/getObjectValueForZodEnum';
import {
    ALL_VALUE,
    DEALS_CREATED_DATE_OPTIONS_IDS,
    DEAL_COMMISSION_LEVEL_OPTIONS_IDS,
    DEAL_STATUS_CODES,
    TIER_LEVEL_SELECT_OPTIONS_IDS,
} from './constants';

export const tierLevelSchema = z
    .enum(getObjectValueForZodEnum(TIER_LEVEL_SELECT_OPTIONS_IDS))
    .default(ALL_VALUE)
    .catch(ALL_VALUE);

export const dashboardDealsStatsSearchParamsSchema = z.object({
    totalReferrals: tierLevelSchema,
    soldReferrals: tierLevelSchema,
    totalCommissions: tierLevelSchema,
});

export const dashboardClientReferralsSearchParamsSchema = z.object({
    createdDate: z
        .enum([ALL_VALUE, ...getObjectValueForZodEnum(DEALS_CREATED_DATE_OPTIONS_IDS)])
        .default(ALL_VALUE)
        .catch(ALL_VALUE),
    level: tierLevelSchema,
    dealStatus: z
        .enum([ALL_VALUE, ...getObjectValueForZodEnum(DEAL_STATUS_CODES)])
        .default(ALL_VALUE)
        .catch(ALL_VALUE),
    commissionStatus: z
        .enum([ALL_VALUE, ...getObjectValueForZodEnum(DEAL_COMMISSION_LEVEL_OPTIONS_IDS)])
        .default(ALL_VALUE)
        .catch(ALL_VALUE),
});
