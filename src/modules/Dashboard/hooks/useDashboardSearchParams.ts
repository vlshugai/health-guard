import { getRouteApi } from '@tanstack/react-router';

const dashboardRouteApi = getRouteApi('/_dashboard-layout/dashboard/');

export const useDashboardSearchParams = () => {
    const search = dashboardRouteApi.useSearch();

    return search;
};
