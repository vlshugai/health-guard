export const ALL_VALUE = 'all' as const;

export const ALL_OPTION = {
    label: 'All',
    value: ALL_VALUE,
    order: 0,
} as const;

export const DEALS_CREATED_DATE_OPTIONS_IDS = {
    today: '0_today',
    last_7_days: '1_last_7_days',
    last_30_days: '2_last_30_days',
} as const;

export const DEAL_COMMISSION_LEVEL_OPTIONS_IDS = {
    unpaid: 'Unpaid',
    paid: 'Paid',
    paymentFailure: 'Payment Failure',
    noPaymentDue: 'No Payment Due',
} as const;

export const DEAL_STATUS_CODES = {
    created: '146607242',
    closed: '146607243',
    paid: '146607244',
    canceled: '146607245',
    ineligible: '160257059',
} as const;

export const TIER_LEVEL_SELECT_OPTIONS_IDS = {
    all: 'all',
    level_0: '0',
    level_1: '1',
    level_2: '2',
} as const;

export const DASHBOARD_TABLE_PAGE_SIZE = 25;
