import z from 'zod';
import type { ObjValues } from '@/types';
import { dashboardDealsStatsSearchParamsSchema, dashboardClientReferralsSearchParamsSchema } from './schemas';
import {
    ALL_VALUE,
    DEALS_CREATED_DATE_OPTIONS_IDS,
    DEAL_COMMISSION_LEVEL_OPTIONS_IDS,
    DEAL_STATUS_CODES,
    TIER_LEVEL_SELECT_OPTIONS_IDS,
} from './constants';

export type AllValue = typeof ALL_VALUE;

export type DashboardDealsStatsSearchParams = z.infer<typeof dashboardDealsStatsSearchParamsSchema>;
export type DashboardClientReferralsSearchParams = z.infer<typeof dashboardClientReferralsSearchParamsSchema>;

export type DashboardSearchParams = DashboardDealsStatsSearchParams & DashboardClientReferralsSearchParams;

export type DealStatusCode = ObjValues<typeof DEAL_STATUS_CODES>;
export type TierLevelSelectOptionIds = ObjValues<typeof TIER_LEVEL_SELECT_OPTIONS_IDS>;
export type DealsCreatedDateOptionIds = ObjValues<typeof DEALS_CREATED_DATE_OPTIONS_IDS>;
export type DealsLevelOptionsIds = Exclude<TierLevelSelectOptionIds, AllValue>;
export type DealsCommissionStatusOptionIds = ObjValues<typeof DEAL_COMMISSION_LEVEL_OPTIONS_IDS>;
