import DashboardClientReferrals from './components/DashboardClientReferrals';
import DashboardSummary from './components/DashboardSummary';
import s from './Dashboard.module.css';

const Dashboard: React.FC = () => {
    return (
        <div className={s.wrap}>
            <div className={s.inner}>
                <DashboardSummary />
                <DashboardClientReferrals />
            </div>
        </div>
    );
};

export default Dashboard;
