import type { DropdownMenuOptionType } from '@/components/DropdownMenu';
import type { TierLevelSelectOptionIds } from '../types';
import { TIER_LEVEL_SELECT_OPTIONS_IDS } from '../constants';
import { splitObjectToObjectAndArrayValues } from '@/utils/common';

export const getOptionsByTierLevel = (prefix: string) => {
    const options = {
        [TIER_LEVEL_SELECT_OPTIONS_IDS.all]: {
            label: `All ${prefix}`,
            value: TIER_LEVEL_SELECT_OPTIONS_IDS.all,
            order: 0,
        },
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_0]: {
            label: `My ${prefix}`,
            value: TIER_LEVEL_SELECT_OPTIONS_IDS.level_0,
            order: 1,
        },
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_1]: {
            label: `Level 1 ${prefix}`,
            value: TIER_LEVEL_SELECT_OPTIONS_IDS.level_1,
            order: 2,
        },
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_2]: {
            label: `Level 2 ${prefix}`,
            value: TIER_LEVEL_SELECT_OPTIONS_IDS.level_2,
            order: 3,
        },
    } as Record<TierLevelSelectOptionIds, DropdownMenuOptionType>;

    return splitObjectToObjectAndArrayValues(options);
};
