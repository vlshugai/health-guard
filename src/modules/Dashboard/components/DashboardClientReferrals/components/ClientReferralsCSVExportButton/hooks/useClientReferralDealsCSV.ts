import { useMemo } from 'react';
import type { Row } from '@tanstack/react-table';
import type { ClientReferralsTableRow } from '../../../types';
import { useMe } from '@/hooks/useMe';

export const useClientReferralDealsCSV = (rows: Row<ClientReferralsTableRow>[]) => {
    const user = useMe();

    const filename = `${user.id}_client_referrals.csv`;

    const data = useMemo(() => {
        return rows
            .map((row) => {
                return row.original;
            })
            .map((deal) => {
                return {
                    createdDate: deal.createdDate,
                    level: deal.level,
                    id: deal.id,
                    dealStatus: deal.dealStatus,
                    commissionStatus: deal.commissionStatus,
                    amount: deal.amount,
                };
            });
    }, [rows]);

    return { filename, data };
};
