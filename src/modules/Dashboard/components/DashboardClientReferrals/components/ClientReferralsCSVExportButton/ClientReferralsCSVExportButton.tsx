import { memo } from 'react';
import { CSVLink } from 'react-csv';
import type { ClientReferralsCSVExportButtonProps } from './types';
import { useClientReferralDealsCSV } from './hooks/useClientReferralDealsCSV';
import { CLIENT_REFERRALS_CSV_HEADER } from './constants';
import Button from '@/ui/Button';

const ClientReferralsCSVExportButton: React.FC<ClientReferralsCSVExportButtonProps> = ({ deals, ...rest }) => {
    const dealsCSV = useClientReferralDealsCSV(deals);

    return (
        <Button {...rest} variant="primary" asChild>
            <CSVLink {...dealsCSV} headers={CLIENT_REFERRALS_CSV_HEADER}>
                Export
            </CSVLink>
        </Button>
    );
};

export default memo(ClientReferralsCSVExportButton);
