export const CLIENT_REFERRALS_CSV_HEADER = [
    {
        key: 'createdDate',
        label: 'Created Date',
    },
    {
        key: 'level',
        label: 'Level',
    },
    {
        key: 'id',
        label: 'Deal ID',
    },
    {
        key: 'dealStatus',
        label: 'Deal Status',
    },
    {
        key: 'commissionStatus',
        label: 'Commission Status',
    },
    {
        key: 'amount',
        label: 'Earnings',
    },
];
