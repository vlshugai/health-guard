import type { Row } from '@tanstack/react-table';
import type { ButtonProps } from '@/ui/Button/types';
import type { ClientReferralsTableRow } from '../../types';

export type ClientReferralsCSVExportButtonProps = Omit<ButtonProps, 'ref'> & {
    deals: Row<ClientReferralsTableRow>[];
};
