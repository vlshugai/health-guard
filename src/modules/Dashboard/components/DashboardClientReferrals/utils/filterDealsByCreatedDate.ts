import { differenceInDays, isToday } from 'date-fns';
import type { AllValue, DealsCreatedDateOptionIds } from '../../../types';
import type { ClientReferralsTableRow } from '../types';
import { ALL_VALUE, DEALS_CREATED_DATE_OPTIONS_IDS } from '../../../constants';

export const filterDealsByCreatedDate = (filter?: DealsCreatedDateOptionIds | AllValue | null) => {
    return (deal: ClientReferralsTableRow) => {
        switch (filter) {
            case null:
            case ALL_VALUE: {
                return true;
            }
            case DEALS_CREATED_DATE_OPTIONS_IDS.today: {
                return isToday(new Date(deal.createdDate));
            }
            case DEALS_CREATED_DATE_OPTIONS_IDS.last_7_days: {
                return differenceInDays(new Date(), new Date(deal.createdDate)) <= 7;
            }
            case DEALS_CREATED_DATE_OPTIONS_IDS.last_30_days: {
                return differenceInDays(new Date(), new Date(deal.createdDate)) <= 30;
            }
            default: {
                return false;
            }
        }
    };
};
