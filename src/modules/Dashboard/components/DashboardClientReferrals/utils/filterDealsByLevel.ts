import type { AllValue, TierLevelSelectOptionIds } from '../../../types';
import type { ClientReferralsTableRow } from '../types';
import { ALL_VALUE } from '../../../constants';

export const filterDealsByLevel = (filter: TierLevelSelectOptionIds | AllValue | null) => {
    return (deal: ClientReferralsTableRow) => {
        if (!filter || filter === ALL_VALUE) {
            return true;
        }

        return deal.level.toString() === filter;
    };
};
