import type { AllValue, DealsCommissionStatusOptionIds } from '../../../types';
import type { ClientReferralsTableRow } from '../types';
import { ALL_VALUE } from '../../../constants';

export const filterDealsByCommissionStatus = (filter?: DealsCommissionStatusOptionIds | AllValue | null) => {
    return (deal: ClientReferralsTableRow) => {
        if (!filter || filter === ALL_VALUE) {
            return true;
        }

        return deal.commissionStatus === filter;
    };
};
