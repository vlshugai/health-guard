import type { HubspotDealItem } from '@/api/hubspotHealthGuard';
import type { HubspotContactTierLevel } from '@/types';
import type { ClientReferralsTableRow } from '../types';

export const generateDealTableRow = (
    deal: HubspotDealItem,
    level: HubspotContactTierLevel,
    overridePositionToGetCommissionAmount?: HubspotContactTierLevel
): ClientReferralsTableRow => {
    const amount = parseFloat(
        deal.properties?.[`deal_position_${overridePositionToGetCommissionAmount ?? level}_commission_amount`] ?? '0'
    );
    const commissionStatus =
        deal.properties?.[`deal_position_${overridePositionToGetCommissionAmount ?? level}_commission_payment_status`];
    const dealName = (deal.properties?.dealname ?? '').split('-')[1];

    return {
        id: deal.id,
        amount,
        commissionStatus,
        createdDate: deal.createdAt,
        dealStatus: deal.properties?.dealstage,
        dealName,
        level,
        isMyDeal: level === 0,
    };
};
