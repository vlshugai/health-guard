import type { AllValue, DealStatusCode } from '../../../types';
import type { ClientReferralsTableRow } from '../types';
import { ALL_VALUE } from '../../../constants';

export const filterDealsByDealStage = (filter?: DealStatusCode | AllValue | null) => {
    return (deal: ClientReferralsTableRow) => {
        if (!filter || filter === ALL_VALUE) {
            return true;
        }

        return deal.dealStatus === filter;
    };
};
