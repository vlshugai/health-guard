import type { HubspotContactTierLevel, ObjValues } from '@/types';
import type { DealsCommissionStatusOptionIds } from '../../types';
import { DEAL_STATUS_CODES } from '../../constants';

export type ClientReferralsTableRow = {
    id: string;
    createdDate: string | Date;
    level: HubspotContactTierLevel;
    dealStatus: ObjValues<typeof DEAL_STATUS_CODES>;
    commissionStatus: DealsCommissionStatusOptionIds | null;
    amount: number;
    dealName: string;
    isMyDeal: boolean;
};
