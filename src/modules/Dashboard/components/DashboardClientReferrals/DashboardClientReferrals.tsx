import { memo, useCallback, useMemo } from 'react';
import { useReactTable, getCoreRowModel, getPaginationRowModel } from '@tanstack/react-table';
import { format } from 'date-fns';
import clsx from 'clsx';
import type { ColumnDef, HeaderGroup } from '@tanstack/react-table';
import type { ClientReferralsTableRow } from './types';
import { useGetDealsByAffiliates } from '@/hooks/api/useHubspotHealthGuardApi';
import { useClientReferralsFilter } from './hooks/useClientReferralsFilter';
import { useClientReferralsTableRows } from './hooks/useClientReferralsTableRows';
import { intlNumberFormat } from '@/utils/common';
import { ALL_VALUE, DEAL_STATUS_CODES } from '../../constants';
import {
    CLIENT_REFERRALS_COMMISSION_STATUS_FILTER_OPTIONS,
    CLIENT_REFERRALS_CREATED_DATE_FILTER_OPTIONS,
    CLIENT_REFERRALS_DEAL_STATUS_FILTER_OPTIONS,
    CLIENT_REFERRALS_LEVEL_FILTER_OPTIONS,
} from './constants';
import { Case, Default, Switch } from '@/components/utils/Switch';
import DropdownMenu from '@/components/DropdownMenu';
import Table from '@/components/Table';
import TablePagination from '@/components/Table/components/TablePagination';
import Typography from '@/ui/Typography';
import Container from '@/ui/Container';
import Loader from '@/ui/Loader';
import ClientReferralsCSVExportButton from './components/ClientReferralsCSVExportButton';
import s from './DashboardClientReferrals.module.css';

const DashboardClientReferrals: React.FC = () => {
    const {
        createdDate,
        level,
        dealStatus,
        commissionStatus,
        onCreatedDateFilterChange,
        onLevelFilterChange,
        onDealStatusFilterChange,
        onCommissionStatusFilterChange,
    } = useClientReferralsFilter();

    const { clientReferrals, totalClientReferrals, totalFilteredClientReferrals, pagination, setPagination } =
        useClientReferralsTableRows();

    const { isFetching } = useGetDealsByAffiliates();

    const isTableDisabled = isFetching || totalClientReferrals === 0;
    const isFilteredTableEmpty = totalFilteredClientReferrals === 0;
    const isWithPagination = !!totalFilteredClientReferrals && totalFilteredClientReferrals > pagination.pageSize;

    const selectedCreatedDateValue = useMemo(() => {
        return CLIENT_REFERRALS_CREATED_DATE_FILTER_OPTIONS.optionsObject[createdDate];
    }, [createdDate]);
    const selectedLevelValue = useMemo(() => {
        return CLIENT_REFERRALS_LEVEL_FILTER_OPTIONS.optionsObject[level];
    }, [level]);
    const selectedDealStatusValue = useMemo(() => {
        return CLIENT_REFERRALS_DEAL_STATUS_FILTER_OPTIONS.optionsObject[dealStatus];
    }, [dealStatus]);
    const selectedCommissionStatusValue = useMemo(() => {
        return CLIENT_REFERRALS_COMMISSION_STATUS_FILTER_OPTIONS.optionsObject[commissionStatus];
    }, [commissionStatus]);

    const tableColumns = useMemo<ColumnDef<ClientReferralsTableRow>[]>(() => {
        return [
            {
                accessorKey: 'createdDate',
                header: 'Create Date',
                // TODO: add sorting
                enableSorting: false,
                minSize: 218,
                cell({ getValue }) {
                    const createdDate = getValue<ClientReferralsTableRow['createdDate']>();

                    const formattedDate = format(createdDate, 'MMM d, yyyy');

                    return (
                        <Typography
                            className={clsx(s.cell, 'truncate')}
                            variant="paragraph-sm"
                            title={formattedDate}
                            asChild
                        >
                            <span>{formattedDate}</span>
                        </Typography>
                    );
                },
            },
            {
                accessorKey: 'level',
                header: 'Level',
                // TODO: add sorting
                enableSorting: false,
                minSize: 192,
                cell({ getValue, row }) {
                    const level = getValue<ClientReferralsTableRow['level']>();
                    const { isMyDeal } = row.original;

                    if (isMyDeal) {
                        return (
                            <Typography
                                className={clsx(s.cell, 'truncate')}
                                variant="paragraph-sm"
                                title="My referrals"
                                asChild
                            >
                                <span>Direct</span>
                            </Typography>
                        );
                    }

                    const label = CLIENT_REFERRALS_LEVEL_FILTER_OPTIONS.optionsObject[level]?.label;

                    return (
                        <Typography className={clsx(s.cell, 'truncate')} variant="paragraph-sm" title={label} asChild>
                            <span>{label}</span>
                        </Typography>
                    );
                },
            },
            {
                accessorKey: 'dealName',
                header: 'Deal Name',
                // TODO: add sorting
                enableSorting: false,
                minSize: 192,
                cell({ getValue }) {
                    const clientName = getValue<ClientReferralsTableRow['dealName']>();

                    if (!clientName) {
                        return (
                            <Typography
                                className={clsx(s.cell, 'truncate')}
                                variant="paragraph-sm"
                                title="Not specified"
                                asChild
                            >
                                <span>---</span>
                            </Typography>
                        );
                    }

                    return (
                        <Typography
                            className={clsx(s.cell, 'truncate')}
                            variant="paragraph-sm"
                            title={clientName}
                            asChild
                        >
                            <span>{clientName}</span>
                        </Typography>
                    );
                },
            },
            {
                accessorKey: 'dealStatus',
                header: 'Deal Stage',
                // TODO: add sorting
                enableSorting: false,
                minSize: 192,
                cell({ getValue }) {
                    const dealStatus = getValue<ClientReferralsTableRow['dealStatus']>();

                    const label = CLIENT_REFERRALS_DEAL_STATUS_FILTER_OPTIONS.optionsObject[dealStatus]?.label;

                    if (!label) {
                        return (
                            <Typography
                                className={clsx(s.cell, 'truncate')}
                                variant="paragraph-sm"
                                title="Not specified"
                                asChild
                            >
                                <span>Not specified</span>
                            </Typography>
                        );
                    }

                    return (
                        <Typography className={clsx(s.cell, 'truncate')} variant="paragraph-sm" title={label} asChild>
                            <span>{label}</span>
                        </Typography>
                    );
                },
            },
            {
                accessorKey: 'commissionStatus',
                header: 'Commission Status',
                // TODO: add sorting
                enableSorting: false,
                minSize: 192,
                cell({ getValue }) {
                    const commissionStatus = getValue<ClientReferralsTableRow['commissionStatus']>();

                    if (!commissionStatus) {
                        return (
                            <Typography
                                className={clsx(s.cell, 'truncate')}
                                variant="paragraph-sm"
                                title="Not paid"
                                asChild
                            >
                                <span>--</span>
                            </Typography>
                        );
                    }

                    const label =
                        CLIENT_REFERRALS_COMMISSION_STATUS_FILTER_OPTIONS.optionsObject[commissionStatus]?.label;

                    return (
                        <Typography className={clsx(s.cell, 'truncate')} variant="paragraph-sm" title={label} asChild>
                            <span>{label || '--'}</span>
                        </Typography>
                    );
                },
            },
            {
                accessorKey: 'amount',
                header: 'Earnings',
                // TODO: add sorting
                enableSorting: false,
                minSize: 192,
                cell({ getValue, row }) {
                    const amount = getValue<ClientReferralsTableRow['amount']>();
                    const dealStage = row.original.dealStatus;

                    if (dealStage !== DEAL_STATUS_CODES.closed) {
                        return (
                            <Typography
                                className={clsx(s.cell, 'truncate')}
                                variant="paragraph-sm"
                                title="Not paid"
                                asChild
                            >
                                <span>---</span>
                            </Typography>
                        );
                    }

                    return (
                        <Typography
                            className={clsx(s.cell, 'truncate')}
                            variant="paragraph-sm"
                            title={amount.toString()}
                            asChild
                        >
                            <span>${intlNumberFormat(amount)}</span>
                        </Typography>
                    );
                },
            },
        ];
    }, []);

    const clientReferralsTable = useReactTable({
        columns: tableColumns,
        data: clientReferrals,
        state: {
            pagination,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        onPaginationChange: setPagination,
    });
    const { rows } = clientReferralsTable.getRowModel();

    const paginationPageChangeHandler = useCallback(
        (newPage: number) => {
            clientReferralsTable.setPageIndex(newPage - 1);
        },
        [clientReferralsTable]
    );

    return (
        <section className={s.wrap}>
            <Container className={s.container}>
                <header className={s.header}>
                    <Typography className={s.title} variant="title-h4" asChild>
                        <h2>Client Referrals</h2>
                    </Typography>
                    <div className={s['filter-wrap']}>
                        <div className={s.filters}>
                            <DropdownMenu
                                className={s.field}
                                placeholder="Create Date"
                                options={CLIENT_REFERRALS_CREATED_DATE_FILTER_OPTIONS.optionsArray}
                                value={
                                    selectedCreatedDateValue.value === ALL_VALUE ? undefined : selectedCreatedDateValue
                                }
                                disabled={isFetching}
                                onChange={onCreatedDateFilterChange}
                            />
                            <DropdownMenu
                                className={s.field}
                                placeholder="Level"
                                options={CLIENT_REFERRALS_LEVEL_FILTER_OPTIONS.optionsArray}
                                value={selectedLevelValue.value === ALL_VALUE ? undefined : selectedLevelValue}
                                disabled={isFetching}
                                onChange={onLevelFilterChange}
                            />
                            <DropdownMenu
                                className={s.field}
                                placeholder="Deal Status"
                                options={CLIENT_REFERRALS_DEAL_STATUS_FILTER_OPTIONS.optionsArray}
                                value={
                                    selectedDealStatusValue.value === ALL_VALUE ? undefined : selectedDealStatusValue
                                }
                                disabled={isFetching}
                                onChange={onDealStatusFilterChange}
                            />
                            <DropdownMenu
                                className={s.field}
                                placeholder="Commission Status"
                                options={CLIENT_REFERRALS_COMMISSION_STATUS_FILTER_OPTIONS.optionsArray}
                                value={
                                    selectedCommissionStatusValue.value === ALL_VALUE
                                        ? undefined
                                        : selectedCommissionStatusValue
                                }
                                disabled={isFetching}
                                onChange={onCommissionStatusFilterChange}
                            />
                        </div>
                        <div className={s['cta-wrap']}>
                            <ClientReferralsCSVExportButton
                                className={clsx(s.cta, s.export)}
                                deals={rows}
                                disabled={isTableDisabled || isFilteredTableEmpty}
                            />
                        </div>
                    </div>
                </header>
                <div className={s['table-wrap']}>
                    <Switch>
                        <Case condition={isFetching}>
                            <Loader className="mx-auto" />
                        </Case>
                        <Default>
                            <Table
                                className={clsx(s.table, {
                                    [s['with-pagination']]: isWithPagination,
                                })}
                                headerGroups={
                                    clientReferralsTable.getHeaderGroups() as unknown as HeaderGroup<unknown>[]
                                }
                                rows={rows}
                                noRowsMessage={
                                    !totalClientReferrals
                                        ? 'You don’t have any referrals yet. Use the links above to start enrolling users.'
                                        : 'Referrals not found. Try other parameters'
                                }
                            />
                        </Default>
                    </Switch>
                    {isWithPagination ? (
                        <TablePagination
                            className={s.pagination}
                            page={clientReferralsTable.getState().pagination.pageIndex + 1}
                            total={totalFilteredClientReferrals}
                            pageSize={clientReferralsTable.getState().pagination.pageSize}
                            onPageChange={paginationPageChangeHandler}
                        />
                    ) : null}
                </div>
            </Container>
        </section>
    );
};

export default memo(DashboardClientReferrals);
