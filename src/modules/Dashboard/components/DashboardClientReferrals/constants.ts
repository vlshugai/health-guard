import { splitObjectToObjectAndArrayValues } from '@/utils/common';
import {
    ALL_OPTION,
    ALL_VALUE,
    DEALS_CREATED_DATE_OPTIONS_IDS,
    DEAL_COMMISSION_LEVEL_OPTIONS_IDS,
    DEAL_STATUS_CODES,
    TIER_LEVEL_SELECT_OPTIONS_IDS,
} from '../../constants';

export const CLIENT_REFERRALS_CREATED_DATE_FILTER_OPTIONS = splitObjectToObjectAndArrayValues({
    [ALL_VALUE]: ALL_OPTION,
    [DEALS_CREATED_DATE_OPTIONS_IDS.today]: {
        label: 'Today',
        value: DEALS_CREATED_DATE_OPTIONS_IDS.today,
        order: 1,
    },
    [DEALS_CREATED_DATE_OPTIONS_IDS.last_7_days]: {
        label: 'Last 7 days',
        value: DEALS_CREATED_DATE_OPTIONS_IDS.last_7_days,
        order: 2,
    },
    [DEALS_CREATED_DATE_OPTIONS_IDS.last_30_days]: {
        label: 'Last 30 days',
        value: DEALS_CREATED_DATE_OPTIONS_IDS.last_30_days,
        order: 3,
    },
});

export const CLIENT_REFERRALS_LEVEL_FILTER_OPTIONS = splitObjectToObjectAndArrayValues({
    [ALL_VALUE]: ALL_OPTION,
    [TIER_LEVEL_SELECT_OPTIONS_IDS.level_0]: {
        label: 'Direct',
        value: TIER_LEVEL_SELECT_OPTIONS_IDS.level_0,
        order: 1,
    },
    [TIER_LEVEL_SELECT_OPTIONS_IDS.level_1]: {
        label: 'Level 1 Affiliate',
        value: TIER_LEVEL_SELECT_OPTIONS_IDS.level_1,
        order: 2,
    },
    [TIER_LEVEL_SELECT_OPTIONS_IDS.level_2]: {
        label: 'Level 2 Affiliate',
        value: TIER_LEVEL_SELECT_OPTIONS_IDS.level_2,
        order: 3,
    },
});

export const CLIENT_REFERRALS_DEAL_STATUS_FILTER_OPTIONS = splitObjectToObjectAndArrayValues({
    [ALL_VALUE]: ALL_OPTION,
    [DEAL_STATUS_CODES.created]: {
        label: 'Created',
        value: DEAL_STATUS_CODES.created,
        order: 1,
    },
    [DEAL_STATUS_CODES.paid]: {
        label: 'Paid',
        value: DEAL_STATUS_CODES.paid,
        order: 2,
    },
    [DEAL_STATUS_CODES.closed]: {
        label: 'Closed',
        value: DEAL_STATUS_CODES.closed,
        order: 3,
    },
    [DEAL_STATUS_CODES.canceled]: {
        label: 'Canceled',
        value: DEAL_STATUS_CODES.canceled,
        order: 4,
    },
    [DEAL_STATUS_CODES.ineligible]: {
        label: 'Ineligible',
        value: DEAL_STATUS_CODES.ineligible,
        order: 5,
    },
});

export const CLIENT_REFERRALS_COMMISSION_STATUS_FILTER_OPTIONS = splitObjectToObjectAndArrayValues({
    [ALL_VALUE]: ALL_OPTION,
    [DEAL_COMMISSION_LEVEL_OPTIONS_IDS.paid]: {
        label: 'Paid',
        value: DEAL_COMMISSION_LEVEL_OPTIONS_IDS.paid,
        order: 1,
    },
    [DEAL_COMMISSION_LEVEL_OPTIONS_IDS.unpaid]: {
        label: 'Unpaid',
        value: DEAL_COMMISSION_LEVEL_OPTIONS_IDS.unpaid,
        order: 2,
    },
    [DEAL_COMMISSION_LEVEL_OPTIONS_IDS.paymentFailure]: {
        label: 'Payment Failure',
        value: DEAL_COMMISSION_LEVEL_OPTIONS_IDS.paymentFailure,
        order: 3,
    },
    [DEAL_COMMISSION_LEVEL_OPTIONS_IDS.noPaymentDue]: {
        label: 'No Payment Due',
        value: DEAL_COMMISSION_LEVEL_OPTIONS_IDS.noPaymentDue,
        order: 4,
    },
});
