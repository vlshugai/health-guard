import { useMemo, useState } from 'react';
import { useUpdateEffect } from '@react-hookz/web';
import type { PaginationState } from '@tanstack/react-table';
import type { ClientReferralsTableRow } from '../types';
import { useMe } from '@/hooks/useMe';
import { useGetDealsByAffiliates } from '@/hooks/api/useHubspotHealthGuardApi';
import { useDashboardClientReferralsSearchParams } from './useDashboardClientReferralsSearchParams';
import { filterDealsByCommissionStatus } from '../utils/filterDealsByCommissionStatus';
import { generateDealTableRow } from '../utils/generateClientReferralTableRow';
import { filterDealsByCreatedDate } from '../utils/filterDealsByCreatedDate';
import { filterDealsByDealStage } from '../utils/filterDealsByDealStage';
import { filterDealsByLevel } from '../utils/filterDealsByLevel';
import { DASHBOARD_TABLE_PAGE_SIZE } from '../../../constants';

export const useClientReferralsTableRows = () => {
    const user = useMe();
    const search = useDashboardClientReferralsSearchParams();

    const { data: deals } = useGetDealsByAffiliates();

    const [pagination, setPagination] = useState<PaginationState>({
        pageIndex: 0,
        pageSize: DASHBOARD_TABLE_PAGE_SIZE,
    });

    const totalClientReferrals = deals?.total ?? 0;

    const filteredClientReferrals = useMemo<ClientReferralsTableRow[]>(() => {
        if (!deals?.results?.length) {
            return [];
        }

        return deals.results
            .map((deal) => {
                const isMyDeal = deal.properties?.deal_position_0_affiliate_id === user.id;

                if (isMyDeal) {
                    return generateDealTableRow(deal, 0);
                }

                const isFirstLevelDeal = deal.properties?.deal_position_1_affiliate_id === user.id;

                if (isFirstLevelDeal) {
                    return generateDealTableRow(deal, 1);
                }

                const isSecondLevelDeal = deal.properties?.deal_position_2_affiliate_id === user.id;

                if (isSecondLevelDeal) {
                    return generateDealTableRow(deal, 2);
                }

                return generateDealTableRow(deal, 0);
            })
            .filter(filterDealsByCreatedDate(search.createdDate))
            .filter(filterDealsByLevel(search.level))
            .filter(filterDealsByDealStage(search.dealStatus))
            .filter(filterDealsByCommissionStatus(search.commissionStatus));
    }, [deals, search.commissionStatus, search.createdDate, search.dealStatus, search.level, user.id]);
    const totalFilteredClientReferrals = filteredClientReferrals.length;

    useUpdateEffect(() => {
        setPagination((prev) => {
            return {
                ...prev,
                pageIndex: 0,
            };
        });
    }, [search.commissionStatus, search.createdDate, search.dealStatus, search.level]);

    return {
        pagination,
        totalClientReferrals,
        totalFilteredClientReferrals,
        clientReferrals: filteredClientReferrals,
        setPagination,
    };
};
