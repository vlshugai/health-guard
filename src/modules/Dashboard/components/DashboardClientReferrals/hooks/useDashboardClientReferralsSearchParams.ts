import { useDashboardSearchParams } from '@/modules/Dashboard/hooks/useDashboardSearchParams';
import { DashboardClientReferralsSearchParams } from '@/modules/Dashboard/types';

export const useDashboardClientReferralsSearchParams = () => {
    const search = useDashboardSearchParams();

    return {
        createdDate: search.createdDate,
        level: search.level,
        dealStatus: search.dealStatus,
        commissionStatus: search.commissionStatus,
    } as DashboardClientReferralsSearchParams;
};
