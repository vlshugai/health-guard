import { useCallback } from 'react';
import { useNavigate } from '@tanstack/react-router';
import { useDashboardSearchParams } from '../../../hooks/useDashboardSearchParams';

export const useClientReferralsFilter = () => {
    const search = useDashboardSearchParams();
    const navigate = useNavigate({ from: '/dashboard' });

    const clientReferralsFilterChangeHandler = useCallback(
        (field: keyof typeof search) => {
            return (value: string) => {
                navigate({
                    search: {
                        ...search,
                        [field]: value,
                    },
                    replace: true,
                });
            };
        },
        [navigate, search]
    );

    const onCreatedDateFilterChange = clientReferralsFilterChangeHandler('createdDate');
    const onLevelFilterChange = clientReferralsFilterChangeHandler('level');
    const onDealStatusFilterChange = clientReferralsFilterChangeHandler('dealStatus');
    const onCommissionStatusFilterChange = clientReferralsFilterChangeHandler('commissionStatus');

    return {
        ...search,
        onCreatedDateFilterChange,
        onLevelFilterChange,
        onDealStatusFilterChange,
        onCommissionStatusFilterChange,
    };
};
