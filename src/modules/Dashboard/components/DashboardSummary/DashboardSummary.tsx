import { memo, useMemo } from 'react';
import clsx from 'clsx';
import { useGetDealsByAffiliates } from '@/hooks/api/useHubspotHealthGuardApi';
import { useGetFirebaseUserAffiliatesTeam } from '@/hooks/api/useFirebaseApi';
import { useUserAffiliatesTeamStats } from './hooks/useUserAffiliatesTeamStats';
import { useGetReferralLinks } from './hooks/useGetReferralLinks';
import { useDealsStatsFilter } from './hooks/useDealsStatsFilter';
import { useDealsStats } from './hooks/useDealsStats';
import { intlNumberFormat } from '@/utils/common';
import {
    DEALS_STATS_SOLD_REFERRALS_FILTER_OPTIONS,
    DEALS_STATS_TOTAL_COMMISSIONS_FILTER_OPTIONS,
    DEALS_STATS_TOTAL_REFERRALS_FILTER_OPTIONS,
} from './constants';
import DropdownMenu from '@/components/DropdownMenu';
import Card from '@/components/Card';
import Typography from '@/ui/Typography';
import Container from '@/ui/Container';
import Button from '@/ui/Button';
import Input from '@/ui/Input';
import s from './DashboardSummary.module.css';

const DashboardSummary: React.FC = () => {
    const { affiliateReferralLink, customerReferralLink } = useGetReferralLinks();

    const {
        totalReferrals: totalReferralsFilter,
        soldReferrals: soldReferralsFilter,
        totalCommissions: totalCommissionsFilter,
        onTotalReferralsFilterChange,
        onSoldReferralsFilterChange,
        onTotalCommissionsFilterChange,
    } = useDealsStatsFilter();

    const { totalReferrals, soldReferrals, totalCommissions } = useDealsStats();
    const { totalAffiliates, totalFirstLevelAffiliates, totalSecondLevelAffiliates } = useUserAffiliatesTeamStats();

    const { isFetching: isDashboardStatsFetching } = useGetDealsByAffiliates();
    const { isFetching: isUserAffiliatesTeamStatsFetching } = useGetFirebaseUserAffiliatesTeam();

    const selectedTotalReferralsValue = useMemo(() => {
        return DEALS_STATS_TOTAL_REFERRALS_FILTER_OPTIONS.optionsObject[totalReferralsFilter];
    }, [totalReferralsFilter]);
    const selectedSoldReferralsValue = useMemo(() => {
        return DEALS_STATS_SOLD_REFERRALS_FILTER_OPTIONS.optionsObject[soldReferralsFilter];
    }, [soldReferralsFilter]);
    const selectedTotalCommissionsValue = useMemo(() => {
        return DEALS_STATS_TOTAL_COMMISSIONS_FILTER_OPTIONS.optionsObject[totalCommissionsFilter];
    }, [totalCommissionsFilter]);

    return (
        <section className={s.wrap}>
            <Container className={s.container}>
                <header className={s.header}>
                    <Typography className={s.title} variant="title-h1" asChild>
                        <h1>Affiliate Dashboard</h1>
                    </Typography>
                </header>
                <div className={s.content}>
                    <div className={s['fields-wrap']}>
                        <Input
                            className={s.field}
                            label="Affiliate Referral Link"
                            type="text"
                            placeholder="Affiliate Referral Link"
                            value={affiliateReferralLink}
                            variant="copy"
                            readOnly
                        />
                        <Input
                            className={s.field}
                            label="Customer Referral Link"
                            type="text"
                            placeholder="Customer Referral Link"
                            value={customerReferralLink}
                            variant="copy"
                            readOnly
                        />
                        <div className={s['cta-wrap']}>
                            <Button className={clsx(s.cta, s['contact-support'])} variant="outline" asChild>
                                <a href="mailto:hello@triton.finance">Contact support</a>
                            </Button>
                        </div>
                    </div>
                    <div className={s['cards-wrap']}>
                        <Card
                            className={s.card}
                            title="Total Customer Referrals"
                            number={isDashboardStatsFetching ? '--' : totalReferrals}
                        >
                            <DropdownMenu
                                placeholder="Select level"
                                options={DEALS_STATS_TOTAL_REFERRALS_FILTER_OPTIONS.optionsArray}
                                value={selectedTotalReferralsValue}
                                disabled={isDashboardStatsFetching}
                                onChange={onTotalReferralsFilterChange}
                            />
                        </Card>
                        <Card
                            className={s.card}
                            title="Total Customers Enrolled"
                            number={isDashboardStatsFetching ? '--' : soldReferrals}
                        >
                            <DropdownMenu
                                placeholder="Select level"
                                options={DEALS_STATS_SOLD_REFERRALS_FILTER_OPTIONS.optionsArray}
                                value={selectedSoldReferralsValue}
                                disabled={isDashboardStatsFetching}
                                onChange={onSoldReferralsFilterChange}
                            />
                        </Card>
                        <Card
                            className={s.card}
                            title="Total Earned Commission"
                            number={isDashboardStatsFetching ? '--' : `$${intlNumberFormat(totalCommissions)}`}
                        >
                            <DropdownMenu
                                placeholder="Select level"
                                options={DEALS_STATS_TOTAL_COMMISSIONS_FILTER_OPTIONS.optionsArray}
                                value={selectedTotalCommissionsValue}
                                disabled={isDashboardStatsFetching}
                                onChange={onTotalCommissionsFilterChange}
                            />
                        </Card>
                        <Card
                            className={s.card}
                            title="Level 1 Affiliates"
                            number={isUserAffiliatesTeamStatsFetching ? '--' : totalFirstLevelAffiliates}
                        />
                        <Card
                            className={s.card}
                            title="Level 2 Affiliates"
                            number={isUserAffiliatesTeamStatsFetching ? '--' : totalSecondLevelAffiliates}
                        />
                        <Card
                            className={s.card}
                            title="Total Affiliates"
                            number={isUserAffiliatesTeamStatsFetching ? '--' : totalAffiliates}
                        />
                    </div>
                </div>
            </Container>
        </section>
    );
};

export default memo(DashboardSummary);
