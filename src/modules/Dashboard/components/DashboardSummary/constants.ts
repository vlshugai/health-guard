import type { DealsCommissionStatusOptionIds } from '../../types';
import type { DealsStats } from './types';
import { getOptionsByTierLevel } from '../../utils/getOptionsByTierLevel';
import { DEAL_COMMISSION_LEVEL_OPTIONS_IDS, TIER_LEVEL_SELECT_OPTIONS_IDS } from '../../constants';

export const DEAL_COMMISSION_INVALID_STATUSES: DealsCommissionStatusOptionIds[] = [
    DEAL_COMMISSION_LEVEL_OPTIONS_IDS.noPaymentDue,
];

export const DEALS_STATS_TOTAL_REFERRALS_FILTER_OPTIONS = getOptionsByTierLevel('Referrals');
export const DEALS_STATS_SOLD_REFERRALS_FILTER_OPTIONS = getOptionsByTierLevel('Sales');
export const DEALS_STATS_TOTAL_COMMISSIONS_FILTER_OPTIONS = getOptionsByTierLevel('Commissions');

export const DASHBOARD_DEALS_STATS_INITIAL_VALUES: DealsStats = {
    totalReferrals: {
        [TIER_LEVEL_SELECT_OPTIONS_IDS.all]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_0]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_1]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_2]: 0,
    },
    soldReferrals: {
        [TIER_LEVEL_SELECT_OPTIONS_IDS.all]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_0]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_1]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_2]: 0,
    },
    totalCommissions: {
        [TIER_LEVEL_SELECT_OPTIONS_IDS.all]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_0]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_1]: 0,
        [TIER_LEVEL_SELECT_OPTIONS_IDS.level_2]: 0,
    },
};
