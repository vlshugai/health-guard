import type { TierLevelSelectOptionIds } from '../../types';

export type DealsStats = {
    totalReferrals: Record<TierLevelSelectOptionIds, number>;
    soldReferrals: Record<TierLevelSelectOptionIds, number>;
    totalCommissions: Record<TierLevelSelectOptionIds, number>;
};
