import { useMemo } from 'react';
import { useMe } from '@/hooks/useMe';
import { useGetDealsByAffiliates } from '@/hooks/api/useHubspotHealthGuardApi';
import { useDealsStatsFilter } from './useDealsStatsFilter';
import { transformDealForDashboardDealsStats } from '../utils/transformDealForDashboardDealsStats';
import { ERR_CANCELLED } from '@/constants';
import { TIER_LEVEL_SELECT_OPTIONS_IDS } from '../../../constants';
import { DASHBOARD_DEALS_STATS_INITIAL_VALUES } from '../constants';

export const useDealsStats = () => {
    const user = useMe();

    const {
        totalReferrals: totalReferralsFilter,
        soldReferrals: soldReferralsFilter,
        totalCommissions: totalCommissionsFilter,
    } = useDealsStatsFilter();

    const { data: deals, isLoading, isError, error } = useGetDealsByAffiliates();
    const isRequestError = isError && error.name !== ERR_CANCELLED;

    const transformedDeals = useMemo(() => {
        if (isLoading || isRequestError || !deals) {
            return DASHBOARD_DEALS_STATS_INITIAL_VALUES;
        }

        const result = deals.results.reduce(
            (acc, deal) => {
                const isMyDeal = deal.properties?.deal_position_0_affiliate_id === user.id;

                if (isMyDeal) {
                    return transformDealForDashboardDealsStats(acc, deal, 0);
                }

                const isFirstLevelDeal = deal.properties?.deal_position_1_affiliate_id === user.id;

                if (isFirstLevelDeal) {
                    return transformDealForDashboardDealsStats(acc, deal, 1);
                }

                const isSecondLevelDeal = deal.properties?.deal_position_2_affiliate_id === user.id;

                if (isSecondLevelDeal) {
                    return transformDealForDashboardDealsStats(acc, deal, 2);
                }

                return acc;
            },
            {
                ...DASHBOARD_DEALS_STATS_INITIAL_VALUES,
                totalReferrals: {
                    ...DASHBOARD_DEALS_STATS_INITIAL_VALUES.totalReferrals,
                    [TIER_LEVEL_SELECT_OPTIONS_IDS.all]: deals.total,
                },
            }
        );

        return result;
    }, [deals, isLoading, isRequestError, user.id]);

    const filteredTotalReferrals = useMemo(() => {
        if (!totalReferralsFilter) {
            return deals?.total ?? 0;
        }

        return transformedDeals.totalReferrals[totalReferralsFilter];
    }, [deals?.total, totalReferralsFilter, transformedDeals.totalReferrals]);

    const filteredSoldReferrals = useMemo(() => {
        if (!soldReferralsFilter) {
            return transformedDeals.soldReferrals[TIER_LEVEL_SELECT_OPTIONS_IDS.all];
        }

        return transformedDeals.soldReferrals[soldReferralsFilter];
    }, [soldReferralsFilter, transformedDeals.soldReferrals]);

    const filteredTotalCommissions = useMemo(() => {
        if (!totalCommissionsFilter) {
            return transformedDeals.totalCommissions[TIER_LEVEL_SELECT_OPTIONS_IDS.all];
        }

        return transformedDeals.totalCommissions[totalCommissionsFilter];
    }, [totalCommissionsFilter, transformedDeals.totalCommissions]);

    return {
        totalReferrals: filteredTotalReferrals,
        soldReferrals: filteredSoldReferrals,
        totalCommissions: filteredTotalCommissions,
    };
};
