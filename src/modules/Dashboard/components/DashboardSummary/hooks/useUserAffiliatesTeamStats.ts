import { useMemo } from 'react';
import { useGetFirebaseUserAffiliatesTeam } from '@/hooks/api/useFirebaseApi';

export const useUserAffiliatesTeamStats = () => {
    const { data: team } = useGetFirebaseUserAffiliatesTeam();

    const teamStats = useMemo(() => {
        if (!team) {
            return {
                totalAffiliates: 0,
                totalFirstLevelAffiliates: 0,
                totalSecondLevelAffiliates: 0,
            };
        }

        const totalAffiliates = team.totalFirstLevelAffiliates + team.totalSecondLevelAffiliates;

        return {
            totalAffiliates,
            totalFirstLevelAffiliates: team.totalFirstLevelAffiliates,
            totalSecondLevelAffiliates: team.totalSecondLevelAffiliates,
        };
    }, [team]);

    return teamStats;
};
