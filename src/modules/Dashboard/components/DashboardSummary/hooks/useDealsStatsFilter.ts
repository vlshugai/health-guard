import { useCallback } from 'react';
import { useNavigate } from '@tanstack/react-router';
import { useDashboardSearchParams } from '../../../hooks/useDashboardSearchParams';

export const useDealsStatsFilter = () => {
    const search = useDashboardSearchParams();
    const navigate = useNavigate({ from: '/dashboard' });

    const dealsStatsFilterChangeHandler = useCallback(
        (field: keyof typeof search) => {
            return (value: string) => {
                navigate({
                    search: {
                        ...search,
                        [field]: value,
                    },
                    replace: true,
                });
            };
        },
        [navigate, search]
    );

    const onTotalReferralsFilterChange = dealsStatsFilterChangeHandler('totalReferrals');
    const onSoldReferralsFilterChange = dealsStatsFilterChangeHandler('soldReferrals');
    const onTotalCommissionsFilterChange = dealsStatsFilterChangeHandler('totalCommissions');

    return {
        ...search,
        onTotalReferralsFilterChange,
        onSoldReferralsFilterChange,
        onTotalCommissionsFilterChange,
    };
};
