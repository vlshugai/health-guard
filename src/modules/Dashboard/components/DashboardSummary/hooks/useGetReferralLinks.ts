import { useMemo } from 'react';
import { useMe } from '@/hooks/useMe';
import { SPONSOR_MARKETING_ID_PREFIX, AFFILIATE_MARKETING_ID_PREFIX } from '@/constants';

export const useGetReferralLinks = () => {
    const user = useMe();

    const affiliateReferralLink = useMemo(() => {
        return `${window.location.origin.toString()}/sign-up?subid=${SPONSOR_MARKETING_ID_PREFIX}${user.id}`;
    }, [user.id]);

    const customerReferralLink = useMemo(() => {
        return `${window.location.origin.toString()}/?subid=${AFFILIATE_MARKETING_ID_PREFIX}${user.id}`;
    }, [user.id]);

    return {
        affiliateReferralLink,
        customerReferralLink,
    };
};
