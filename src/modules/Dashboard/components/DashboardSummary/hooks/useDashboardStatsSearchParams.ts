import type { DashboardDealsStatsSearchParams } from '../../../types';
import { useDashboardSearchParams } from '../../../hooks/useDashboardSearchParams';

export const useDashboardStatsSearchParams = () => {
    const search = useDashboardSearchParams();

    return {
        soldReferrals: search.soldReferrals,
        totalCommissions: search.totalCommissions,
        totalReferrals: search.totalReferrals,
    } as DashboardDealsStatsSearchParams;
};
