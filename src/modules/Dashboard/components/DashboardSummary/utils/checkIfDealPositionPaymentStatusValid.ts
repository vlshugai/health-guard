import type { HubspotContactTierLevel } from '@/types';
import type { HubspotDealItem } from '@/api/hubspotHealthGuard';
import { DEAL_COMMISSION_INVALID_STATUSES } from '../constants';

export const checkIfDealPositionPaymentStatusValid = (deal: HubspotDealItem, position: HubspotContactTierLevel) => {
    const commissionStatus = deal.properties[`deal_position_${position}_commission_payment_status`];

    if (!commissionStatus) {
        return false;
    }

    return !DEAL_COMMISSION_INVALID_STATUSES.includes(commissionStatus);
};
