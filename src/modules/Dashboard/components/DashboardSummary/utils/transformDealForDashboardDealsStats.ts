import { produce } from 'immer';
import type { HubspotContactTierLevel } from '@/types';
import type { HubspotDealItem } from '@/api/hubspotHealthGuard';
import type { DealsStats } from '../types';
import { checkIfDealPositionPaymentStatusValid } from './checkIfDealPositionPaymentStatusValid';
import { DEAL_STATUS_CODES, TIER_LEVEL_SELECT_OPTIONS_IDS } from '../../../constants';

export const transformDealForDashboardDealsStats = (
    acc: DealsStats,
    deal: HubspotDealItem,
    positionToWriteStats: HubspotContactTierLevel
) => {
    const isDealClosed = deal.properties.dealstage === DEAL_STATUS_CODES.closed;

    return produce(acc, (prev) => {
        const isCommissionStatusValid = checkIfDealPositionPaymentStatusValid(deal, positionToWriteStats);

        if (isCommissionStatusValid) {
            const parsedCommissionAmount = parseFloat(
                deal.properties[`deal_position_${positionToWriteStats}_commission_amount`] ?? '0'
            );
            const isCommissionAmountNaN = isNaN(parsedCommissionAmount);
            const commissionAmount = isCommissionAmountNaN ? 0 : parsedCommissionAmount;

            prev.totalCommissions[TIER_LEVEL_SELECT_OPTIONS_IDS.all] += commissionAmount;
            prev.totalCommissions[positionToWriteStats] += commissionAmount;
        }

        if (isDealClosed) {
            prev.soldReferrals[TIER_LEVEL_SELECT_OPTIONS_IDS.all] += 1;
            prev.soldReferrals[positionToWriteStats] += 1;
        }

        prev.totalReferrals[positionToWriteStats] += 1;
    });
};
