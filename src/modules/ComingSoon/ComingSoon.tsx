import { Link } from '@tanstack/react-router';
import clsx from 'clsx';
import Typography from '@/ui/Typography';
import Container from '@/ui/Container';
import Button from '@/ui/Button';
import Icon from '@/ui/Icon';
import s from './ComingSoon.module.css';

const ComingSoon: React.FC = () => {
    return (
        <div className={clsx(s.wrap, 'full-height')}>
            <div className={s.inner}>
                <Container className={s.container}>
                    <div className={s.content}>
                        <div className={s['image-wrap']}>
                            <img
                                className={s.image}
                                src="/images/puzzles.webp"
                                alt="Two blue puzzles touching each other"
                            />
                        </div>
                        <Typography className={s.title} variant="title-h4" asChild>
                            <h1>Coming&nbsp;Soon&nbsp;...</h1>
                        </Typography>
                        <Typography className={s.subtitle} variant="paragraph-base" asChild>
                            <p>We’re doing our best to show this page as soon as possible</p>
                        </Typography>
                        <Button className={s.cta} variant="primary" asChild>
                            <Link to="/dashboard" search>
                                Back Home
                                <Icon className={s.icon} id="icon-arrow-circle-right-top_18" />
                            </Link>
                        </Button>
                    </div>
                </Container>
            </div>
        </div>
    );
};

export default ComingSoon;
