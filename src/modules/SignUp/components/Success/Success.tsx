import { Link } from '@tanstack/react-router';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import Icon from '@/ui/Icon';
import s from './Success.module.css';

const Success: React.FC = () => {
    return (
        <section className={s.wrap}>
            <header className={s.header}>
                <div className={s['image-wrap']}>
                    <img src="/images/logo-mark.webp" alt="Health Guard Shield Logo " />
                </div>
                <Typography className={s.title} variant="title-h4" asChild>
                    <h2>Thank you for registering, please check your inbox!</h2>
                </Typography>
                <Typography className={s.subtitle} variant="paragraph-base" asChild>
                    <p>
                        Please make sure to verify your account by clicking on the link in the email we just sent to
                        your inbox.
                    </p>
                </Typography>
            </header>
            <footer className={s.footer}>
                <Button className={s.cta} variant="primary" asChild>
                    <Link to="/login">
                        Back to login
                        <Icon className={s.icon} id="icon-arrow-right_18" />
                    </Link>
                </Button>
            </footer>
        </section>
    );
};

export default Success;
