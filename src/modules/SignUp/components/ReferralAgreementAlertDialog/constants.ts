/* eslint-disable no-irregular-whitespace */
/* eslint-disable no-useless-escape */

export const REFERRAL_AGREEMENT_TEXT = `
### FOREWORD

Our affiliates are very important to us. We do our best to treat you with the fairness and respect you deserve. We simply ask the same consideration of you. We have written the following affiliate agreement with you in mind, as well as to protect our company.. Please bear with us as we take you through this legal formality.

If you have any questions, please don't hesitate to let us know. We are strong believers in straight-forward and honest communication. For quickest results please email us at hello@triton.finance.

Best regards,

The Triton Team

### AFFILIATE AGREEMENT

PLEASE READ THE ENTIRE AGREEMENT.

YOU MAY PRINT THIS PAGE FOR YOUR RECORDS.

THIS IS A LEGAL AGREEMENT BETWEEN YOU AND TRITON CONSULTING, LLC which can also be referred to as "Triton" or "Triton Finance" throughout this agreement.

BY SUBMITTING THE ONLINE APPLICATION YOU ARE AGREEING THAT YOU HAVE READ AND UNDERSTAND THE TERMS AND CONDITIONS OF THIS AGREEMENT AND THAT YOU AGREE TO BE LEGALLY RESPONSIBLE FOR EACH AND EVERY TERM AND CONDITION.

#### 1\. Overview

This Agreement contains the complete terms and conditions that apply to you becoming an affiliate in Triton's Affiliate Program. The purpose of this Agreement is to allow HTML linking between your web site and the Triton web site. Please note that throughout this Agreement, "we," "us," and "our" refer to Triton, and "you," "your," and "yours" refer to the affiliate.

#### 2\. Affiliate Obligations

2.1. To begin the enrollment process, you will complete and submit the online application at the referral link provided to you. The fact that we auto-approve applications does not imply that we may not re-evaluate your application at a later time. We may reject your application at our sole discretion. We may cancel your application if we determine that your site is unsuitable for our Program, including if it:

2.1.1. Promotes sexually explicit materials\
2.1.2. Promotes violence\
2.1.3. Promotes discrimination based on race, sex, religion, nationality, disability, sexual orientation, or age\
2.1.4. Promotes illegal activities\
2.1.5. Incorporates any materials which infringe or assist others to infringe on any copyright, trademark or other intellectual property rights or to violate the law\
2.1.6. Includes "Triton" or variations or misspellings thereof in its domain name\
2.1.7. Is otherwise in any way unlawful, harmful, threatening, defamatory, obscene, harassing, or racially, ethnically or otherwise objectionable to us in our sole discretion.\
2.1.8. Contains software downloads that potentially enable diversions of commission from other affiliates in our program.\
2.1.9. You may not create or design your website or any other website that you operate, explicitly or implied in a manner which resembles our website nor design your website in a manner which leads customers to believe you are Triton or any other Triton affiliated business.  This does not apply to embeddable web tools provided directly by Triton.

2.2. As a member of Triton's Affiliate Program, you will have access to our Affiliate Account Dashboard. Here you will be able to review our Program's details and previously-published affiliate newsletters, download code (that provides for links to web pages within the Triton web site) and banner creatives, browse and get tracking codes for our coupons and deals. In order for us to accurately keep track of all guest visits from your site to ours, you must use the code or affiliate links that we provide.

2.3. Triton reserves the right, at any time, to review your placement and approve the use of your links and require that you change the placement or use to comply with the guidelines provided to you.

2.4. The maintenance and the updating of your own site will be your responsibility. We may monitor your site as we feel necessary to make sure that it is up-to-date and to notify you of any changes that we feel should enhance your performance.

2.5. It is entirely your responsibility to follow all applicable intellectual property and other laws that pertain to your site. You must have express permission to use any person's copyrighted material, whether it be a writing, an image, or any other copyrightable work. We will not be responsible (and you will be solely responsible) if you use another person's copyrighted material or other intellectual property in violation of the law or any third party rights.

#### 3\. Triton Rights and Obligations

3.1. We have the right to monitor your site at any time to determine if you are following the terms and conditions of this Agreement. We may notify you of any changes to your site that we feel should be made, or to make sure that your links to our web site are appropriate and to notify further you of any changes that we feel should be made. If you do not make the changes to your site that we feel are necessary, we reserve the right to terminate your participation in the Triton Affiliate Program.

3.2. Triton reserves the right to terminate this Agreement and your participation in the Triton Affiliate Program immediately and without notice to you should you commit fraud in your use of the Triton Affiliate Program or should you violate this Agreement or misuse, in our sole discretion, this program in any way. If such fraud or abuse is detected, Triton shall not be liable to you for any commissions for such fraudulent sales as determined by Triton.

3.3. This Agreement will begin upon our acceptance of your Affiliate application, and will continue unless terminated hereunder.

#### 4\. Termination

Either you or we may end this Agreement AT ANY TIME, with or without cause, by giving the other party written notice, except in instances of termination pursuant to Section 3.2 of this Agreement. Written notice can be in the form of mail or email. In addition, this Agreement will terminate immediately upon any breach of this Agreement by you.

#### 5\. Modification

We may modify any of the terms and conditions in this Agreement at any time at our sole discretion. In such event, you will be notified by email. Modifications may include, but are not limited to, changes in the payment procedures and Triton's Affiliate Program rules. If any modification is unacceptable to you, you may elect to end this Agreement. Your continued participation in Triton's Affiliate Program following the posting of the change notice or new Agreement on our site will indicate your agreement to the changes.

#### 6\. Payment

Triton uses Paypal as its primary payment software and may also use other secondary payment softwares as needed. 

#### 7\. Access to Affiliate Account Interface

You will create a password so that you may enter the Triton Affiliate Dashboard and secure affiliate account interface. From this site you will be able to receive your reports that will describe our calculation of the commissions due to you, track commissions, referrals, etc. 

#### 8\. Promotion Restrictions

8.1. You are free to promote your own web sites, but naturally any promotion that mentions Triton could be perceived by the public or the press as a joint effort. You should know that certain forms of advertising are always prohibited by Triton. For example, advertising commonly referred to as "spamming" is unacceptable to us and could cause damage to our name and business reputation, which we take very seriously. Other generally prohibited forms of advertising include the use of unsolicited commercial email (UCE), postings to non-commercial newsgroups and cross-posting to multiple newsgroups at once. In addition, you may not advertise in any way that effectively conceals or misrepresents your identity, your domain name, or your return email address. You may use mailings to customers to promote Triton so long as the recipient is already a customer or subscriber of your services or web site, and recipients have the option to remove themselves from future mailings. Also, you may post to newsgroups to promote Triton so long as the news group specifically welcomes commercial messages. At all times, you must clearly represent yourself and your web sites as independent from Triton. If it comes to our attention that you are spamming, we will consider that cause for immediate termination of this Agreement and your participation in the Triton Affiliate Program. Any pending balances owed to you will not be paid if your account is terminated due to such unacceptable advertising or solicitation.

8.2. Qualified Customer:

A qualified customer is defined as a customer residing in our Qualified States List* that has $15,000 of more in unsecured credit card or personal loan debt which is in no way collateralized against an asset or cross-collateralized in a instance where a debt is held with a credit union.

Qualified States List: Alabama, Alaska, Arizona, Arkansas, California, Connecticut, Delaware, District of Columbia, Florida, Georgia, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana, Nebraska, Nevada, New Hampshire, New Mexico, New York, North Carolina, Ohio, Oklahoma, Pennsylvania, South Carolina, South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, West Virginia, Wisconsin, Wyoming

8.3. Affiliates are not prohibited from keying in prospect's information into the lead form as long as the prospects' information is real and true, and these are valid leads (i.e. sincerely interested in Triton's service).  Entering false or misleading lead information is grounds for termination of your Agreement.

8.4. Affiliate shall not transmit any so-called "interstitials," "Parasiteware™," "Parasitic Marketing," "Shopping Assistance Application," "Toolbar Installations and/or Add-ons," "Shopping Wallets" or "deceptive pop-ups and/or pop-unders" to consumers from the time the consumer clicks on a qualifying link until such time as the consumer has fully exited Triton's site (i.e., no page from our site or any Triton's content or branding is visible on the end-user's screen). As used herein a. "Parasiteware™" and "Parasitic Marketing" shall mean an application that (a) through accidental or direct intent causes the overwriting of affiliate and non affiliate commission tracking cookies through any other means than a customer initiated click on a qualifying link on a web page or email; (b) intercepts searches to redirect traffic through an installed software, thereby causing, pop ups, commission tracking cookies to be put in place or other commission tracking cookies to be overwritten where a user would under normal circumstances have arrived at the same destination through the results given by the search (search engines being, but not limited to, Google, MSN, Yahoo, Overture, AltaVista, Hotbot and similar search or directory engines); (c) set commission tracking cookies through loading of Triton site in IFrames, hidden links and automatic pop ups that open Triton's site; (d) targets text on web sites, other than those web sites 100% owned by the application owner, for the purpose of contextual marketing; (e) removes, replaces or blocks the visibility of Affiliate banners with any other banners, other than those that are on web sites 100% owned by the owner of the application.

#### 9\. Grant of Licenses

9.1. We grant to you a non-exclusive, non-transferable, revocable right to (i) access our site through affiliate links solely in accordance with the terms of this Agreement and (ii) solely in connection with such links, to use our logos, trade names, trademarks, and similar identifying material (collectively, the "Licensed Materials") that we provide to you or authorize for such purpose. You are only entitled to use the Licensed Materials to the extent that you are a member in good standing of Triton's Affiliate Program. You agree that all uses of the Licensed Materials will be on behalf of Triton and the good will associated therewith will inure to the sole benefit of Triton.

9.2. Each party agrees not to use the other's proprietary materials in any manner that is disparaging, misleading, obscene or that otherwise portrays the party in a negative light. Each party reserves all of its respective rights in the proprietary materials covered by this license. Other than the license granted in this Agreement, each party retains all right, title, and interest to its respective rights, including but not limited to all intellectual property rights, and no right, title, or interest is transferred to the other.

#### 10\. Disclaimer

Triton MAKES NO EXPRESS OR IMPLIED REPRESENTATIONS OR WARRANTIES REGARDING Triton SERVICE AND WEB SITE OR THE PRODUCTS OR SERVICES PROVIDED THEREIN, ANY IMPLIED WARRANTIES OF Triton ABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT ARE EXPRESSLY DISCLAIMED AND EXCLUDED. IN ADDITION, WE MAKE NO REPRESENTATION THAT THE OPERATION OF OUR SITE WILL BE UNINTERRUPTED OR ERROR FREE, AND WE WILL NOT BE LIABLE FOR THE CONSEQUENCES OF ANY INTERRUPTIONS OR ERRORS.

#### 11\. Representations and Warranties

You represent and warrant that:

11.1. This Agreement has been duly and validly executed and delivered by you and constitutes your legal, valid, and binding obligation, enforceable against you in accordance with its terms;

11.2. You have the full right, power, and authority to enter into and be bound by the terms and conditions of this Agreement and to perform your obligations under this Agreement, without the approval or consent of any other party;

11.3. You have sufficient right, title, and interest in and to the rights granted to us in this Agreement.

#### 12\. Limitations of Liability

WE WILL NOT BE LIABLE TO YOU WITH RESPECT TO ANY SUBJECT MATTER OF THIS AGREEMENT UNDER ANY CONTRACT, NEGLIGENCE, TORT, STRICT LIABILITY OR OTHER LEGAL OR EQUITABLE THEORY FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR EXEMPLARY DAMAGES (INCLUDING, WITHOUT LIMITATION, LOSS OF REVENUE OR GOODWILL OR ANTICIPATED PROFITS OR LOST BUSINESS), EVEN IF WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. FURTHER, NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED IN THIS AGREEMENT, IN NO EVENT SHALL Triton'S CUMULATIVE LIABILITY TO YOU ARISING OUT OF OR RELATED TO THIS AGREEMENT, WHETHER BASED IN CONTRACT, NEGLIGENCE, STRICT LIABILITY, TORT OR OTHER LEGAL OR EQUITABLE THEORY, EXCEED THE TOTAL COMMISSION FEES PAID TO YOU UNDER THIS AGREEMENT.

#### 13\. Indemnification

You hereby agree to indemnify and hold harmless Triton, and its subsidiaries and affiliates, and their directors, officers, employees, agents, shareholders, partners, members, and other owners, against any and all claims, actions, demands, liabilities, losses, damages, judgments, settlements, costs, and expenses (including reasonable attorneys' fees) (any or all of the foregoing hereinafter referred to as "Losses") insofar as such Losses (or actions in respect thereof) arise out of or are based on (i) any claim that our use of the affiliate trademarks infringes on any trademark, trade name, service mark, copyright, license, intellectual property, or other proprietary right of any third party, (ii) any misrepresentation of a representation or warranty or breach of a covenant and agreement made by you herein, or (iii) any claim related to your site, including, without limitation, content therein not attributable to us.

#### 14\. Confidentiality

All confidential information, including, but not limited to, any business, technical, financial, and customer information, disclosed by one party to the other during negotiation or the effective term of this Agreement which is marked "Confidential," will remain the sole property of the disclosing party, and each party will keep in confidence and not use or disclose such proprietary information of the other party without express written permission of the disclosing party.

#### 15\. Miscellaneous

15.1. You agree that you are an independent contractor, and nothing in this Agreement will create any partnership, joint venture, agency, franchise, sales representative, or employment relationship between you and Triton. You will have no authority to make or accept any offers or representations on our behalf. You will not make any statement, whether on Your Site or any other of Your Site or otherwise, that reasonably would contradict anything in this Section.

15.2. Neither party may assign its rights or obligations under this Agreement to any party, except to a party who obtains all or substantially all of the business or assets of a third party.

15.3. This Agreement shall be governed by and interpreted in accordance with the laws of the State of Delaware without regard to the conflicts of laws and principles thereof.

15.4. You may not amend or waive any provision of this Agreement unless in writing and signed by both parties.

15.5. This Agreement represents the entire agreement between us and you, and shall supersede all prior agreements and communications of the parties, oral or written.

15.6. The headings and titles contained in this Agreement are included for convenience only, and shall not limit or otherwise affect the terms of this Agreement.

15.7. If any provision of this Agreement is held to be invalid or unenforceable, that provision shall be eliminated or limited to the minimum extent necessary such that the intent of the parties is effectuated, and the remainder of this agreement shall have full force and effect.`;
