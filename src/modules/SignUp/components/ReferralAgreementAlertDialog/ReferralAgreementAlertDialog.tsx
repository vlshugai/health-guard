import { useMemo } from 'react';
import { Link, getRouteApi } from '@tanstack/react-router';
import { format } from 'date-fns';
import {
    Title as AlertDialogTitle,
    Description as AlertDialogDescription,
    Action as AlertDialogAction,
} from '@radix-ui/react-alert-dialog';
import clsx from 'clsx';
import { REFERRAL_AGREEMENT_TEXT } from './constants';
import AlertDialogClose from '@/components/AlertDialog/components/AlertDialogClose';
import AlertDialog from '@/components/AlertDialog';
import Markdown from '@/components/Markdown';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import Icon from '@/ui/Icon';
import s from './ReferralAgreementAlertDialog.module.css';

const routeApi = getRouteApi('/_sign-up-layout/sign-up/_payments-layout/payments/affiliate-agreement');

const ReferralAgreementAlertDialog: React.FC = () => {
    const search = routeApi.useSearch();

    const today = useMemo(() => {
        return format(new Date(), 'MMMM do, yyyy');
    }, []);

    return (
        <AlertDialog>
            <AlertDialogClose>
                <Link className={clsx(s['close-link'], 'focus-primary')} to="/sign-up/payments" search={search}>
                    <Icon className={s.icon} id="icon-close_18" />
                </Link>
            </AlertDialogClose>
            <header className={s.header}>
                <AlertDialogTitle asChild>
                    <Typography className={s.title} variant="title-h4" asChild>
                        <h1>Referral Agreement</h1>
                    </Typography>
                </AlertDialogTitle>
                <AlertDialogDescription className={s.subtitle} asChild>
                    <Typography variant="paragraph-base" asChild>
                        <span>Effective Date: {today}</span>
                    </Typography>
                </AlertDialogDescription>
            </header>
            <div className={s['agreement-wrap']}>
                <Markdown className={s.agreement}>{REFERRAL_AGREEMENT_TEXT}</Markdown>
            </div>
            <footer>
                <AlertDialogAction asChild>
                    <Button variant="primary" asChild>
                        <Link to="/sign-up/create-password" search={search}>
                            <Typography variant="paragraph-base" asChild>
                                <span>I Agree</span>
                            </Typography>
                        </Link>
                    </Button>
                </AlertDialogAction>
            </footer>
        </AlertDialog>
    );
};

export default ReferralAgreementAlertDialog;
