import { useCallback, useEffect, useMemo } from 'react';
import { Link, getRouteApi, useNavigate } from '@tanstack/react-router';
import { NumericFormat, PatternFormat } from 'react-number-format';
import { SingleValue } from 'react-select';
import clsx from 'clsx';
import type { SelectOptionType } from '@/types';
import type { SignUpContactFormType } from '../../types';
import { useStatesAndCities } from '@/hooks/useStatesAndCities';
import { useSignUpContactForm } from './hooks/useSignUpContactForm';
import Select from '@/components/Select';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import Input from '@/ui/Input';
import Icon from '@/ui/Icon';
import s from './ContactsForm.module.css';

const signUpRouteApi = getRouteApi('/_sign-up-layout');
const routeApi = getRouteApi('/_sign-up-layout/sign-up/contacts');

const ContactsForm: React.FC = () => {
    const signUpLoaderData = signUpRouteApi.useLoaderData();
    const search = routeApi.useSearch();

    const navigate = useNavigate({ from: '/sign-up/contacts' });

    const { statesOptions, citiesByStateOptions, citiesObject, statesObject } = useStatesAndCities();

    const {
        firstNameController: {
            field: firstNameField,
            fieldState: { error: firstNameError },
        },
        lastNameController: {
            field: lastNameField,
            fieldState: { error: lastNameError },
        },
        businessNameController: {
            field: businessNameField,
            fieldState: { error: businessNameError },
        },
        addressController: {
            field: addressField,
            fieldState: { error: addressError },
        },
        emailController: {
            field: emailField,
            fieldState: { error: emailError },
        },
        phoneController: {
            field: phoneField,
            fieldState: { error: phoneError },
        },
        stateController: {
            field: stateField,
            fieldState: { error: stateError },
        },
        cityController: {
            field: cityField,
            fieldState: { error: cityError },
        },
        zipController: {
            field: zipField,
            fieldState: { error: zipError },
        },
        sponsorIdController: {
            field: sponsorIdField,
            fieldState: { error: sponsorIdError },
        },
        homeStateHealthInsuranceController: {
            field: homeStateHealthInsuranceField,
            fieldState: { error: homeStateHealthInsuranceError },
        },
        homeStateLifeInsuranceController: {
            field: homeStateLifeInsuranceField,
            fieldState: { error: homeStateLifeInsuranceError },
        },
        handleSubmit,
    } = useSignUpContactForm({
        ...search,
        sponsorId: signUpLoaderData.isWithSponsor ? signUpLoaderData.sponsorId : '',
    } as SignUpContactFormType);

    const selectedState = useMemo(() => {
        if (stateField.value) {
            return statesObject?.[stateField.value];
        }

        return null;
    }, [stateField.value, statesObject]);

    const selectedCity = useMemo(() => {
        if (cityField.value) {
            return citiesObject?.[cityField.value];
        }

        return null;
    }, [cityField.value, citiesObject]);

    const citiesOptions = useMemo(() => {
        if (!stateField.value || !citiesByStateOptions || !statesOptions.length) {
            return [];
        }

        return citiesByStateOptions[stateField.value];
    }, [citiesByStateOptions, stateField.value, statesOptions.length]);

    const submitHandler = handleSubmit(
        (data) => {
            // eslint-disable-next-line no-console
            console.log(data);

            navigate({
                to: '/sign-up/payments',
                search,
            });
        },
        (error) => {
            // eslint-disable-next-line no-console
            console.error(error);
        }
    );

    const zipCodeChangeHandler = useCallback(
        (numberFormatValue: { value: string }) => {
            zipField.onChange(numberFormatValue.value);
        },
        [zipField]
    );

    const sponsorIdChangeHandler = useCallback(
        (numberFormatValue: { value: string }) => {
            sponsorIdField.onChange(numberFormatValue.value);
        },
        [sponsorIdField]
    );

    const phoneChangeHandler = useCallback(
        (numberFormatValue: { value: string }) => {
            phoneField.onChange(numberFormatValue.value);
        },
        [phoneField]
    );

    useEffect(() => {
        if (signUpLoaderData.isWithSponsor && signUpLoaderData.sponsorId) {
            sponsorIdField.onChange(signUpLoaderData.sponsorId);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [signUpLoaderData.isWithSponsor, signUpLoaderData.sponsorId]);

    return (
        <section className={s.wrap}>
            <header className={s.header}>
                <Typography className={s.title} variant="title-h4" asChild>
                    <h2>Sign up</h2>
                </Typography>
                <Typography className={s.subtitle} variant="paragraph-base" asChild>
                    <p>Please complete the form below so we can contact you for onboarding and payment purposes.</p>
                </Typography>
            </header>
            <form className={s.form} onSubmit={submitHandler} noValidate>
                <div className={s['fields-wrap']}>
                    <Input
                        className={s.field}
                        type="text"
                        label="First Name"
                        placeholder="First Name"
                        enterKeyHint="next"
                        autoFocus
                        value={firstNameField.value}
                        onChange={firstNameField.onChange}
                        errorMessage={firstNameError?.message}
                    />
                    <Input
                        className={s.field}
                        type="text"
                        label="Last Name"
                        placeholder="Last Name"
                        enterKeyHint="next"
                        value={lastNameField.value}
                        onChange={lastNameField.onChange}
                        errorMessage={lastNameError?.message}
                    />
                    <Input
                        className={s.field}
                        type="text"
                        label="Entity/Business Name (If Applicable)"
                        placeholder="Entity/Business Name"
                        enterKeyHint="next"
                        value={businessNameField.value}
                        onChange={businessNameField.onChange}
                        errorMessage={businessNameError?.message}
                    />
                    <Input
                        className={s.field}
                        type="email"
                        label="Email"
                        placeholder="Email"
                        enterKeyHint="next"
                        value={emailField.value}
                        onChange={emailField.onChange}
                        errorMessage={emailError?.message}
                    />
                    <PatternFormat
                        className={s.field}
                        format="(###) ###-####"
                        mask="_"
                        type="tel"
                        label="Phone Number"
                        placeholder="Phone Number"
                        enterKeyHint="next"
                        value={phoneField.value}
                        errorMessage={phoneError?.message}
                        customInput={Input}
                        autoComplete="off"
                        onValueChange={phoneChangeHandler}
                    />
                    <Input
                        className={s.field}
                        type="text"
                        label="Street Address"
                        placeholder="Street Address"
                        enterKeyHint="next"
                        value={addressField.value}
                        errorMessage={addressError?.message}
                        onChange={addressField.onChange}
                    />
                    <Select
                        className={s.field}
                        label="State"
                        placeholder="Select state"
                        options={statesOptions}
                        isSearchable
                        value={selectedState}
                        errorMessage={stateError?.message}
                        onChange={(value) => {
                            stateField.onChange((value as SingleValue<SelectOptionType>)?.value);
                            cityField.onChange('');
                        }}
                    />
                    <Select
                        className={s.field}
                        label="City"
                        placeholder="Select city"
                        options={citiesOptions}
                        isSearchable
                        value={selectedCity}
                        disabled={!stateField.value}
                        onChange={(value) => {
                            cityField.onChange((value as SingleValue<SelectOptionType>)?.value);
                        }}
                        errorMessage={cityError?.message}
                    />
                    <PatternFormat
                        className={s.field}
                        type="text"
                        label="Zip Code"
                        placeholder="Enter zip code"
                        enterKeyHint="next"
                        value={zipField.value}
                        errorMessage={zipError?.message}
                        customInput={Input}
                        autoComplete="off"
                        format="#####"
                        mask="_"
                        onValueChange={zipCodeChangeHandler}
                    />
                    <NumericFormat
                        className={s.field}
                        type="text"
                        label="Sponsor ID"
                        placeholder="Sponsor ID"
                        value={sponsorIdField.value}
                        errorMessage={sponsorIdError?.message}
                        customInput={Input}
                        autoComplete="off"
                        disabled={!!signUpLoaderData?.subId}
                        maxLength={8}
                        allowLeadingZeros={false}
                        onValueChange={sponsorIdChangeHandler}
                    />
                    <NumericFormat
                        className={s.field}
                        type="text"
                        label="Home State Life Insurance Number"
                        placeholder="Home State Life Insurance Number"
                        value={homeStateLifeInsuranceField.value}
                        errorMessage={homeStateLifeInsuranceError?.message}
                        customInput={Input}
                        autoComplete="off"
                        onValueChange={(numberFormatValue) => {
                            homeStateLifeInsuranceField.onChange(numberFormatValue.value);
                        }}
                    />
                    <NumericFormat
                        className={s.field}
                        type="text"
                        label="Home State Health Insurance Number"
                        placeholder="Home State Health Insurance Number"
                        value={homeStateHealthInsuranceField.value}
                        errorMessage={homeStateHealthInsuranceError?.message}
                        customInput={Input}
                        autoComplete="off"
                        onValueChange={(numberFormatValue) => {
                            homeStateHealthInsuranceField.onChange(numberFormatValue.value);
                        }}
                    />
                </div>
                <footer className={s.footer}>
                    <Button className={clsx(s.cta, s.back)} variant="outline" asChild>
                        <Link
                            to="/"
                            search={{
                                subId: signUpLoaderData?.subId,
                            }}
                        >
                            Back
                        </Link>
                    </Button>
                    <Button className={clsx(s.cta, s.next)} variant="primary" type="submit">
                        Next
                        <Icon className={s.icon} id="icon-arrow-right_18" />
                    </Button>
                </footer>
            </form>
        </section>
    );
};

export default ContactsForm;
