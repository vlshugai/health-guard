import { useEffect } from 'react';
import { useNavigate } from '@tanstack/react-router';
import { useDebouncedCallback } from '@react-hookz/web';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import type { SignUpContactFormType } from '@/modules/SignUp/types';
import { useFieldController } from '@/hooks/useFieldController';
import { signUpContactFormSchema } from '@/modules/SignUp/schemas';

export const useSignUpContactForm = (initialValues: SignUpContactFormType & { subId?: string }) => {
    const navigate = useNavigate({ from: '/sign-up/contacts' });

    const { control, ...rest } = useForm<SignUpContactFormType>({
        resolver: zodResolver(signUpContactFormSchema),
        defaultValues: initialValues,
        reValidateMode: 'onChange',
    });

    const sponsorIdChangeHandler = useDebouncedCallback(
        (newValue: string) => {
            rest.clearErrors('sponsorId');

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        sponsorId: newValue,
                    };
                },
                replace: true,
            });
        },
        [],
        300
    );

    const firstNameController = useFieldController(
        {
            control,
            name: 'firstName',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        firstName: newValue,
                    };
                },
            });
        }
    );
    const lastNameController = useFieldController(
        {
            control,
            name: 'lastName',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        lastName: newValue,
                    };
                },
            });
        }
    );
    const businessNameController = useFieldController(
        {
            control,
            name: 'businessName',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        businessName: newValue,
                    };
                },
            });
        }
    );
    const addressController = useFieldController(
        {
            control,
            name: 'address',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        address: newValue,
                    };
                },
            });
        }
    );
    const emailController = useFieldController(
        {
            control,
            name: 'email',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        email: newValue,
                    };
                },
            });
        }
    );
    const phoneController = useFieldController(
        {
            control,
            name: 'phone',
        },
        (newValue: string) => {
            navigate({
                search(prev) {
                    return {
                        ...prev,
                        phone: newValue,
                    };
                },
            });
        }
    );
    const stateController = useFieldController(
        {
            control,
            name: 'state',
        },
        (newValue: string) => {
            navigate({
                search(prev) {
                    return {
                        ...prev,
                        state: newValue,
                    };
                },
            });
        }
    );
    const cityController = useFieldController(
        {
            control,
            name: 'city',
        },
        (newValue: string) => {
            navigate({
                search(prev) {
                    return {
                        ...prev,
                        city: newValue,
                    };
                },
            });
        }
    );
    const zipController = useFieldController(
        {
            control,
            name: 'zip',
        },
        (newValue: string) => {
            navigate({
                search(prev) {
                    return {
                        ...prev,
                        zip: newValue,
                    };
                },
                replace: true,
            });
        }
    );
    const sponsorIdController = useFieldController(
        {
            control,
            name: 'sponsorId',
        },
        sponsorIdChangeHandler
    );
    const homeStateLifeInsuranceController = useFieldController(
        {
            control,
            name: 'homeStateLifeInsuranceNumber',
        },
        (newValue: string) => {
            navigate({
                search(prev) {
                    return {
                        ...prev,
                        homeStateLifeInsuranceNumber: newValue,
                    };
                },
                replace: true,
            });
        }
    );
    const homeStateHealthInsuranceController = useFieldController(
        {
            control,
            name: 'homeStateHealthInsuranceNumber',
        },
        (newValue: string) => {
            navigate({
                search(prev) {
                    return {
                        ...prev,
                        homeStateHealthInsuranceNumber: newValue,
                    };
                },
                replace: true,
            });
        }
    );
    const licensedStatesController = useFieldController({
        control,
        name: 'licensedStates',
    });

    useEffect(() => {
        if (!initialValues?.subId && !initialValues.sponsorId) {
            rest.setError('sponsorId', {
                message: 'Sponsor ID is required',
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [initialValues.sponsorId, initialValues?.subId]);

    return {
        firstNameController,
        lastNameController,
        businessNameController,
        addressController,
        emailController,
        phoneController,
        stateController,
        cityController,
        zipController,
        sponsorIdController,
        homeStateLifeInsuranceController,
        homeStateHealthInsuranceController,
        licensedStatesController,
        ...rest,
    };
};
