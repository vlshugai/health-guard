import { Link, getRouteApi, useNavigate } from '@tanstack/react-router';
import { HTTPError } from 'ky';
import clsx from 'clsx';
import type { CreateHubspotContactSuccess } from '@/api/hubspotHealthGuard';
import type { UserType } from '@/api/auth';
import { useCreateHubspotContact, useUpdateHubspotContact } from '@/hooks/api/useHubspotHealthGuardApi';
import { useVerifyFirebaseUser } from '@/hooks/api/useFirebaseApi';
import { useRegister } from '@/hooks/api/useAuthApi';
import { useSignUpCreatePasswordForm } from './hooks/useSignUpCreatePasswordForm';
import { getAffiliateById } from '@/api/firebase';
import { getAffiliateTierLevelFromSponsorLevel } from './utils/getAffiliateTierLevelFromSponsorLevel';
import { generateAffiliateId } from './utils/generateAffiliateId';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import Input from '@/ui/Input';
import Icon from '@/ui/Icon';
import s from './CreatePasswordForm.module.css';

const routeApi = getRouteApi('/_sign-up-layout/sign-up/create-password/');

const CreatePasswordForm: React.FC = () => {
    const search = routeApi.useSearch();

    const navigate = useNavigate({ from: '/sign-up/create-password' });

    const {
        passwordController: {
            field: passwordField,
            fieldState: { error: passwordError },
        },
        confirmedPasswordController: {
            field: confirmPasswordField,
            fieldState: { error: confirmPasswordError },
        },
        handleSubmit,
        setError,
    } = useSignUpCreatePasswordForm(search);

    const { mutateAsync: createHubspotContact, isPending: isHubspotContactCreating } = useCreateHubspotContact();
    const { mutateAsync: updateHubspotContact, isPending: isHubspotContactUpdating } = useUpdateHubspotContact();
    const { mutateAsync: register, isPending: isRegisterPending } = useRegister();
    const { mutateAsync: verifyFirebaseUser, isPending: isFirebaseUserVerifying } = useVerifyFirebaseUser();

    const isLoading =
        isHubspotContactCreating || isHubspotContactUpdating || isRegisterPending || isFirebaseUserVerifying;

    const submitHandler = handleSubmit(async () => {
        // eslint-disable-next-line no-console
        console.log(search);

        let isCriticalError = false;

        const affiliateId = generateAffiliateId();
        const sponsor = await getAffiliateById(search.sponsorId);
        const affiliatesTierLevel = getAffiliateTierLevelFromSponsorLevel(sponsor?.tree_level);
        let hubspotContactId = '';

        const userRequestData = {
            affiliate_id: affiliateId,
            email: search.email,
            firstname: search.firstName,
            lastname: search.lastName,
            phone: search.phone,
            state: search.state,
            city: search.city,
            zip: search.zip,
            type: 'Affiliate' as UserType,
            tree_level: affiliatesTierLevel,
            sponsor_id: search.sponsorId ? Number(search.sponsorId) : undefined,
            account: search.accountHolderName,
            company: search.businessName,
            routing: search.bankRoutingNumber,
            bank_account_holder_name: '',
            bank_name: search.bankName,
            bank_address: '',
            paypal_address: '',
            bank_city: '',
            bank_state_code: '',
            bank_zip_code: '',
        };

        try {
            const hubspotContact = await createHubspotContact(userRequestData);

            if ((hubspotContact as CreateHubspotContactSuccess)?.hubspot_contact_id) {
                hubspotContactId = (hubspotContact as CreateHubspotContactSuccess).hubspot_contact_id;
            }
        } catch (error) {
            const errorResponse = error as HTTPError;
            const errorData = await errorResponse.response.json();

            if (errorData?.email_used_by) {
                hubspotContactId = errorData.email_used_by;
            } else {
                isCriticalError = true;
                setError('password', {
                    message: 'Something went wrong. Please try again later.',
                });
                return;
            }
        }

        if (isCriticalError) {
            return;
        }

        await updateHubspotContact({
            email: search.email,
            hubspot_contact_id: hubspotContactId,
        });

        await register({
            ...userRequestData,
            password: search.password,
            'Hubspot ID': hubspotContactId,
            homeStateHealthInsuranceNumber: search.homeStateHealthInsuranceNumber,
            homeStateLifeInsuranceNumber: search.homeStateLifeInsuranceNumber,
            licensedStates: search.licensedStates,
        });

        // TODO: send email request instead
        await verifyFirebaseUser(search.email);

        navigate({
            to: '/sign-up/success',
        });
    });

    return (
        <section className={s.wrap}>
            <header className={s.header}>
                <Typography className={s.title} variant="title-h4" asChild>
                    <h2>Create password</h2>
                </Typography>
                <Typography className={s.subtitle} variant="paragraph-base" asChild>
                    <p>Password must be at least 8 characters long</p>
                </Typography>
            </header>
            <form className={s.form} onSubmit={submitHandler} noValidate>
                <div className={s['fields-wrap']}>
                    <Input
                        className={s.field}
                        type="password"
                        label="New Password"
                        placeholder="Password"
                        value={passwordField.value}
                        onChange={passwordField.onChange}
                        errorMessage={passwordError?.message}
                    />
                    <Input
                        className={s.field}
                        type="password"
                        label="Confirm Password"
                        placeholder="New Password"
                        value={confirmPasswordField.value}
                        onChange={confirmPasswordField.onChange}
                        errorMessage={confirmPasswordError?.message}
                    />
                </div>
                <footer className={s.footer}>
                    <div className={s['cta-wrap']}>
                        <Button className={clsx(s.cta, s.back)} variant="outline" asChild>
                            <Link
                                to="/sign-up/payments"
                                search={{
                                    ...search,
                                    password: '',
                                    confirmedPassword: '',
                                }}
                                disabled={isLoading}
                            >
                                Back
                            </Link>
                        </Button>
                        <Button className={clsx(s.cta, s.next)} variant="primary" type="submit" disabled={isLoading}>
                            Next
                            <Icon className={s.icon} id="icon-arrow-right_18" />
                        </Button>
                    </div>

                    <Typography className={s['login-wrap']} variant="paragraph-sm" asChild>
                        <h5>
                            Already have an account?&nbsp;
                            <Typography className={clsx(s.link, 'focus-primary')} variant="link" asChild>
                                <Link to="/login" disabled={isLoading}>
                                    Log in
                                </Link>
                            </Typography>
                        </h5>
                    </Typography>
                </footer>
            </form>
        </section>
    );
};

export default CreatePasswordForm;
