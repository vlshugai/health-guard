import { useNavigate } from '@tanstack/react-router';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import type { SignUpCreatePasswordFormType } from '@/modules/SignUp/types';
import { useFieldController } from '@/hooks/useFieldController';
import { signUpCreatePasswordFormSchema } from '@/modules/SignUp/schemas';

export const useSignUpCreatePasswordForm = (initialValues: SignUpCreatePasswordFormType) => {
    const navigate = useNavigate({ from: '/sign-up/create-password' });

    const { control, ...rest } = useForm<SignUpCreatePasswordFormType>({
        resolver: zodResolver(signUpCreatePasswordFormSchema),
        defaultValues: initialValues,
        reValidateMode: 'onChange',
    });

    const passwordController = useFieldController(
        {
            control,
            name: 'password',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        password: newValue,
                    };
                },
            });
        }
    );
    const confirmedPasswordController = useFieldController(
        {
            control,
            name: 'confirmedPassword',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        confirmedPassword: newValue,
                    };
                },
            });
        }
    );

    return {
        passwordController,
        confirmedPasswordController,
        ...rest,
    };
};
