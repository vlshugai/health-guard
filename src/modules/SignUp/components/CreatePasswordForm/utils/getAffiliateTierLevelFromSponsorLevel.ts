export const getAffiliateTierLevelFromSponsorLevel = (level?: number) => {
    if (level === undefined) {
        return 0;
    }

    return level + 1;
};
