import { useNavigate } from '@tanstack/react-router';
import { useForm } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import type { SignUpPaymentFormType } from '@/modules/SignUp/types';
import { useFieldController } from '@/hooks/useFieldController';
import { signUpPaymentFormSchema } from '@/modules/SignUp/schemas';

export const useSignUpPaymentForm = (initialValues: SignUpPaymentFormType) => {
    const navigate = useNavigate({ from: '/sign-up/payments' });

    const { control, ...rest } = useForm<SignUpPaymentFormType>({
        resolver: zodResolver(signUpPaymentFormSchema),
        defaultValues: initialValues,
        reValidateMode: 'onChange',
    });

    const accountHolderNameController = useFieldController(
        {
            control,
            name: 'accountHolderName',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        accountHolderName: newValue,
                    };
                },
            });
        }
    );
    const bankNameController = useFieldController(
        {
            control,
            name: 'bankName',
        },
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;

            navigate({
                search(prev) {
                    return {
                        ...prev,
                        bankName: newValue,
                    };
                },
            });
        }
    );
    const bankRoutingNumberController = useFieldController(
        {
            control,
            name: 'bankRoutingNumber',
        },
        (newValue: string) => {
            navigate({
                search(prev) {
                    return {
                        ...prev,
                        bankRoutingNumber: newValue,
                    };
                },
            });
        }
    );
    const bankAccountNumberController = useFieldController(
        {
            control,
            name: 'bankAccountNumber',
        },
        (newValue: string) => {
            navigate({
                search(prev) {
                    return {
                        ...prev,
                        bankAccountNumber: newValue,
                    };
                },
            });
        }
    );

    return {
        accountHolderNameController,
        bankNameController,
        bankRoutingNumberController,
        bankAccountNumberController,
        ...rest,
    };
};
