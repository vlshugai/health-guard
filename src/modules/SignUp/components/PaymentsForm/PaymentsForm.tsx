import { useCallback } from 'react';
import { Link, getRouteApi, useNavigate } from '@tanstack/react-router';
import { NumericFormat } from 'react-number-format';
import clsx from 'clsx';
import { useSignUpPaymentForm } from './hooks/useSignUpPaymentForm';
import Typography from '@/ui/Typography';
import Button from '@/ui/Button';
import Input from '@/ui/Input';
import Icon from '@/ui/Icon';
import s from './PaymentsForm.module.css';

const routeApi = getRouteApi('/_sign-up-layout/sign-up/_payments-layout');

const PaymentsForm: React.FC = () => {
    const search = routeApi.useSearch();

    const navigate = useNavigate({ from: '/sign-up/payments' });

    const {
        accountHolderNameController: {
            field: accountHolderNameField,
            fieldState: { error: accountHolderNameError },
        },
        bankNameController: {
            field: bankNameField,
            fieldState: { error: bankNameError },
        },
        bankAccountNumberController: {
            field: bankAccountNumberFiled,
            fieldState: { error: bankAccountNumberError },
        },
        bankRoutingNumberController: {
            field: bankRoutingNumberField,
            fieldState: { error: bankRoutingNumberError },
        },
        handleSubmit,
    } = useSignUpPaymentForm(search);

    const submitHandler = handleSubmit(
        (data) => {
            // eslint-disable-next-line no-console
            console.log(data);

            navigate({
                to: '/sign-up/payments/affiliate-agreement',
                search,
            });
        },
        (error) => {
            // eslint-disable-next-line no-console
            console.error(error);
        }
    );

    const bankRoutingNumberChangeHandler = useCallback(
        (numberFormatValue: { value: string }) => {
            bankRoutingNumberField.onChange(numberFormatValue.value);
        },
        [bankRoutingNumberField]
    );

    const bankAccountNumberChangeHandler = useCallback(
        (numberFormatValue: { value: string }) => {
            bankAccountNumberFiled.onChange(numberFormatValue.value);
        },
        [bankAccountNumberFiled]
    );

    return (
        <section className={s.wrap}>
            <header className={s.header}>
                <Typography className={s.title} variant="title-h4" asChild>
                    <h2>Sign up</h2>
                </Typography>
                <Typography className={s.subtitle} variant="paragraph-base" asChild>
                    <p>
                        Please complete the form below to receive your payments. All payments will be sent via Paypal by
                        default. Banking info is collected as a secondary backup method to send your payments.
                    </p>
                </Typography>
            </header>
            <form className={s.form} onSubmit={submitHandler} noValidate>
                <div className={s['fields-wrap']}>
                    <Input
                        className={s.field}
                        type="text"
                        label="Account Holder Address"
                        placeholder="Account Holder Address"
                        enterKeyHint="next"
                        autoFocus
                        value={accountHolderNameField.value}
                        errorMessage={accountHolderNameError?.message}
                        onChange={accountHolderNameField.onChange}
                    />
                    <Input
                        className={s.field}
                        type="text"
                        label="Bank Name"
                        placeholder="Bank Name"
                        enterKeyHint="next"
                        autoFocus
                        value={bankNameField.value}
                        errorMessage={bankNameError?.message}
                        onChange={bankNameField.onChange}
                    />
                    <NumericFormat
                        className={s.field}
                        type="text"
                        label="Bank Routing Number"
                        placeholder="Bank Routing Number"
                        enterKeyHint="next"
                        autoFocus
                        value={bankRoutingNumberField.value}
                        errorMessage={bankRoutingNumberError?.message}
                        customInput={Input}
                        autoComplete="off"
                        allowNegative={false}
                        thousandSeparator={false}
                        decimalScale={0}
                        onValueChange={bankRoutingNumberChangeHandler}
                    />
                    <NumericFormat
                        className={s.field}
                        type="text"
                        label="Bank Account Number"
                        placeholder="Bank Account Number"
                        enterKeyHint="next"
                        autoFocus
                        value={bankAccountNumberFiled.value}
                        errorMessage={bankAccountNumberError?.message}
                        customInput={Input}
                        autoComplete="off"
                        allowNegative={false}
                        thousandSeparator={false}
                        decimalScale={0}
                        onValueChange={bankAccountNumberChangeHandler}
                    />
                </div>
                <footer className={s.footer}>
                    <Button className={clsx(s.cta, s.back)} variant="outline" asChild>
                        <Link to="/sign-up/contacts" search={search}>
                            Back
                        </Link>
                    </Button>
                    <Button className={clsx(s.cta, s.next)} variant="primary" type="submit">
                        Next
                        <Icon className={s.icon} id="icon-arrow-right_18" />
                    </Button>
                    <Typography className={clsx(s.cta, s.skip)} variant="link" asChild>
                        <Link
                            to="/sign-up/payments/affiliate-agreement"
                            search={{
                                ...search,
                                accountHolderName: '',
                                bankName: '',
                                bankRoutingNumber: '',
                                bankAccountNumber: '',
                            }}
                        >
                            Skip for now
                        </Link>
                    </Typography>
                </footer>
            </form>
        </section>
    );
};

export default PaymentsForm;
