import type { SignUpContactFormType, SignUpPaymentFormType, SignUpCreatePasswordFormType } from './types';

export const SIGN_UP_STEPPER_TITLES = ['Contact Form', 'Payment Form', 'Create password'];

export const SIGN_UP_CONTACT_STEP_INITIAL_VALUES: SignUpContactFormType = {
    firstName: '',
    lastName: '',
    businessName: '',
    address: '',
    email: '',
    phone: '',
    state: '',
    city: '',
    zip: '',
    sponsorId: '',
    homeStateLifeInsuranceNumber: '',
    homeStateHealthInsuranceNumber: '',
    licensedStates: '',
};

export const SIGN_UP_PAYMENT_STEP_INITIAL_VALUES: SignUpPaymentFormType = {
    accountHolderName: '',
    bankName: '',
    bankRoutingNumber: '',
    bankAccountNumber: '',
};

export const SIGN_UP_CREATE_PASSWORD_STEP_INITIAL_VALUES: SignUpCreatePasswordFormType = {
    password: '',
    confirmedPassword: '',
};
