import { useMemo } from 'react';
import { useChildMatches } from '@tanstack/react-router';
import { SIGN_UP_STEPPER_TITLES } from './constants';
import Stepper from '@/components/Stepper';
import s from './SignUp.module.css';

const SignUp: React.FC = () => {
    const childMatches = useChildMatches();

    const isContactsStep = useMemo(() => {
        return (
            childMatches.findIndex((match) => {
                return match.pathname.includes('/contacts');
            }) !== -1
        );
    }, [childMatches]);
    const isPaymentsStep = useMemo(() => {
        return (
            childMatches.findIndex((match) => {
                return match.pathname.includes('/payments');
            }) !== -1
        );
    }, [childMatches]);

    const currentStep = useMemo(() => {
        switch (true) {
            case isContactsStep:
                return 1;
            case isPaymentsStep:
                return 2;
            default:
                return SIGN_UP_STEPPER_TITLES.length;
        }
    }, [isContactsStep, isPaymentsStep]);

    return (
        <div className={s.wrap}>
            <Stepper currentStep={currentStep} stepsTitles={SIGN_UP_STEPPER_TITLES} />
        </div>
    );
};

export default SignUp;
