import z from 'zod';
import {
    signUpContactFormSchema,
    signUpCreatePasswordFormSchema,
    signUpFormSchema,
    signUpPaymentFormSchema,
} from './schemas';

export type SignUpContactFormType = z.infer<typeof signUpContactFormSchema>;
export type SignUpPaymentFormType = z.infer<typeof signUpPaymentFormSchema>;
export type SignUpCreatePasswordFormType = z.infer<typeof signUpCreatePasswordFormSchema>;

export type SignUpFormsType = z.infer<typeof signUpFormSchema>;
