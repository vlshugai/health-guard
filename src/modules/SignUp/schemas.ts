import z from 'zod';
import { convertFormSchemaToSearchParamsSchema } from '@/utils/convertFormSchemaToSearchParamsSchema';
import { EXTRACT_NUMBER_REGEX, FIELD_PASSWORD_MATCH, SPONSOR_MARKETING_ID_PREFIX } from '@/constants';
import { nameSchema, emailSchema, phoneSchema, stringSchema, zipCodeSchema, passwordCreateSchema } from '@/schemas';

export const signUpSponsorIdSearchParamsSchema = z.object({
    subId: z.coerce
        .string()
        .optional()
        .refine((value) => {
            const isSponsorId = value && value.startsWith(SPONSOR_MARKETING_ID_PREFIX);
            const isSponsorIdHasNumber = value && value.replace(EXTRACT_NUMBER_REGEX, '').length >= 6;

            return isSponsorId && isSponsorIdHasNumber;
        })
        .catch(''),
});

export const signUpContactFormSchema = z.object({
    firstName: nameSchema,
    lastName: nameSchema,
    businessName: z.string(),
    address: stringSchema,
    email: emailSchema,
    phone: phoneSchema,
    state: stringSchema,
    city: stringSchema,
    zip: zipCodeSchema,
    sponsorId: z
        .string()
        .min(1, 'Sponsor ID is required')
        .refine(
            (value) => {
                const isFirstDigitIsNotZero = value[0] !== '0';
                const isStringContainsSixDigits = value.replace(EXTRACT_NUMBER_REGEX, '').length >= 6;

                return (
                    (!isNaN(Number(value)) && isStringContainsSixDigits) ||
                    (isFirstDigitIsNotZero && isStringContainsSixDigits)
                );
            },
            {
                message: 'Invalid sponsor id',
            }
        ),
    homeStateLifeInsuranceNumber: stringSchema,
    homeStateHealthInsuranceNumber: stringSchema,
    // Splitted by comma
    // licensedStates: stringSchema,
    licensedStates: z.string(),
});

export const signUpPaymentFormSchema = z.object({
    accountHolderName: z.string(),
    bankName: z.string(),
    bankRoutingNumber: z.string(),
    bankAccountNumber: z.string(),
});

export const signUpCreatePasswordFormSWithoutRefineSchema = z.object({
    password: passwordCreateSchema,
    confirmedPassword: passwordCreateSchema,
});

export const signUpCreatePasswordFormSchema = signUpCreatePasswordFormSWithoutRefineSchema.refine(
    (data) => {
        return data.password === data.confirmedPassword;
    },
    {
        message: FIELD_PASSWORD_MATCH,
        path: ['confirmedPassword'],
    }
);

export const signUpFormSchema = z
    .object({})
    .merge(signUpContactFormSchema)
    .merge(signUpPaymentFormSchema)
    .merge(signUpCreatePasswordFormSWithoutRefineSchema);

export const signUpContactFieldsSearchParamsSchema = convertFormSchemaToSearchParamsSchema(signUpContactFormSchema);
export const signUpPaymentFieldsSearchParamsSchema = convertFormSchemaToSearchParamsSchema(signUpPaymentFormSchema);
export const signUpCreatePasswordFieldsSearchParamsSchema = convertFormSchemaToSearchParamsSchema(
    signUpCreatePasswordFormSWithoutRefineSchema
);
