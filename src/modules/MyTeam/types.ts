import z from 'zod';
import { myTeamSearchParamsSchema } from './schemas';

export type MyTeamSearchParams = z.infer<typeof myTeamSearchParamsSchema>;
