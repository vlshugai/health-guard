import z from 'zod';

export const myTeamSearchParamsSchema = z.object({
    levelOneSearch: z.string().default('').catch(''),
    levelTwoSearch: z.string().default('').catch(''),
});
