import MyTeamAffiliates from './components/MyTeamAffiliates';
import MyTeamSummary from './components/MyTeamSummary';
import s from './MyTeam.module.css';

const MyTeam: React.FC = () => {
    return (
        <div className={s.wrap}>
            <div className={s.inner}>
                <MyTeamSummary />
                <MyTeamAffiliates />
            </div>
        </div>
    );
};

export default MyTeam;
