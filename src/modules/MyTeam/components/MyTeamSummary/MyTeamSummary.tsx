import { memo } from 'react';
import { useGetFirebaseUserAffiliatesTeam } from '@/hooks/api/useFirebaseApi';
import Card from '@/components/Card';
import Typography from '@/ui/Typography';
import Container from '@/ui/Container';
import s from './MyTeamSummary.module.css';

const MyTeamSummary: React.FC = () => {
    const { data: team, isFetching } = useGetFirebaseUserAffiliatesTeam();

    return (
        <section className={s.wrap}>
            <Container className={s.container}>
                <header className={s.header}>
                    <Typography className={s.title} variant="title-h1" asChild>
                        <h1>My Team</h1>
                    </Typography>
                </header>
                <div className={s.content}>
                    <Card
                        className={s.card}
                        title="Level 1 Affiliates"
                        number={isFetching ? '--' : team?.totalFirstLevelAffiliates ?? 0}
                    />
                    <Card
                        className={s.card}
                        title="Level 2 Affiliates"
                        number={isFetching ? '--' : team?.totalSecondLevelAffiliates ?? 0}
                    />
                </div>
            </Container>
        </section>
    );
};

export default memo(MyTeamSummary);
