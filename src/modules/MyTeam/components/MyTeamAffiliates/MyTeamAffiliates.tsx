import { memo } from 'react';
import { useGetFirebaseUserAffiliatesTeam } from '@/hooks/api/useFirebaseApi';
import Typography from '@/ui/Typography';
import Container from '@/ui/Container';
import Loader from '@/ui/Loader';
import AffiliatesLevelTable from './components/AffiliatesLevelTable';
import s from './MyTeamAffiliates.module.css';

const MyTeamAffiliates: React.FC = () => {
    const { data: team, isFetching } = useGetFirebaseUserAffiliatesTeam();

    if (isFetching) {
        return (
            <div className={s['loader-wrap']}>
                <Loader className={s.loader} />
            </div>
        );
    }

    if (!team) {
        return (
            <div className={s['error-wrap']}>
                <Typography className={s['error-title']} variant="paragraph-lg">
                    Something went wrong. Please try again later.
                </Typography>
            </div>
        );
    }

    return (
        <Container>
            <AffiliatesLevelTable level={1} affiliates={team.firstLevelAffiliates} searchKey="levelOneSearch" />
            <AffiliatesLevelTable level={2} affiliates={team.secondLevelAffiliates} searchKey="levelTwoSearch" />
        </Container>
    );
};

export default memo(MyTeamAffiliates);
