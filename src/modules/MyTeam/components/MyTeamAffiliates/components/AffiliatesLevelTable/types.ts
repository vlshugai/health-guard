import type { HubspotContactTierLevel, WithClassName } from '@/types';
import type { Affiliate } from '@/api/firebase';
import type { MyTeamSearchParams } from '../../../../types';

export type AffiliatesLevelTableProps = WithClassName<{
    level: HubspotContactTierLevel;
    affiliates: Affiliate[];
    searchKey: keyof MyTeamSearchParams;
}>;
