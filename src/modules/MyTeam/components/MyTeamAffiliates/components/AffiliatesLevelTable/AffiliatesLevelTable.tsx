import { memo, useCallback, useMemo } from 'react';
import { useReactTable, getCoreRowModel, getPaginationRowModel } from '@tanstack/react-table';
import { PatternFormat } from 'react-number-format';
import clsx from 'clsx';
import type { ColumnDef, HeaderGroup } from '@tanstack/react-table';
import type { Affiliate } from '@/api/firebase';
import type { AffiliatesLevelTableProps } from './types';
import { useGetFirebaseUserAffiliatesTeam } from '@/hooks/api/useFirebaseApi';
import { useAffiliateLevelTableRows } from './hooks/useAffiliateLevelTableRows';
import { useAffiliateLevelFilter } from './hooks/useAffiliatesLevelFilter';
import { MY_TEAM_TABLE_PAGE_SIZE } from '../../../../constants';
import TablePagination from '@/components/Table/components/TablePagination';
import Table from '@/components/Table';
import Typography from '@/ui/Typography';
import Input from '@/ui/Input';
import AffiliatesLevelCSVExportButton from './components/AffiliatesLevelCSVExportButton';
import s from './AffiliatesLevelTable.module.css';

const AffiliatesLevelTable: React.FC<AffiliatesLevelTableProps> = ({ className, affiliates, level, searchKey }) => {
    const [searchValue, setSearchValue] = useAffiliateLevelFilter(searchKey);

    const {
        affiliates: filteredAffiliates,
        totalAffiliates,
        totalFilteredAffiliates,
        pagination,
        setPagination,
    } = useAffiliateLevelTableRows(affiliates, searchValue);

    const { isFetching } = useGetFirebaseUserAffiliatesTeam();

    const isTableDisabled = isFetching || totalAffiliates === 0;
    const isFilteredTableEmpty = totalFilteredAffiliates === 0;
    const isWithPagination = totalFilteredAffiliates > MY_TEAM_TABLE_PAGE_SIZE;

    const tableColumns = useMemo<ColumnDef<Affiliate>[]>(() => {
        return [
            {
                accessorKey: 'firstname',
                header: 'First Name',
                enableSorting: false,
                minSize: 226,
                maxSize: 226,
                cell({ getValue }) {
                    const firstName = getValue<Affiliate['firstname']>();

                    return (
                        <Typography
                            className={clsx(s.cell, 'truncate')}
                            variant="paragraph-sm"
                            title={firstName}
                            asChild
                        >
                            <span>{firstName}</span>
                        </Typography>
                    );
                },
            },
            {
                accessorKey: 'lastname',
                header: 'Last Name',
                enableSorting: false,
                minSize: 226,
                maxSize: 479,
                cell({ getValue }) {
                    const lastName = getValue<Affiliate['lastname']>();

                    return (
                        <Typography
                            className={clsx(s.cell, 'truncate')}
                            variant="paragraph-sm"
                            title={lastName}
                            asChild
                        >
                            <span>{lastName}</span>
                        </Typography>
                    );
                },
            },
            {
                accessorKey: 'phone',
                header: 'Phone number',
                enableSorting: false,
                minSize: 200,
                maxSize: 320,
                cell({ getValue }) {
                    const phone = getValue<Affiliate['phone']>();

                    if (!phone) {
                        return (
                            <Typography className={clsx(s.cell, 'truncate')} variant="paragraph-sm" asChild>
                                <span>-</span>
                            </Typography>
                        );
                    }

                    return (
                        <PatternFormat
                            className={clsx(s.cell, s.phone, 'truncate')}
                            displayType="text"
                            value={phone}
                            format="+1 (###) ###-##-##"
                        />
                    );
                },
            },
            {
                accessorKey: 'email',
                header: 'Email',
                enableSorting: false,
                minSize: 226,
                maxSize: 479,
                cell({ getValue }) {
                    const email = getValue<Affiliate['email']>();

                    return (
                        <Typography className={clsx(s.cell, 'truncate')} variant="paragraph-sm" title={email} asChild>
                            <span>{email}</span>
                        </Typography>
                    );
                },
            },
        ];
    }, []);

    const affiliatesTable = useReactTable({
        columns: tableColumns,
        data: filteredAffiliates,
        state: {
            pagination,
        },
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        onPaginationChange: setPagination,
    });
    const { rows } = affiliatesTable.getRowModel();

    const paginationPageChangeHandler = useCallback(
        (newPage: number) => {
            affiliatesTable.setPageIndex(newPage - 1);
        },
        [affiliatesTable]
    );

    return (
        <article className={clsx(s.wrap, className)}>
            <header className={s.header}>
                <Typography className={s.title} variant="title-h4" asChild>
                    <h2>Level&nbsp;{level}&nbsp;Affiliates</h2>
                </Typography>
            </header>
            <div className={s['actions-wrap']}>
                <Input
                    className={s.search}
                    type="search"
                    placeholder="Search Affiliate"
                    value={searchValue}
                    disabled={!affiliates.length}
                    onChange={setSearchValue}
                />

                <AffiliatesLevelCSVExportButton
                    className={clsx(s.cta, s.export)}
                    affiliates={rows}
                    level={level}
                    disabled={isTableDisabled || isFilteredTableEmpty}
                />
            </div>
            <Table
                className={clsx(s.table, {
                    [s['with-pagination']]: isWithPagination,
                })}
                headerGroups={affiliatesTable.getHeaderGroups() as unknown as HeaderGroup<unknown>[]}
                rows={rows}
                noRowsMessage={
                    !affiliates.length
                        ? 'You don’t have any affiliates yet. Share referral link to invite new affiliates.'
                        : 'Affiliates not found. Try another search query.'
                }
            />
            {isWithPagination ? (
                <TablePagination
                    className={s.pagination}
                    page={affiliatesTable.getState().pagination.pageIndex + 1}
                    total={totalFilteredAffiliates}
                    pageSize={affiliatesTable.getState().pagination.pageSize}
                    onPageChange={paginationPageChangeHandler}
                />
            ) : null}
        </article>
    );
};

export default memo(AffiliatesLevelTable);
