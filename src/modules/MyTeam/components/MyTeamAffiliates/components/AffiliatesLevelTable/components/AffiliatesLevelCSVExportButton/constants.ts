export const AFFILIATES_LEVEL_CSV_HEADER = [
    {
        key: 'firstName',
        label: 'First Name',
    },
    {
        key: 'lastName',
        label: 'Last Name',
    },
    {
        key: 'email',
        label: 'Email',
    },
];
