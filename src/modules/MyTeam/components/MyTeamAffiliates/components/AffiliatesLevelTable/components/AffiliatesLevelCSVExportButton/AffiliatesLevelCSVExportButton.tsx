import { memo } from 'react';
import { CSVLink } from 'react-csv';
import type { AffiliatesLevelCSVExportButtonProps } from './types';
import { useAffiliatesLevelCSV } from './hooks/useAffiliatesLevelCSV';
import { AFFILIATES_LEVEL_CSV_HEADER } from './constants';
import Button from '@/ui/Button';

const AffiliatesLevelCSVExportButton: React.FC<AffiliatesLevelCSVExportButtonProps> = ({
    affiliates,
    level,
    ...rest
}) => {
    const affiliatesCSV = useAffiliatesLevelCSV(affiliates, level);
    return (
        <Button {...rest} variant="primary" asChild>
            <CSVLink {...affiliatesCSV} headers={AFFILIATES_LEVEL_CSV_HEADER}>
                Export
            </CSVLink>
        </Button>
    );
};

export default memo(AffiliatesLevelCSVExportButton);
