import { useMemo } from 'react';
import type { Row } from '@tanstack/react-table';
import type { HubspotContactTierLevel } from '@/types';
import type { Affiliate } from '@/api/firebase';
import { useMe } from '@/hooks/useMe';

export const useAffiliatesLevelCSV = (rows: Row<Affiliate>[], level: HubspotContactTierLevel) => {
    const user = useMe();

    const filename = `${user.id}_affiliates_level_${level}.csv`;

    const data = useMemo(() => {
        return rows
            .map((row) => {
                return row.original;
            })
            .map((affiliate) => {
                return {
                    firstName: affiliate.firstname,
                    lastName: affiliate.lastname,
                    email: affiliate.email,
                };
            });
    }, [rows]);

    return { filename, data };
};
