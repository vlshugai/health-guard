import type { Row } from '@tanstack/react-table';
import type { HubspotContactTierLevel } from '@/types';
import type { Affiliate } from '@/api/firebase';
import type { ButtonProps } from '@/ui/Button/types';

export type AffiliatesLevelCSVExportButtonProps = Omit<ButtonProps, 'ref'> & {
    affiliates: Row<Affiliate>[];
    level: HubspotContactTierLevel;
};
