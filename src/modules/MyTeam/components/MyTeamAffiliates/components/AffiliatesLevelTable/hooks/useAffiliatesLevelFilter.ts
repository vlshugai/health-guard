import { useCallback } from 'react';
import { getRouteApi, useNavigate } from '@tanstack/react-router';
import type { MyTeamSearchParams } from '../../../../../types';

const routeApi = getRouteApi('/_dashboard-layout/my-team/');

export const useAffiliateLevelFilter = (key: keyof MyTeamSearchParams) => {
    const search = routeApi.useSearch();
    const navigate = useNavigate({ from: '/my-team' });

    const value = search[key] || '';

    const onSearchChange = useCallback(
        (event: React.ChangeEvent<HTMLInputElement>) => {
            const newValue = event.target.value;
            navigate({
                search: {
                    ...search,
                    [key]: newValue,
                },
                replace: true,
            });
        },
        [key, navigate, search]
    );

    return [value, onSearchChange] as const;
};
