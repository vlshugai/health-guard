import { useMemo, useState } from 'react';
import { useUpdateEffect } from '@react-hookz/web';
import type { PaginationState } from '@tanstack/react-table';
import type { Affiliate } from '@/api/firebase';
import { useDebounce } from '@/hooks/useDebounce';
import { MY_TEAM_TABLE_PAGE_SIZE } from '../../../../../constants';

export const useAffiliateLevelTableRows = (affiliates: Affiliate[], searchValue: string) => {
    const debouncedSearch = useDebounce(searchValue, 300);

    const [pagination, setPagination] = useState<PaginationState>({
        pageIndex: 0,
        pageSize: MY_TEAM_TABLE_PAGE_SIZE,
    });

    const totalAffiliates = affiliates.length;

    const filteredAffiliates = useMemo(() => {
        return affiliates.filter((affiliate) => {
            const isEmailMatch = affiliate.email.toLowerCase().includes(debouncedSearch.toLowerCase());
            const isFirstNameMatch = affiliate.firstname.toLowerCase().includes(debouncedSearch.toLowerCase());
            const isLastNameMatch = affiliate.lastname.toLowerCase().includes(debouncedSearch.toLowerCase());

            return isEmailMatch || isFirstNameMatch || isLastNameMatch;
        });
    }, [affiliates, debouncedSearch]);
    const totalFilteredAffiliates = filteredAffiliates.length;

    useUpdateEffect(() => {
        setPagination((prev) => {
            return {
                ...prev,
                pageIndex: 0,
            };
        });
    }, [debouncedSearch]);

    return {
        pagination,
        totalAffiliates,
        totalFilteredAffiliates,
        affiliates: filteredAffiliates,
        setPagination,
    };
};
