import { StrictMode } from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider, createRouter } from '@tanstack/react-router';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import '@/styles/index.css';

// Import the generated route tree
import { routeTree } from './routeTree.gen';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { HTTPError } from 'ky';
import { IS_DEV, STATUS_CODE_FORBIDDEN, STATUS_CODE_UNAUTHORIZED } from './constants';

// Create a new router instance
const router = createRouter({
    routeTree,
});

// Register the router instance for type safety
declare module '@tanstack/react-router' {
    interface Register {
        router: typeof router;
    }
}

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            retry(failureCount, error) {
                const isKyError = error instanceof HTTPError;
                const isTrying = failureCount < 2;

                if (!isKyError) {
                    return isTrying;
                }

                const statusCode = error.response?.status;

                if (statusCode === STATUS_CODE_UNAUTHORIZED || statusCode === STATUS_CODE_FORBIDDEN) {
                    return false;
                }

                return isTrying;
            },
        },
    },
});

// Render the app
const rootElement = document.getElementById('root')!;

if (!rootElement.innerHTML) {
    const root = ReactDOM.createRoot(rootElement);

    root.render(
        <StrictMode>
            <QueryClientProvider client={queryClient}>
                <RouterProvider router={router} />
                {IS_DEV && <ReactQueryDevtools />}
            </QueryClientProvider>
        </StrictMode>
    );
}
