import { useMemo } from 'react';
import { useQuery } from '@tanstack/react-query';
import type { SelectOptionType } from '@/types';
import { capitalize } from '@/utils/capitalize';

export type StateItem = {
    name: string;
    abbreviation: string;
};

export type CitiesPerStates = Record<string, string[]>;

export const useStatesAndCities = () => {
    const { data: statesResponse } = useQuery({
        queryKey: ['states'],
        async queryFn() {
            const statesResponse = await fetch('/states.json');
            const json: StateItem[] = await statesResponse.json();

            return json;
        },
        staleTime: Infinity,
    });

    const { data: citiesByStatesResponse } = useQuery({
        queryKey: ['cities-by-states'],
        async queryFn() {
            const citiesResponse = await fetch('/cities-by-state.json');
            const json: CitiesPerStates = await citiesResponse.json();

            return json;
        },
        staleTime: Infinity,
    });

    const states = useMemo(() => {
        if (!statesResponse?.length) {
            return {};
        }

        return statesResponse.reduce(
            (acc, state) => {
                acc[state.abbreviation] = {
                    label: state.name,
                    value: state.abbreviation,
                };

                return acc;
            },
            {} as Record<string, SelectOptionType>
        );
    }, [statesResponse]);

    const statesCodesByNameObject = useMemo(() => {
        if (!statesResponse?.length) {
            return {};
        }

        return statesResponse.reduce(
            (acc, state) => {
                acc[state.name] = state.abbreviation;

                return acc;
            },
            {} as Record<string, string>
        );
    }, [statesResponse]);

    const citiesByStates = useMemo(() => {
        if (!citiesByStatesResponse || !Object.keys(citiesByStatesResponse)?.length) {
            return {};
        }

        return Object.entries(citiesByStatesResponse).reduce(
            (acc, [state, cities]) => {
                acc[statesCodesByNameObject[state]] = cities.map((city) => {
                    const capitalizedCityName = capitalize(city);

                    return {
                        label: capitalizedCityName,
                        value: capitalizedCityName,
                    };
                });

                return acc;
            },
            {} as Record<string, SelectOptionType[]>
        );
    }, [citiesByStatesResponse, statesCodesByNameObject]);

    const cities = useMemo(() => {
        if (!citiesByStatesResponse || !Object.keys(citiesByStatesResponse)?.length) {
            return {};
        }

        return Object.values(citiesByStates)
            .flatMap((cityPerState) => {
                return cityPerState;
            })
            .reduce(
                (acc, city) => {
                    acc[city.value] = city;

                    return acc;
                },
                {} as Record<string, SelectOptionType>
            );
    }, [citiesByStates, citiesByStatesResponse]);

    return {
        statesOptions: Object.values(states),
        statesObject: states,
        citiesByStateOptions: citiesByStates,
        citiesObject: cities,
        statesNamesByCodeObject: statesCodesByNameObject,
    };
};
