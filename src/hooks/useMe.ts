import { getRouteApi } from '@tanstack/react-router';

const dashboardRouteApi = getRouteApi('/_dashboard-layout');

export const useMe = () => {
    const { user } = dashboardRouteApi.useLoaderData();

    return user;
};
