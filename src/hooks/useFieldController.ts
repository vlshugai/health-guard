import { useCallback } from 'react';
import { useController } from 'react-hook-form';
import type { FieldValues, FieldPath, UseControllerProps } from 'react-hook-form';

export const useFieldController = <
    TFieldValues extends FieldValues = FieldValues,
    TName extends FieldPath<TFieldValues> = FieldPath<TFieldValues>,
>(
    controllerProps: UseControllerProps<TFieldValues, TName>,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    onChangeCallback?: (event: any) => void
) => {
    const controller = useController<TFieldValues, TName>(controllerProps);

    const changeHandler = useCallback(
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (event: any) => {
            controller.field.onChange(event);
            onChangeCallback?.(event);
        },
        [controller.field, onChangeCallback]
    );

    return {
        ...controller,
        field: {
            ...controller.field,
            onChange: changeHandler,
        },
    };
};
