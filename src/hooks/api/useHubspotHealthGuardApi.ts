import { useCallback } from 'react';
import { useMutation, useQuery } from '@tanstack/react-query';
import { HTTPError } from 'ky';
import type { QueryFunction, QueryKey } from '@tanstack/react-query';
import type {
    CreateHubspotContactRequest,
    CreateHubspotContactResponse,
    GetDealsByAffiliatesResponse,
    UpdateHubspotContactRequest,
} from '@/api/hubspotHealthGuard';
import { useMe } from '@/hooks/useMe';
import { getDealsByAffiliates, createHubspotContact, updateHubspotContact } from '@/api/hubspotHealthGuard';
import { ONE_MINUTE } from '@/constants';

type QueryFnGetDealsByAffiliates = QueryFunction<GetDealsByAffiliatesResponse, QueryKey>;

export const useGetDealsByAffiliates = () => {
    const user = useMe();

    const response = useQuery<GetDealsByAffiliatesResponse, HTTPError>({
        queryKey: ['deals-by-affiliates', user.id],
        queryFn: useCallback<QueryFnGetDealsByAffiliates>(
            ({ signal }) => {
                return getDealsByAffiliates({ affiliate: user.id }, { signal });
            },
            [user.id]
        ),
        staleTime: 20 * ONE_MINUTE,
    });

    return response;
};

export const useCreateHubspotContact = () => {
    const mutation = useMutation<CreateHubspotContactResponse, HTTPError, CreateHubspotContactRequest>({
        mutationFn: createHubspotContact,
    });

    return mutation;
};

export const useUpdateHubspotContact = () => {
    const mutation = useMutation<unknown, HTTPError, UpdateHubspotContactRequest>({
        mutationFn: updateHubspotContact,
    });

    return mutation;
};
