import { useCallback } from 'react';
import { useMutation, useQuery } from '@tanstack/react-query';
import type { QueryFunction, QueryKey } from '@tanstack/react-query';
import type {
    GetUserTeamResponse,
    GetAffiliateByIdResponse,
    Affiliate,
    VerifyUserResponse,
    GetUserByEmailResponse,
} from '@/api/firebase';
import { useMe } from '@/hooks/useMe';
import { getAffiliateById, getUserAffiliatesTeam, verifyUser, getUserByEmail } from '@/api/firebase';
import { ONE_MINUTE } from '@/constants';

type QueryFnGetFirebaseUserAffiliatesTeam = QueryFunction<Awaited<GetUserTeamResponse>, QueryKey>;
type QueryFnGetAffiliateById = QueryFunction<GetAffiliateByIdResponse, QueryKey>;
type QueryFnGetUserByEmail = QueryFunction<Awaited<GetUserByEmailResponse>, QueryKey>;

export const useGetFirebaseUserAffiliatesTeam = () => {
    const user = useMe();

    const response = useQuery<Awaited<GetUserTeamResponse>, Error>({
        queryKey: ['firebase/user-affiliates-team', user.id],
        queryFn: useCallback<QueryFnGetFirebaseUserAffiliatesTeam>(() => {
            return getUserAffiliatesTeam(user.id);
        }, [user.id]),
        staleTime: 20 * ONE_MINUTE,
    });

    return response;
};

export const useGetAffiliateById = (affiliateId: string) => {
    const response = useQuery<Affiliate | null, Error, string>({
        queryKey: ['firenase/affiliate-by-id', affiliateId],
        queryFn: useCallback<QueryFnGetAffiliateById>(async () => {
            return await getAffiliateById(affiliateId);
        }, [affiliateId]),
        enabled: !!affiliateId,
    });

    return response;
};

export const useGetUserByEmail = (email: string, enabled = true) => {
    const response = useQuery<GetUserByEmailResponse, Error>({
        queryKey: ['firebase/user-by-email', email],
        queryFn: useCallback<QueryFnGetUserByEmail>(() => {
            return getUserByEmail(email);
        }, [email]),
        enabled: !email || enabled,
    });

    return response;
};

export const useVerifyFirebaseUser = () => {
    const mutation = useMutation<VerifyUserResponse, Error, string>({
        mutationFn: verifyUser,
    });

    return mutation;
};
