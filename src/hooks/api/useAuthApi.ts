import { useMutation } from '@tanstack/react-query';
import type { AuthLoginRequest, AuthLoginResponse, AuthRegisterRequest, AuthRegisterResponse } from '@/api/auth';
import { logIn, logOut, register } from '@/api/auth';

export const useLogin = () => {
    const mutation = useMutation<AuthLoginResponse, Error, AuthLoginRequest>({
        mutationFn: logIn,
    });

    return mutation;
};

export const useRegister = () => {
    const mutation = useMutation<AuthRegisterResponse, Error, AuthRegisterRequest>({
        mutationFn: register,
    });

    return mutation;
};

export const useLogOut = () => {
    const mutation = useMutation<void, Error, void>({
        mutationFn: logOut,
    });

    return mutation;
};
