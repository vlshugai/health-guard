import { useState } from 'react';

/**
 * Hook provides a copy method to save a string in the and the copied value.
 * @param text The value to copy.
 * @returns The array containing the
 * - copied text
 * - the function to copy in clipboard.
 */

type CopiedValue = string | null;
type CopyFn = (text: string) => void;

export function useCopyToClipboard(): [CopiedValue, CopyFn] {
    const [copiedText, setCopiedText] = useState<CopiedValue>(null);

    const copy: CopyFn = async (text) => {
        if (!navigator?.clipboard) {
            document.execCommand('copy', true, text);
            setCopiedText(text);
            return;
        }

        await navigator.clipboard.writeText(text);
        setCopiedText(text);
    };

    return [copiedText, copy];
}
