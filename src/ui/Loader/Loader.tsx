import { memo } from 'react';
import clsx from 'clsx';
import type { LoaderProps } from './types';
import s from './Loader.module.css';

const Loader: React.FC<LoaderProps> = ({ className }) => {
    return (
        <div className={clsx(s.wrap, className)}>
            <img className={s.loader} src="/images/icon-loader.svg" alt="Loader" />
        </div>
    );
};

export default memo(Loader);
