import type { WithClassName } from '@/types';

export type LoaderProps = WithClassName<unknown>;
