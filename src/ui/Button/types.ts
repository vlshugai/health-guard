import { FCProps } from '@/types';
import { SlotProps } from '@radix-ui/react-slot';
import { BUTTON_VARIANTS } from './constants';

export type ButtonVariant = (typeof BUTTON_VARIANTS)[number];

export type ButtonProps = FCProps<{
    variant?: ButtonVariant;
    asChild?: boolean;
}> &
    SlotProps &
    React.ComponentProps<'button'>;
