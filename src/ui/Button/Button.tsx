import { forwardRef, memo } from 'react';
import { ButtonProps } from './types';
import { Slot } from '@radix-ui/react-slot';
import s from './Button.module.css';
import clsx from 'clsx';

const Button = forwardRef<HTMLButtonElement, ButtonProps>(
    ({ asChild, children, className, variant = 'primary', ...rest }, ref) => {
        const Component = asChild ? Slot : 'button';

        return (
            <Component className={clsx(s.wrap, s[variant], 'focus-primary', className)} {...rest} ref={ref}>
                {children}
            </Component>
        );
    }
);
Button.displayName = 'Button';

export default memo(Button);
