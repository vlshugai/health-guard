import type { FCProps } from '@/types';

export type ErrorMessageProps = FCProps<unknown>;
