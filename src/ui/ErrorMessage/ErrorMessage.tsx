import { memo } from 'react';
import clsx from 'clsx';
import type { ErrorMessageProps } from './types';
import Typography from '@/ui/Typography';
import s from './ErrorMessage.module.css';

const ErrorMessage: React.FC<ErrorMessageProps> = ({ children, className }) => {
    return (
        <span className={clsx(s.wrap, className)}>
            <Typography variant="caption" asChild>
                <span>{children}</span>
            </Typography>
        </span>
    );
};

export default memo(ErrorMessage);
