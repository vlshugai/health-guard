import type { FCProps } from '@/types';

export type LabelProps = FCProps<unknown> & React.LabelHTMLAttributes<HTMLLabelElement>;
