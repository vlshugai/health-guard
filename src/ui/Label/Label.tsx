import { useId, memo } from 'react';
import clsx from 'clsx';
import Typography from '@/ui/Typography';
import type { LabelProps } from './types';
import s from './Label.module.css';

const Label: React.FC<LabelProps> = ({ htmlFor, children, className, ...rest }) => {
    const generatedInputId = useId();

    return (
        <label className={clsx(s.wrap, className)} htmlFor={htmlFor ? htmlFor : generatedInputId} {...rest}>
            <Typography variant="paragraph-sm" className={s.text} asChild>
                <span>{children}</span>
            </Typography>
        </label>
    );
};

export default memo(Label);
