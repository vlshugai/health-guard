import { useRef, useEffect, useCallback } from 'react';
import { useToggle } from '@react-hookz/web';
import { useCopyToClipboard } from '@/hooks/useCopyToClipboard';
import { TIMEOUT_WAIT_COPY } from '../constants';

export const useInputCopyValue = (value?: string): [boolean, () => void] => {
    const [isCopying, toggleIsCopying] = useToggle();
    const [, copy] = useCopyToClipboard();
    const refTimer = useRef<number | null>(null);

    const stopTimer = () => {
        if (refTimer.current === null) {
            return;
        }
        window.clearTimeout(refTimer.current);
        refTimer.current = null;
    };

    const onCopyValue = useCallback(() => {
        if (!value) {
            return;
        }
        stopTimer();
        copy(value);
        toggleIsCopying(true);

        refTimer.current = window.setTimeout(() => {
            toggleIsCopying();
            stopTimer();
        }, TIMEOUT_WAIT_COPY);
    }, [toggleIsCopying, copy, value]);

    useEffect(() => {
        return () => {
            stopTimer();
        };
    }, []);

    return [isCopying, onCopyValue];
};
