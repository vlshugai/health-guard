import type { FCProps, ObjValues } from '@/types';
import type { IconId } from '@/ui/Icon';
import { INPUT_VARIANTS } from './constants';

export type InputVariantType = ObjValues<typeof INPUT_VARIANTS>;

export type InputProps = FCProps<{
    /**
      Label of input.
    */
    label?: string;
    /**
        The variant to use.
    */
    variant?: InputVariantType;
    /**
        The value of the component.
    */
    value?: string;
    /**
        The title of the icon component.
    */
    iconTitle?: string;
    /**
        The iconId of the component.
    */
    iconId?: IconId;
    /**
        Icon child component.
     */
    iconChild?: React.ReactNode;
    /**
        If true, error state will be shown.
    */
    errorMessage?: string;
}> &
    React.InputHTMLAttributes<HTMLInputElement>;
