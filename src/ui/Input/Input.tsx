import { forwardRef, ForwardRefRenderFunction, useMemo, useId, useCallback, memo } from 'react';
import { useToggle } from '@react-hookz/web';
import clsx from 'clsx';
import type { IconId } from '@/ui/Icon';
import type { InputProps } from './types';
import { useInputCopyValue } from './hooks/useInputCopyValue';
import { INPUT_VARIANTS } from './constants';
import ErrorMessage from '@/ui/ErrorMessage';
import Label from '@/ui/Label';
import Icon from '@/ui/Icon';
import s from './Input.module.css';

const Input: ForwardRefRenderFunction<HTMLInputElement, InputProps> = (
    {
        id,
        className,
        label,
        variant,
        name,
        type,
        iconId,
        iconTitle,
        iconChild,
        errorMessage,
        value,
        onChange,
        disabled,
        ...rest
    },
    ref
) => {
    let addonSearchJSX: React.ReactNode = null;
    const generatedInputId = useId();
    const [isPasswordVisible, toggleIsPasswordVisible] = useToggle();
    const [isCopy, onCopyValue] = useInputCopyValue(value);

    const memoizedInputType = useMemo(() => {
        if (type === 'password') {
            return isPasswordVisible ? 'text' : 'password';
        }

        return type;
    }, [isPasswordVisible, type]);

    const iconClickHandler = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.stopPropagation();
            if (variant === INPUT_VARIANTS.copy) {
                onCopyValue();
            } else {
                toggleIsPasswordVisible();
            }
        },
        [toggleIsPasswordVisible, onCopyValue, variant]
    );

    const addonJSX = useMemo(() => {
        if (iconChild) {
            return <div className={s['addon-wrap']}>{iconChild}</div>;
        }

        if (iconId || type === 'password' || variant === INPUT_VARIANTS.copy) {
            let icon = iconId as IconId;
            const iconTitleProp = iconTitle ? { title: iconTitle, 'aria-label': iconTitle } : {};

            if (type === 'password') {
                icon = isPasswordVisible ? 'icon-eye-opened_18' : 'icon-eye-crossed_18';
                const title = isPasswordVisible ? 'password shown' : 'password hidden';
                iconTitleProp.title = title;
                iconTitleProp['aria-label'] = title;
            }

            if (variant === INPUT_VARIANTS.copy) {
                icon = isCopy ? 'icon-check_18' : 'icon-copy-alt_18';
                const title = isCopy ? 'copied' : 'copy';
                iconTitleProp.title = title;
                iconTitleProp['aria-label'] = title;
            }

            return (
                <div className={s['addon-wrap']}>
                    <button
                        type="button"
                        className={clsx(s['icon-btn'], 'focus-primary')}
                        onClick={iconClickHandler}
                        {...iconTitleProp}
                        disabled={disabled}
                    >
                        <Icon id={icon as IconId} className={s['addon-icon']} />
                    </button>
                </div>
            );
        }
    }, [iconChild, iconId, type, variant, iconTitle, iconClickHandler, disabled, isPasswordVisible, isCopy]);

    if (type === 'search') {
        addonSearchJSX = (
            <label className={clsx(s['addon-wrap'], s['addon-wrap-start'])} htmlFor={id ?? generatedInputId}>
                <Icon id="icon-search_18" className={s['addon-icon']} />
            </label>
        );
    }

    const changeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (typeof onChange === 'function') {
            onChange(e);
        }
    };

    return (
        <div
            className={clsx(s.wrap, className, {
                [s.error]: !!errorMessage,
                [s.copied]: isCopy,
                [s.disabled]: disabled,
            })}
        >
            {label ? (
                <Label className={s.label} htmlFor={id ? id : generatedInputId}>
                    {label}
                </Label>
            ) : null}
            <div className={clsx(s.inner, addonJSX && s['with-addon'])}>
                <input
                    id={id ?? generatedInputId}
                    ref={ref}
                    name={name}
                    type={memoizedInputType}
                    value={value}
                    onChange={changeHandler}
                    disabled={disabled}
                    {...rest}
                />
                {addonSearchJSX}
                {addonJSX}
            </div>
            {!!errorMessage && <ErrorMessage className={s['error-wrap']}>{errorMessage}</ErrorMessage>}
        </div>
    );
};

export default memo(forwardRef(Input));
