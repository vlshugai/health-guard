import { memo, forwardRef } from 'react';
import type { IconProps } from './types';

const Icon = forwardRef<SVGSVGElement, IconProps>(({ id, className }, ref) => {
    return (
        <svg className={className} ref={ref}>
            <use href={`/images/sprite.svg#${id}`} />
        </svg>
    );
});

Icon.displayName = 'Icon';

export default memo(Icon);
