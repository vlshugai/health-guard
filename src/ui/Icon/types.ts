import type { WithClassName } from '@/types';
import { ICON_IDS } from './constants';

export type IconId = (typeof ICON_IDS)[number];

export type IconProps = WithClassName<{
    /**
        Id of icon.
    */
    id: IconId;
}>;
