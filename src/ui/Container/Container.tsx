import { memo } from 'react';
import clsx from 'clsx';
import type { ContainerProps } from './types';
import s from './Container.module.css';

const Container: React.FC<ContainerProps> = ({ children, size, withPaddingsX = true, className }) => {
    return (
        <div
            className={clsx(s.wrap, withPaddingsX && s['paddings-x'], size && s[size], className)}
            style={{ '--container-padding-multiplier': withPaddingsX ? 2 : 0 } as React.CSSProperties}
        >
            {children}
        </div>
    );
};

export default memo(Container);
