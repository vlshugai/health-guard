import type { FCProps } from '@/types';

export type ContainerSizeType = 'sm';

export type ContainerProps = FCProps<{
    /**
        The size of the component.
    */
    size?: ContainerSizeType;
    /**
        If true, the paddings by x will not show.
    */
    withPaddingsX?: boolean;
}>;
