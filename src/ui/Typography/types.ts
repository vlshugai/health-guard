import type { SlotProps } from '@radix-ui/react-slot';
import type { WithClassName } from '@/types';
import { TYPOGRAPHY_VARIANTS } from './constants';

export type TypographyVariant = (typeof TYPOGRAPHY_VARIANTS)[number];

export type TypographyProps = WithClassName<{
    /**
        Change the default rendered element for the one passed as a child, merging their props and behavior.
    */
    asChild?: boolean;
    /**
        The variant to use.
    */
    variant?: TypographyVariant;
}> &
    SlotProps;
