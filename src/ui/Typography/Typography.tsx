import { memo } from 'react';
import clsx from 'clsx';
import { Slot } from '@radix-ui/react-slot';
import type { TypographyProps } from './types';
import s from './Typography.module.css';

const Typography: React.FC<TypographyProps> = ({ asChild, variant = 'paragraph-base', className, ...rest }) => {
    const Component = asChild ? Slot : 'div';

    return <Component {...rest} className={clsx(s.wrap, s[variant], className)} />;
};

export default memo(Typography);
