export const TYPOGRAPHY_VARIANTS = [
    'title-h1',
    'title-h3',
    'title-h4',
    'link',
    'paragraph-sm',
    'paragraph-base',
    'paragraph-lg',
    'caption',
] as const;
