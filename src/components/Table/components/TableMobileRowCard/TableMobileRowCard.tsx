import { memo } from 'react';
import clsx from 'clsx';
import type { TableMobileRowCardProps } from './types';
import s from './TableMobileRowCard.module.css';

const TableMobileRowCard: React.FC<TableMobileRowCardProps> = ({ className, children }) => {
    return <div className={clsx(s.wrap, className)}>{children}</div>;
};

export default memo(TableMobileRowCard);
