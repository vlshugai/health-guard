import type { WithClassName } from '@/types';

export type TablePaginationProps = WithClassName<{
    page: number;
    pageSize: number;
    total: number;
    onPageChange: (page: number) => void;
}>;
