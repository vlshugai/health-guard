import { memo, useCallback } from 'react';
import clsx from 'clsx';
import type { TablePaginationProps } from './types';
import { useMediaQuery } from '@/hooks/useMediaQuery';
import Typography from '@/ui/Typography';
import Icon from '@/ui/Icon';
import TableMobilePagination from './components/TableMobilePagination';
import s from './TablePagination.module.css';

const Pagination: React.FC<TablePaginationProps> = ({ className, page, pageSize, total, onPageChange }) => {
    const isMobile = useMediaQuery('(max-width: 991px)');
    const lastPage = Math.max(0, Math.ceil(total / pageSize));
    const isPrevPageDisabled = page === 1;
    const isNextPageDisabled = page === lastPage;

    const prevPageClickHandler = useCallback(() => {
        onPageChange(page - 1);
    }, [onPageChange, page]);

    const nextPageClickHandler = useCallback(() => {
        onPageChange(page + 1);
    }, [onPageChange, page]);

    return (
        <div className={clsx(s.wrap, className)}>
            {isMobile ? (
                <TableMobilePagination page={page} pageSize={pageSize} total={total} onPageChange={onPageChange} />
            ) : (
                <div className={s.navigation}>
                    <button
                        className={clsx(s.button, s.prev, 'focus-primary')}
                        disabled={isPrevPageDisabled}
                        title="Previous page"
                        onClick={prevPageClickHandler}
                    >
                        <Icon className={s.icon} id="icon-caret_24" />
                        <span className={clsx(s.label, 'visually-hidden')} aria-label="Previous page">
                            Previous
                        </span>
                    </button>
                    <div className={s.page} title="Current page">
                        <Typography className={s.text} variant="paragraph-sm" asChild>
                            <span>{page}</span>
                        </Typography>
                    </div>
                    <button
                        className={clsx(s.button, s.next, 'focus-primary')}
                        disabled={isNextPageDisabled}
                        title="Next page"
                        onClick={nextPageClickHandler}
                    >
                        <span className={clsx(s.label, 'visually-hidden')} aria-label="Next page">
                            Next
                        </span>
                        <Icon className={s.icon} id="icon-caret_24" />
                    </button>
                </div>
            )}
        </div>
    );
};

export default memo(Pagination);
