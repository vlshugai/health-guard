import { memo, useCallback } from 'react';
import ReactPaginate from 'react-paginate';
import clsx from 'clsx';
import type { MobilePaginationProps } from './types';
import Icon from '@/ui/Icon';
import s from './TableMobilePagination.module.css';

const MobilePagination: React.FC<MobilePaginationProps> = ({ pageSize, total, onPageChange }) => {
    const lastPage = Math.max(0, Math.ceil(total / pageSize));

    const pageChangeHandler = useCallback(
        (item: { selected: number }) => {
            onPageChange(item.selected + 1);
        },
        [onPageChange]
    );

    return (
        <ReactPaginate
            containerClassName={s.wrap}
            previousClassName={clsx(s.nav, s.prev, 'focus-primary')}
            nextClassName={clsx(s.nav, s.next, 'focus-primary')}
            pageClassName={clsx(s.page, 'focus-primary')}
            breakClassName={s.break}
            breakLabel="..."
            nextLabel={<Icon className={s.icon} id="icon-caret_24" />}
            onPageChange={pageChangeHandler}
            pageRangeDisplayed={1}
            marginPagesDisplayed={2}
            pageCount={lastPage}
            previousLabel={<Icon className={s.icon} id="icon-caret_24" />}
            renderOnZeroPageCount={null}
        />
    );
};

export default memo(MobilePagination);
