import { memo, useMemo } from 'react';
import { flexRender } from '@tanstack/react-table';
import clsx from 'clsx';
import type { TableProps } from './types';
import { useMediaQuery } from '@/hooks/useMediaQuery';
import { TABLE_NO_ROWS_MESSAGE } from './constant';
import Typography from '@/ui/Typography';
import Icon from '@/ui/Icon';
import TableMobileRowCard from './components/TableMobileRowCard';
import s from './Table.module.css';

const Table: React.FC<TableProps> = ({ className, rows, headerGroups, noRowsMessage = TABLE_NO_ROWS_MESSAGE }) => {
    const isMobile = useMediaQuery('(max-width: 991px)');

    const totalColumns = useMemo(() => {
        return headerGroups.reduce((acc, headerGroup) => {
            return acc + headerGroup.headers.length;
        }, 0);
    }, [headerGroups]);

    return (
        <div className={clsx(s.wrap, className)}>
            {isMobile ? (
                <div className={s.content}>
                    {rows.length ? (
                        rows.map((row) => {
                            return (
                                <TableMobileRowCard className={s.card} key={row.id}>
                                    <ul className={s.list}>
                                        {row.getVisibleCells().map((cell) => {
                                            return (
                                                <li key={cell.id} className={clsx(s.row, 'truncate')}>
                                                    <Typography
                                                        className={clsx(s.title, 'truncate')}
                                                        variant="paragraph-sm"
                                                        asChild
                                                    >
                                                        <span>{cell.column.columnDef.header?.toString()}</span>
                                                    </Typography>
                                                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </TableMobileRowCard>
                            );
                        })
                    ) : (
                        <TableMobileRowCard className={s.empty}>
                            <Typography className={s.text} variant="paragraph-sm" asChild>
                                <p>{noRowsMessage}</p>
                            </Typography>
                        </TableMobileRowCard>
                    )}
                </div>
            ) : (
                <table className={s.table}>
                    <thead className={s['table-header']}>
                        {headerGroups.map((headerGroup) => {
                            return (
                                <tr key={headerGroup.id}>
                                    {headerGroup.headers.map((header) => {
                                        const headerValue = header.column.columnDef.header;
                                        const canSort = !!rows.length && header.column.getCanSort();

                                        return (
                                            <th
                                                className={s.th}
                                                key={header.id}
                                                colSpan={header.colSpan}
                                                style={{
                                                    minWidth: header.column.columnDef.minSize,
                                                    maxWidth: header.column.columnDef.maxSize,
                                                }}
                                            >
                                                <Typography className={s['header-text']} variant="paragraph-sm" asChild>
                                                    <span>{flexRender(headerValue, header.getContext())}</span>
                                                </Typography>
                                                {canSort && <Icon id="icon-sort_18" className={s.icon} />}
                                            </th>
                                        );
                                    })}
                                </tr>
                            );
                        })}
                    </thead>
                    <tbody className={s.tbody}>
                        {rows.length ? (
                            rows.map((row) => {
                                return (
                                    <tr className={s.tr} key={row.id}>
                                        {row.getVisibleCells().map((cell) => {
                                            const tdStyles: React.CSSProperties = {
                                                minWidth: cell.column.columnDef.minSize,
                                                maxWidth: cell.column.columnDef.maxSize,
                                            };

                                            return (
                                                <td className="truncate" key={cell.id} style={tdStyles}>
                                                    {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                                </td>
                                            );
                                        })}
                                    </tr>
                                );
                            })
                        ) : (
                            <tr className={s['empty-row']}>
                                <td className={s['empty-cell']} colSpan={totalColumns}>
                                    <Typography variant="paragraph-sm" asChild>
                                        <span>{noRowsMessage}</span>
                                    </Typography>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
            )}
        </div>
    );
};

export default memo(Table);
