import type { WithClassName } from '@/types';
import type { HeaderGroup, Row } from '@tanstack/react-table';

export type TableProps = WithClassName<{
    headerGroups: HeaderGroup<unknown>[];
    rows: Row<unknown>[];
    noRowsMessage?: string;
}>;
