import { forwardRef, memo, useId, useMemo, useState } from 'react';
import ReactSelect from 'react-select';
import clsx from 'clsx';
import type { GroupBase } from 'react-select';
import type { SelectComponents } from 'react-select/dist/declarations/src/components';
import type { SelectOptionType } from '@/types';
import type { SelectProps, SelectType } from './types';
import { CUSTOM_SELECT_STYLES } from './constants';
import ErrorMessage from '@/ui/ErrorMessage';
import Label from '@/ui/Label';
import DropdownIndicator from './components/DropdownIndicator';
import NoOptionsMessage from './components/NoOptionMessage';
import SingleValue from './components/SingleValue';
import Option from './components/Option';
import s from './Select.module.css';

const Select = forwardRef<SelectType, SelectProps>(
    ({ className, value, options, label, placeholder, disabled, errorMessage, components, onChange, ...rest }, ref) => {
        const id = useId();

        const [searchValue, setSearchValue] = useState('');

        const filteredOptions = useMemo(() => {
            if (!options?.length) {
                return [];
            }

            if (!rest?.isSearchable) {
                return options;
            }

            return options.filter((option) => {
                return option.label.toLowerCase().includes(searchValue.toLowerCase());
            });
        }, [options, rest?.isSearchable, searchValue]);

        const memoizedComponents = useMemo<
            Partial<SelectComponents<SelectOptionType, false, GroupBase<SelectOptionType>>>
        >(() => {
            return {
                Option,
                NoOptionsMessage,
                DropdownIndicator,
                SingleValue,
                ...components,
            };
        }, [components]);

        return (
            <div
                className={clsx(s.wrap, className, {
                    [s['error-wrap']]: errorMessage,
                    [s.disabled]: disabled,
                })}
            >
                {label ? <Label className={s.label}>{label}</Label> : null}
                <ReactSelect
                    ref={ref}
                    id={id}
                    className={s['select']}
                    classNamePrefix="select"
                    menuPlacement="auto"
                    styles={CUSTOM_SELECT_STYLES}
                    value={value}
                    options={filteredOptions}
                    placeholder={placeholder}
                    isDisabled={disabled}
                    components={memoizedComponents}
                    isSearchable={false}
                    filterOption={null}
                    isClearable={false}
                    hideSelectedOptions={false}
                    closeMenuOnScroll={false}
                    onChange={onChange}
                    onInputChange={rest?.isSearchable ? setSearchValue : undefined}
                    {...rest}
                />
                {errorMessage ? <ErrorMessage className={s.error}>{errorMessage}</ErrorMessage> : null}
            </div>
        );
    }
);

Select.displayName = 'Select';

export default memo(Select);
