import SelectClass from 'react-select/dist/declarations/src/Select';
import type { RefAttributes } from 'react';
import type { ActionMeta, GroupBase, MultiValue, SingleValue } from 'react-select';
import type { SelectComponents } from 'react-select/dist/declarations/src/components';
import type { StateManagerProps } from 'react-select/dist/declarations/src/useStateManager';
import type { SelectOptionType, WithClassName } from '@/types';

export type SelectType<TOption = SelectOptionType> = SelectClass<TOption, false, GroupBase<TOption>>;

export type SelectProps<TOption = SelectOptionType> = WithClassName<{
    /**
        Selected value.
     */
    value?: SingleValue<TOption>;
    /**
        Options list to select from.
     */
    options: TOption[];
    /**
        Label of field.
    */
    label?: string;
    /**
        Description of field
    */
    description?: string;
    /**
        Caption of field
    */
    caption?: string;
    /**
        Callback function for changing value of the select.
    */
    onChange: (value: SingleValue<TOption> | MultiValue<TOption>, action: ActionMeta<TOption>) => void;
    /**
        Placeholder for the select.
    */
    placeholder?: string;
    /**
        If true, the select is disabled.
    */
    disabled?: boolean;
    /**
        An error message of the select.
    */
    errorMessage?: string;
    /**
        The tooltip text of the item.
    */
    tooltip?: React.ReactNode;
    /**
        Custom components for select.
    */
    components?: Partial<SelectComponents<TOption, false, GroupBase<TOption>>>;
}> &
    StateManagerProps<TOption, false, GroupBase<TOption>> &
    RefAttributes<SelectType<TOption>>;
