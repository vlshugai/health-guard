import { memo } from 'react';
import { components } from 'react-select';
import type { NoOptionMessageProps } from './types';
import s from './NoOptionMessage.module.css';

const NoOptionMessage: React.FC<NoOptionMessageProps> = ({
    description = 'We can`t find any options by this value.',
    ...props
}) => {
    return (
        <components.NoOptionsMessage {...props}>
            <div className={s.content}>{description}</div>
        </components.NoOptionsMessage>
    );
};

export default memo(NoOptionMessage);
