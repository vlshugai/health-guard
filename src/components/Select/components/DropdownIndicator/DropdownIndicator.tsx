import { memo } from 'react';
import { components } from 'react-select';
import type { DropdownIndicatorProps } from './types';
import Icon from '@/ui/Icon';
import s from './DropdownIndicator.module.css';

const DropdownIndicator: React.FC<DropdownIndicatorProps> = (props) => {
    return (
        <components.DropdownIndicator {...props}>
            <Icon className={s.icon} id="icon-caret_24" />
        </components.DropdownIndicator>
    );
};

export default memo(DropdownIndicator);
