import { memo } from 'react';
import { components } from 'react-select';
import clsx from 'clsx';
import type { OptionProps } from './types';
import Typography from '@/ui/Typography';
import Icon from '@/ui/Icon';
import s from './Option.module.css';

const Option: React.FC<OptionProps> = (props) => {
    return (
        <components.Option {...props}>
            <Typography className={clsx(s['option-text'], 'truncate')} title={props.children as string} asChild>
                <span>{props.children}</span>
            </Typography>
            {props?.isSelected ? <Icon className={s['option-icon']} id="icon-check_18" /> : null}
        </components.Option>
    );
};

export default memo(Option);
