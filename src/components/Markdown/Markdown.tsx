import { memo } from 'react';
import ReactMarkdown from 'react-markdown';
import clsx from 'clsx';
import type { MarkdownProps } from './types';
import Typography from '@/ui/Typography';
import s from './Markdown.module.css';

const Markdown: React.FC<MarkdownProps> = ({ children, className }) => {
    return (
        <ReactMarkdown
            className={clsx(s.wrap, className)}
            components={{
                h1({ node: _, ...props }) {
                    return (
                        <Typography variant="title-h1" asChild>
                            <h2 {...props} />
                        </Typography>
                    );
                },
                h2({ node: _, ...props }) {
                    return (
                        <Typography variant="title-h4" asChild>
                            <h2 {...props} />
                        </Typography>
                    );
                },
                h3({ node: _, ...props }) {
                    return (
                        <Typography className={s.h3} variant="paragraph-lg" asChild>
                            <h3 {...props} />
                        </Typography>
                    );
                },
                h4({ node: _, ...props }) {
                    return (
                        <Typography variant="paragraph-lg" asChild>
                            <h4 {...props} />
                        </Typography>
                    );
                },

                ul({ node: _, ...props }) {
                    return <ul {...props} className={clsx(s.list, s['revert-paddings'], s['revert-margins'])} />;
                },
                ol({ node: _, ...props }) {
                    return <ol {...props} className={clsx(s.list, s['revert-paddings'], s['revert-margins'])} />;
                },
                p({ node: _, ...props }) {
                    return (
                        <Typography className={s.paragraph} asChild variant="paragraph-sm">
                            <p {...props} />
                        </Typography>
                    );
                },
                a({ node: _, ...props }) {
                    return <a {...props} className={clsx(s.link, 'focus-primary')} target="_blank" />;
                },
                code({ node: _, ...props }) {
                    return <code {...props} className={s.code} />;
                },
                blockquote({ node: _, ...props }) {
                    return <blockquote {...props} className={s.blockquote} />;
                },
                hr({ node: _, ...props }) {
                    return <hr {...props} className={clsx(s.hr, s['revert-margins'])} />;
                },
            }}
        >
            {children}
        </ReactMarkdown>
    );
};

export default memo(Markdown);
