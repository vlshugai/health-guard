import type { WithClassName } from '@/types';

export type MarkdownProps = WithClassName<{
    /**
        Markdown string of the component.
    */
    children: string;
}>;
