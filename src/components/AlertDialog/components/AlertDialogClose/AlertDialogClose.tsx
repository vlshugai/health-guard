import { memo } from 'react';
import { Cancel as AlertDialogCancel } from '@radix-ui/react-alert-dialog';
import clsx from 'clsx';
import type { AlertDialogCloseProps } from './types';
import s from './AlertDialogClose.module.css';

const AlertDialogClose: React.FC<AlertDialogCloseProps> = ({ className, children }) => {
    return (
        <AlertDialogCancel className={clsx(s.wrap, 'focus-primary', className)} asChild>
            <div>
                {children}
                <span className="visually-hidden" aria-label="Close">
                    Close
                </span>
            </div>
        </AlertDialogCancel>
    );
};

export default memo(AlertDialogClose);
