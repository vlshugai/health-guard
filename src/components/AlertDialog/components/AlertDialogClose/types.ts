import type { FCProps } from '@/types';

export type AlertDialogCloseProps = FCProps<unknown>;
