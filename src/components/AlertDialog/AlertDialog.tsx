import { memo } from 'react';
import { useMountEffect, useToggle } from '@react-hookz/web';
import {
    Root as AlertDialogRoot,
    Portal as AlertDialogPortal,
    Overlay as AlertDialogOverlay,
    Content as AlertDialogContent,
} from '@radix-ui/react-alert-dialog';
import clsx from 'clsx';
import type { AlertDialogProps } from './types';
import s from './AlertDialog.module.css';

const AlertDialog: React.FC<AlertDialogProps> = ({ children }) => {
    const [isOpened, setIsOpened] = useToggle(true);

    useMountEffect(() => {
        setIsOpened(true);
    });

    return (
        <AlertDialogRoot open={isOpened}>
            <AlertDialogPortal>
                <AlertDialogOverlay
                    className={clsx(s.overlay, {
                        'animate-fade-in': isOpened,
                    })}
                />
                <AlertDialogContent
                    className={clsx(s.wrap, {
                        'animate-dialog-fade-in': isOpened,
                    })}
                >
                    <div className={s.content}>{children}</div>
                </AlertDialogContent>
            </AlertDialogPortal>
        </AlertDialogRoot>
    );
};

export default memo(AlertDialog);
