import { WithChildren } from '@/types';

export type AlertDialogProps = WithChildren<unknown>;
