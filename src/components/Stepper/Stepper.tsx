import { memo, useMemo } from 'react';
import clsx from 'clsx';
import type { StepperProps } from './types';
import { intlPluralRulesSimplify } from '@/utils/intlPluralRulesSimplify';
import Typography from '@/ui/Typography';
import s from './Stepper.module.css';

const Stepper: React.FC<StepperProps> = ({ currentStep, stepsTitles, className }) => {
    const totalSteps = stepsTitles.length;
    const currentStepTitle = stepsTitles[currentStep - 1];
    const stepsLeft = totalSteps - currentStep;

    const stepsLabel = useMemo(() => {
        return intlPluralRulesSimplify(stepsLeft, 'step', 'steps');
    }, [stepsLeft]);

    return (
        <div className={clsx(s.wrap, className)}>
            <ul className={s['indicators-wrap']} aria-label="Sign up wizard steps indicators">
                {stepsTitles.map((title, index) => {
                    const isStepCompleted = index < currentStep - 1;
                    const isStepActive = index === currentStep - 1;

                    let stepLabel = 'incomplete';

                    if (isStepCompleted) {
                        stepLabel = 'completed';
                    } else if (isStepActive) {
                        stepLabel = 'active';
                    }

                    return (
                        <li
                            key={title}
                            className={s.indicator}
                            data-state={isStepCompleted || isStepActive ? 'active' : 'default'}
                        >
                            <span className="visually-hidden">
                                Step {index + 1} - {title} - {stepLabel}
                            </span>
                        </li>
                    );
                })}
            </ul>
            <div className={s.inner}>
                <Typography
                    className={clsx(s['current-step-label'], 'truncate')}
                    variant="paragraph-lg"
                    title={`Step ${currentStep} - ${currentStepTitle}`}
                    asChild
                >
                    <h3>
                        <strong>Step&nbsp;{currentStep}&nbsp;-</strong>&nbsp;{currentStepTitle}
                    </h3>
                </Typography>
                <Typography
                    className={clsx(s['steps-left-label'], 'truncate')}
                    variant="paragraph-base"
                    title={`${stepsLeft} ${stepsLabel} to complete`}
                    asChild
                >
                    <span>
                        {stepsLeft} {stepsLabel} to complete
                    </span>
                </Typography>
            </div>
        </div>
    );
};

export default memo(Stepper);
