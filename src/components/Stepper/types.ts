import type { WithClassName } from '@/types';

export type StepperProps = WithClassName<{
    stepsTitles: string[];
    currentStep: number;
}>;
