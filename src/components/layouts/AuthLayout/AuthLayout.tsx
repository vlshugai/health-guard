import { memo } from 'react';
import { Link } from '@tanstack/react-router';
import clsx from 'clsx';
import type { WithChildren } from '@/types';
import Container from '@/ui/Container';
import s from './AuthLayout.module.css';

const AuthLayout: React.FC<WithChildren<unknown>> = ({ children }) => {
    return (
        <div className={clsx(s.wrap, 'full-height')}>
            <div className={s.inner}>
                <Container className={s.container}>
                    <header className={s.header}>
                        <div className={clsx(s['logo-wrap'], 'focus-within-primary')}>
                            <Link className={s.link} to="/" search={{}}>
                                <img className={s.logo} src="/images/logo-full.webp" alt="Health Guard logo" />
                            </Link>
                        </div>
                    </header>
                    <main className={s.content}>{children}</main>
                </Container>
            </div>
        </div>
    );
};

export default memo(AuthLayout);
