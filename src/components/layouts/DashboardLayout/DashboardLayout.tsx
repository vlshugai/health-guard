import React, { memo, useCallback, useEffect } from 'react';
import { Link, getRouteApi } from '@tanstack/react-router';
import { useMediaQuery, useToggle } from '@react-hookz/web';
import {
    Root as NavigationMenuRoot,
    List as NavigationMenuList,
    Item as NavigationMenuItem,
    Link as NavigationMenuLink,
} from '@radix-ui/react-navigation-menu';
import Hamburger from 'hamburger-react';
import clsx from 'clsx';
import type { WithChildren } from '@/types';
import useDocumentLockScrollY from '@/hooks/useDocumentLockScrollY';
import { ALL_VALUE } from '@/modules/Dashboard/constants';
import Typography from '@/ui/Typography';
import Container from '@/ui/Container';
import Button from '@/ui/Button';
import NavLinkItem from './components/NavLinkItem';
import s from './DashboardLayout.module.css';

const routeApi = getRouteApi('/_dashboard-layout');

const DashboardLayout: React.FC<WithChildren<unknown>> = ({ children }) => {
    const { user } = routeApi.useLoaderData();

    const { lockDocumentScrollY, unlockDocumentScrollY } = useDocumentLockScrollY();
    const isMobile = useMediaQuery('(max-width: 1280px)');

    const [isMobileMenuOpened, toggleIsMobileMenuOpened] = useToggle();

    useEffect(() => {
        isMobileMenuOpened ? lockDocumentScrollY() : unlockDocumentScrollY();
    }, [isMobileMenuOpened, lockDocumentScrollY, unlockDocumentScrollY]);

    const linkClickHandler = useCallback(() => {
        toggleIsMobileMenuOpened(false);
        unlockDocumentScrollY();
    }, [toggleIsMobileMenuOpened, unlockDocumentScrollY]);

    useEffect(() => {
        if (!isMobile) {
            toggleIsMobileMenuOpened(false);
            unlockDocumentScrollY();
        }

        return () => {
            unlockDocumentScrollY();
        };
    }, [isMobile, toggleIsMobileMenuOpened, unlockDocumentScrollY]);

    return (
        <div className={clsx(s.wrap, 'full-height')}>
            <header className={s.header}>
                <Container className={s.container}>
                    <div className={s.top}>
                        <div className={clsx(s['logo-wrap'], 'focus-within-primary')}>
                            <Link className={s.link} to="/" search={{}}>
                                <img className={s.logo} src="/images/logo-full.webp" alt="Health Guard logo" />
                            </Link>
                        </div>
                        <div
                            className={clsx(s['nav-wrap'], 'scrollbar-hidden', {
                                'full-height': isMobile,
                            })}
                            data-state={isMobileMenuOpened ? 'open' : 'closed'}
                        >
                            <div className={s['nav-inner']}>
                                <NavigationMenuRoot className={s.nav}>
                                    <NavigationMenuList className={s['nav-list']}>
                                        <NavigationMenuItem className={s['nav-item']}>
                                            <NavigationMenuLink asChild>
                                                <Link
                                                    className={clsx(s.link, 'focus-primary')}
                                                    to="/dashboard"
                                                    search={{
                                                        commissionStatus: ALL_VALUE,
                                                        createdDate: ALL_VALUE,
                                                        dealStatus: ALL_VALUE,
                                                        level: ALL_VALUE,
                                                        soldReferrals: ALL_VALUE,
                                                        totalReferrals: ALL_VALUE,
                                                        totalCommissions: ALL_VALUE,
                                                    }}
                                                    tabIndex={isMobileMenuOpened ? 0 : -1}
                                                    onClick={linkClickHandler}
                                                >
                                                    <NavLinkItem label="Dashboard" />
                                                </Link>
                                            </NavigationMenuLink>
                                        </NavigationMenuItem>
                                        <NavigationMenuItem className={s['nav-item']}>
                                            <NavigationMenuLink asChild>
                                                <Link
                                                    className={clsx(s.link, 'focus-primary')}
                                                    to="/my-team"
                                                    search={{
                                                        levelOneSearch: '',
                                                        levelTwoSearch: '',
                                                    }}
                                                    tabIndex={isMobileMenuOpened ? 0 : -1}
                                                    onClick={linkClickHandler}
                                                >
                                                    <NavLinkItem label="My Team" />
                                                </Link>
                                            </NavigationMenuLink>
                                        </NavigationMenuItem>
                                        {/* <TrainingMaterialsPopoverItem /> */}
                                        {/* <NavigationMenuItem className={s['nav-item']}>
                                            <NavigationMenuLink asChild>
                                                <Link
                                                    className={clsx(s.link, 'focus-primary')}
                                                    to="/coming-soon"
                                                    tabIndex={isMobileMenuOpened ? 0 : -1}
                                                    onClick={linkClickHandler}
                                                >
                                                    <NavLinkItem label="Training & Marketing" />
                                                </Link>
                                            </NavigationMenuLink>
                                        </NavigationMenuItem> */}
                                        <NavigationMenuItem className={s['nav-item']}>
                                            <NavigationMenuLink asChild>
                                                <Link
                                                    className={clsx(s.link, 'focus-primary')}
                                                    to="/payment-preferences"
                                                    tabIndex={isMobileMenuOpened ? 0 : -1}
                                                    onClick={linkClickHandler}
                                                >
                                                    <NavLinkItem label="Payment Preferences" />
                                                </Link>
                                            </NavigationMenuLink>
                                        </NavigationMenuItem>
                                        <NavigationMenuItem className={s['nav-item']}>
                                            <NavigationMenuLink asChild>
                                                <Link
                                                    className={clsx(s.link, 'focus-primary')}
                                                    to="/book-appointment"
                                                    tabIndex={isMobileMenuOpened ? 0 : -1}
                                                    onClick={linkClickHandler}
                                                >
                                                    <NavLinkItem label="Book Appointment" />
                                                </Link>
                                            </NavigationMenuLink>
                                        </NavigationMenuItem>
                                    </NavigationMenuList>
                                </NavigationMenuRoot>
                                <div className={s['cta-wrap']}>
                                    <div className={s['user-wrap']}>
                                        <Typography className={s['user-info']} variant="paragraph-sm" asChild>
                                            <span>Affiliate ID:&nbsp;{user.id}</span>
                                        </Typography>
                                        {user?.sponsorId ? (
                                            <Typography className={s['user-info']} variant="paragraph-sm" asChild>
                                                <span>Sponsor ID:&nbsp;{user.sponsorId}</span>
                                            </Typography>
                                        ) : null}
                                    </div>
                                    <Button className={clsx(s.cta, s['log-out'])} variant="outline" asChild>
                                        <Link to="/login" tabIndex={isMobileMenuOpened ? 0 : -1}>
                                            Log out
                                        </Link>
                                    </Button>
                                </div>
                            </div>
                        </div>
                        {isMobile ? (
                            <div className={s['menu-wrap']}>
                                <button className={clsx(s.menu, 'focus-primary')} onClick={toggleIsMobileMenuOpened}>
                                    <Hamburger color="var(--color-dark-100)" size={24} toggled={isMobileMenuOpened} />
                                </button>
                            </div>
                        ) : null}
                    </div>
                </Container>
            </header>
            <div className={s.inner}>
                <main className={s.content}>{children}</main>
            </div>
        </div>
    );
};

export default memo(DashboardLayout);
