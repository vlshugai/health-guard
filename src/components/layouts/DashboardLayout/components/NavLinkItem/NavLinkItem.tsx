import type { NavLinkItemProps } from './types';
import Typography from '@/ui/Typography';
import s from './NavLinkItem.module.css';

const NavLinkItem: React.FC<NavLinkItemProps> = ({ label }) => {
    return (
        <Typography className={s.label} variant="paragraph-sm" asChild>
            <span>{label}</span>
        </Typography>
    );
};

export default NavLinkItem;
