import type { WithClassName } from '@/types';

export type NavLinkItemProps = WithClassName<{
    label: string;
}>;
