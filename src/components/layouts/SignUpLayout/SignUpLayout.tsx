import { memo } from 'react';
import { Link, getRouteApi } from '@tanstack/react-router';
import clsx from 'clsx';
import type { WithChildren } from '@/types';
import Typography from '@/ui/Typography';
import Container from '@/ui/Container';
import s from './SignUpLayout.module.css';

const routeApi = getRouteApi('/_sign-up-layout');

const SignUpLayout: React.FC<WithChildren<unknown>> = ({ children }) => {
    const sponsor = routeApi.useLoaderData();

    return (
        <div className={clsx(s.wrap, 'full-height')}>
            <div className={s.inner}>
                <Container className={s.container}>
                    <header className={s.header}>
                        <div className={clsx(s['logo-wrap'], 'focus-within-primary')}>
                            <Link
                                className={s.link}
                                to="/"
                                search={{
                                    subId: sponsor?.subId,
                                }}
                            >
                                <img className={s.logo} src="/images/logo-full.webp" alt="Health Guard logo" />
                            </Link>
                        </div>
                        {sponsor.isWithSponsor ? (
                            <div className={s['sponsor-wrap']}>
                                <Typography className={s.label} variant="paragraph-sm" asChild>
                                    <h2>
                                        Sponsor ID:&nbsp;
                                        <span>
                                            {sponsor.sponsorName} ({sponsor.sponsorId})
                                        </span>
                                    </h2>
                                </Typography>
                            </div>
                        ) : null}
                    </header>
                    <main className={s.content}>{children}</main>
                </Container>
            </div>
        </div>
    );
};

export default memo(SignUpLayout);
