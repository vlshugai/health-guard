import { memo } from 'react';
import clsx from 'clsx';
import type { CardProps } from './types';
import Typography from '@/ui/Typography';
import s from './Card.module.css';

const Card: React.FC<CardProps> = ({ title, number = undefined, className, children }) => {
    return (
        <article className={clsx(s.wrap, 'truncate', className)}>
            <Typography className={clsx(s.title, 'truncate')} variant="paragraph-base" title={title} asChild>
                <h3>{title}</h3>
            </Typography>
            <Typography className={clsx(s.number, 'truncate')} variant="title-h4" title={number?.toString()} asChild>
                <div>{number === undefined ? '--' : number}</div>
            </Typography>
            {children ? <div className={s.content}>{children}</div> : null}
        </article>
    );
};

export default memo(Card);
