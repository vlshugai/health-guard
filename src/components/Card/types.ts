import type { FCProps } from '@/types';

export type CardProps = FCProps<{
    /**
        The title of the component
    */
    title?: string;
    /**
        The number of the component
    */
    number?: string | number;
}>;
