import { memo } from 'react';
import {
    Root as DropdownMenuRoot,
    Portal as DropdownMenuPortal,
    Trigger as DropdownMenuTrigger,
    Content as DropdownMenuContent,
    RadioGroup as DropdownMenuRadioGroup,
    RadioItem as DropdownMenuRadioItem,
} from '@radix-ui/react-dropdown-menu';
import clsx from 'clsx';
import type { DropdownMenuProps, DropdownMenuOptionType } from './types';
import { Case, Default, Switch } from '@/components/utils/Switch';
import ErrorMessage from '@/ui/ErrorMessage';
import Typography from '@/ui/Typography';
import Label from '@/ui/Label';
import Icon from '@/ui/Icon';
import s from './DropdownMenu.module.css';

const DropdownMenu: React.FC<DropdownMenuProps> = ({
    className,
    label,
    placeholder = 'Choose something...',
    value,
    onChange,
    options,
    errorMessage,
    disabled = false,
    ...rest
}) => {
    return (
        <DropdownMenuRoot modal={false} {...rest}>
            <div
                className={clsx(s.wrap, className, {
                    [s.selected]: !!value,
                    [s.disabled]: disabled,
                })}
            >
                {!!label && <Label className={s.label}>{label}</Label>}
                <DropdownMenuTrigger disabled={disabled} asChild>
                    <button className={s.toggler}>
                        <Typography className={clsx(s['toggler-content'], 'truncate')} variant="paragraph-sm" asChild>
                            <span>
                                <Switch>
                                    <Case condition={!!value}>{value?.label ?? ''}</Case>
                                    <Default>{placeholder}</Default>
                                </Switch>
                            </span>
                        </Typography>
                        <Icon className={s['caret-icon']} id="icon-caret_24" />
                    </button>
                </DropdownMenuTrigger>
                <DropdownMenuPortal>
                    <DropdownMenuContent className={s.content} sideOffset={0}>
                        <DropdownMenuRadioGroup value={value?.value ?? ''} onValueChange={onChange}>
                            {options.map((option: DropdownMenuOptionType, index) => {
                                return (
                                    <DropdownMenuRadioItem key={index} className={s.item} value={option.value}>
                                        <Typography className={s['item-content']} variant="paragraph-base" asChild>
                                            <span>{option.label}</span>
                                        </Typography>
                                        <Icon className={s['item-icon']} id="icon-check_18" />
                                    </DropdownMenuRadioItem>
                                );
                            })}
                        </DropdownMenuRadioGroup>
                    </DropdownMenuContent>
                </DropdownMenuPortal>
                {!!errorMessage && <ErrorMessage className={s['error-wrap']}>{errorMessage}</ErrorMessage>}
            </div>
        </DropdownMenuRoot>
    );
};

export default memo(DropdownMenu);
