import type { DropdownMenuProps as RadixDropdownMenuProps } from '@radix-ui/react-dropdown-menu';
import type { FCProps } from '@/types';

export type DropdownMenuOptionType = {
    /**
        The value of the option.
    */
    value: string;
    /**
        The label of the option.
    */
    label: string;
    /**
     * Order of the option.
     */
    order?: number;
};

export type DropdownMenuProps = FCProps<{
    /**
      Label of the component.
    */
    label?: string;
    /**
        The placeholder of the component.
    */
    placeholder?: string;
    /**
        The value of the component.
    */
    value?: DropdownMenuOptionType | null;
    /**
        Change value handler.
    */
    onChange: (value: string) => void;
    /**
        The options of the component.
    */
    options: DropdownMenuOptionType[];
    /**
        If true, component is disabled.
    */
    disabled?: boolean;
    /**
        If true, error state will be shown.
    */
    errorMessage?: string;
}> &
    RadixDropdownMenuProps;
